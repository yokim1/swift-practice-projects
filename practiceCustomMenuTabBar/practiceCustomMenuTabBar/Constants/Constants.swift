//
//  File.swift
//  practiceCustomMenuTabBar
//
//  Created by 김윤석 on 2021/08/01.
//

import UIKit

struct Constants {
    static let numberOfItems: CGFloat = 3
    static let menuTabBarHeight: CGFloat = 50
    static let watchListHeight: CGFloat = 100
    static let leadingPadding: CGFloat = 20
}
