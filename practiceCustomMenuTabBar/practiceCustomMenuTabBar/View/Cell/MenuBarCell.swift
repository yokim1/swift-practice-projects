//
//  MenuBarCell.swift
//  practiceCustomMenuTabBar
//
//  Created by 김윤석 on 2021/08/01.
//

import UIKit

class MenuBarCell: UICollectionViewCell {
    static let identifier = "MenuBarCell"
    
    let menuImageView: UIImageView = {
       let imageView = UIImageView()
       
        imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .lightGray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override var isHighlighted: Bool {
        didSet {
            menuImageView.tintColor = isHighlighted ? .white : .lightGray
        }
    }
    
    override var isSelected: Bool {
        didSet {
            menuImageView.tintColor = isSelected ? .white : .lightGray
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureCellImageView()
    }
    
    fileprivate func configureCellImageView() {
        addSubview(menuImageView)
        
        menuImageView.image?.withTintColor(.white)
        
        NSLayoutConstraint.activate([
            menuImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            menuImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            menuImageView.widthAnchor.constraint(equalToConstant: 25),
            menuImageView.heightAnchor.constraint(equalToConstant: 25),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
