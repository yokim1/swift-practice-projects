//
//  MenuTabBar.swift
//  practiceCustomMenuTabBar
//
//  Created by 김윤석 on 2021/07/31.
//

import UIKit

class MenuTabBarView: UIView {
    
    let collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = .systemBlue
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    
    var baseViewController: BaseViewController?
    
    let cellImageString = ["house", "stockImage", "person", "circle"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureCollectionView()
        configureHorizontalBar()
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
    }
    
    fileprivate func configureCollectionView() {
        collectionView.register(MenuBarCell.self, forCellWithReuseIdentifier: MenuBarCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    fileprivate func configureHorizontalBar() {
        let horizontalBar = UIView()
        horizontalBar.translatesAutoresizingMaskIntoConstraints = false
        horizontalBar.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        addSubview(horizontalBar)
        
        horizontalBarLeftAnchorConstraint = horizontalBar.leftAnchor.constraint(equalTo: self.leftAnchor)
        
        horizontalBarLeftAnchorConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
            horizontalBar.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            horizontalBar.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1 / CGFloat(Constants.numberOfItems)),
            horizontalBar.heightAnchor.constraint(equalToConstant: 4)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MenuTabBarView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Int(Constants.numberOfItems)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuBarCell.identifier, for: indexPath) as! MenuBarCell
        switch indexPath.item {
        case 0:
            cell.menuImageView.image = UIImage(systemName: cellImageString[indexPath.item])
            break
            
        case 1:
            cell.menuImageView.image = UIImage(named: cellImageString[indexPath.item])?.withRenderingMode(.alwaysTemplate)
            cell.tintColor = .lightGray
            break
            
        case 2:
            cell.menuImageView.image = UIImage(systemName: cellImageString[indexPath.item])
            break
        default:
            break
        }
        
        cell.backgroundColor = .systemBlue
        return cell
    }
}

extension MenuTabBarView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        baseViewController?.scrollToMenuIndex(menuIndex: indexPath.item)
    }
}

extension MenuTabBarView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (frame.width) / CGFloat(Constants.numberOfItems)
        return .init(width: width, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
}
