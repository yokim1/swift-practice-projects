//
//  ViewController.swift
//  practiceCustomMenuTabBar
//
//  Created by 김윤석 on 2021/07/31.
//

import UIKit

class BaseViewController: UICollectionViewController {
    
    let menuBar: MenuTabBarView = {
       let menuBar = MenuTabBarView()
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        return menuBar
    }()
    
    let labelString = ["House", "Pencil", "Person",]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationTitleLabel()
        configureRightNavBarButtons()
        configureCollectionView()
        configureMenuBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    fileprivate func configureNavigationTitleLabel() {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.text = "  iFinance"
        titleLabel.textColor = .white
        titleLabel.font = UIFont.boldSystemFont(ofSize: 32)
        navigationItem.titleView = titleLabel
    }
    
    fileprivate func configureRightNavBarButtons() {
        navigationController?.navigationBar.isTranslucent = false
        let searchButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .done, target: self, action: #selector(searchButtonDidTap))
        searchButton.tintColor = .white
        navigationItem.rightBarButtonItem = searchButton
    }
    
    @objc func searchButtonDidTap() {
        let vc = SearchViewController()
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    fileprivate func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.register(HomeViewCell.self, forCellWithReuseIdentifier: HomeViewCell.identifier)
        collectionView.register(PencilViewCell.self, forCellWithReuseIdentifier: PencilViewCell.identifier)
        collectionView.register(PersonViewCell.self, forCellWithReuseIdentifier: PersonViewCell.identifier)
        collectionView.register(CircleViewCell.self, forCellWithReuseIdentifier: CircleViewCell.identifier)
        collectionView.isPagingEnabled = true
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
    }
    
    fileprivate func configureMenuBar() {
        menuBar.baseViewController = self
        view.addSubview(menuBar)
        
        NSLayoutConstraint.activate([
            menuBar.topAnchor.constraint(equalTo: view.topAnchor),
            menuBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            menuBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            menuBar.heightAnchor.constraint(equalToConstant: Constants.menuTabBarHeight),
        ])
    }
    
    func scrollToMenuIndex(menuIndex: Int) {
        collectionView.isPagingEnabled = false
        let indexPath = IndexPath(row: menuIndex, section: 0)
        collectionView.scrollToItem(at: indexPath, at: [], animated: true)
        collectionView.isPagingEnabled = true
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / CGFloat(Constants.numberOfItems)
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Int(Constants.numberOfItems)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.item {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeViewCell.identifier, for: indexPath) as! HomeViewCell
            cell.set(labelString: labelString[indexPath.item])
            
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PencilViewCell.identifier, for: indexPath) as! PencilViewCell
            cell.set(labelString: labelString[indexPath.item])
            
            return cell
        
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PersonViewCell.identifier, for: indexPath) as! PersonViewCell
            cell.set(labelString: labelString[indexPath.item])
            
            return cell
            
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CircleViewCell.identifier, for: indexPath) as! CircleViewCell
            cell.set(labelString: labelString[indexPath.item])
            
            return cell

        default:
            return UICollectionViewCell()
        }
    }
}

extension BaseViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
}
