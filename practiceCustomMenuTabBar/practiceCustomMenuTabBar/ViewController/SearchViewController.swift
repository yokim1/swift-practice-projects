//
//  SearchViewController.swift
//  practiceCustomMenuTabBar
//
//  Created by 김윤석 on 2021/08/01.
//

import UIKit

class SearchViewController: UIViewController {
    
    fileprivate let customNavBar: UIView = {
       let uv = UIView()
        uv.backgroundColor = .systemBlue
        uv.translatesAutoresizingMaskIntoConstraints = false
        return uv
    }()
    
    fileprivate let backButton: UIButton = {
       let button = UIButton()
        button.setImage(UIImage(systemName: "chevron"), for: .normal)
        button.backgroundColor = .white
        button.setTitleColor(.white, for: .normal)
        button.imageView?.tintColor = .black
        button.contentMode = .scaleAspectFit
        button.clipsToBounds = true
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    fileprivate let searchBarTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .white
        tf.layer.cornerRadius = 8
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.addLeftPadding()
        return tf
    }()
    
    fileprivate let searchButton: UIButton = {
        let sb = UIButton()
        sb.translatesAutoresizingMaskIntoConstraints = false

        return sb
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        configureNavigationBar()
        configureBackButton()
        configureSearchButton()
        configureSearchBar()
        
    }
    
    fileprivate func configureNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        view.addSubview(customNavBar)
        
        NSLayoutConstraint.activate([
            customNavBar.topAnchor.constraint(equalTo: view.topAnchor),
            customNavBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            customNavBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            customNavBar.heightAnchor.constraint(equalToConstant: Constants.menuTabBarHeight * 2)
        ])
    }
    
    fileprivate func configureBackButton() {
        backButton.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        customNavBar.addSubview(backButton)
        
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: customNavBar.leadingAnchor, constant: 10),
            backButton.bottomAnchor.constraint(equalTo: customNavBar.bottomAnchor, constant: -10),
            
            backButton.widthAnchor.constraint(equalToConstant: 30),
            backButton.heightAnchor.constraint(equalToConstant: 30),
        ])
    }
    
    @objc func backButtonDidTap() {
        navigationController?.popViewController(animated: true)
    }
    
    fileprivate func configureSearchBar() {
        customNavBar.addSubview(searchBarTextField)
        
        NSLayoutConstraint.activate([
            searchBarTextField.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: Constants.leadingPadding),
            searchBarTextField.centerYAnchor.constraint(equalTo: backButton.centerYAnchor),
            searchBarTextField.trailingAnchor.constraint(equalTo: searchButton.leadingAnchor, constant: -Constants.leadingPadding),
            searchBarTextField.heightAnchor.constraint(equalTo: backButton.heightAnchor),
            
            //searchBarTextField.widthAnchor.constraint(equalToConstant: 300),
        ])
    }
    
    fileprivate func configureSearchButton() {
        customNavBar.addSubview(searchButton)
        
        searchButton.backgroundColor = .blue
        
        NSLayoutConstraint.activate([
            searchButton.trailingAnchor.constraint(equalTo: customNavBar.trailingAnchor, constant: -10),
            searchButton.bottomAnchor.constraint(equalTo: customNavBar.bottomAnchor, constant: -10),
            
            searchButton.widthAnchor.constraint(equalToConstant: 30),
            searchButton.heightAnchor.constraint(equalToConstant: 30),
        ])

    }
}
