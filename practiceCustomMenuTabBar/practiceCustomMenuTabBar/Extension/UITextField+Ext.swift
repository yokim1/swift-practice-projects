//
//  UITextField+Ext.swift
//  practiceCustomMenuTabBar
//
//  Created by 김윤석 on 2021/08/02.
//

import UIKit

extension UITextField {
  func addLeftPadding() {
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
    self.leftView = paddingView
    self.leftViewMode = ViewMode.always
  }
}
