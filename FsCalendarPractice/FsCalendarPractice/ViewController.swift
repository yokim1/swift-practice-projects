//
//  ViewController.swift
//  FsCalendarPractice
//
//  Created by 김윤석 on 2021/04/10.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func clicked(_ sender: Any) {
        guard let storyboard = self.storyboard?.instantiateViewController(identifier: "CollectionViewController") else {return}
        present(storyboard, animated: true, completion: nil)
    }
    
    @IBAction func clicked2(_ sender: Any) {
        guard let storyboard = self.storyboard?.instantiateViewController(identifier: "TableViewController") else {return}
        present(storyboard, animated: true, completion: nil)
        
    }
    
    @IBAction func clicked3(_ sender: Any) {
        guard let storyboard = self.storyboard?.instantiateViewController(identifier: "LongViewController") else {return}
        present(storyboard, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }


}

