//
//  LocationHandler.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/10.
//

import CoreLocation

class LocationHandler: NSObject, CLLocationManagerDelegate{
    static let shared = LocationHandler()
    var locationManager: CLLocationManager?
    var location: CLLocation?
    
    override init() {
        super.init()
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .authorizedWhenInUse {
            locationManager?.requestAlwaysAuthorization()
        }
    }
}
