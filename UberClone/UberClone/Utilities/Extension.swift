//
//  Extension.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/02.
//

import UIKit
import MapKit

extension UIView {
    //MARK: - Customized Container View
    func customizedInputContainerView(image: UIImage, textField: UITextField? = nil, segmentControl: UISegmentedControl? = nil) -> UIView {
        let view = UIView()
        
        /// custom Image
        let imageView = UIImageView()
        imageView.image = image
        imageView.tintColor = .white
        imageView.alpha = 0.75
        view.addSubview(imageView)
        
        ///text field
        if let textField = textField {
            imageView.centerY(inView: view)
            imageView.anchor(left: view.leftAnchor,paddingLeft: 8, width: 24, height: 24)
            
            view.addSubview(textField)
            textField.anchor(left: imageView.rightAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingLeft: 10)
            textField.centerY(inView: imageView)
        }
        
        ///segment Control
//        if let segmentControl = segmentControl {
//            imageView.anchor(top: view.topAnchor, left: view.leftAnchor, width: 24, height: 24)
//
//            view.addSubview(segmentControl)
//            segmentControl.anchor(top: imageView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor)
//            segmentControl.centerY(inView: view)
//        }
        
        if let sc = segmentControl {
            imageView.anchor(top: view.topAnchor, left: view.leftAnchor,
                             paddingTop: -8, paddingLeft: 8, width: 24, height: 24)
            
            view.addSubview(sc)
            sc.anchor(left: view.leftAnchor, right: view.rightAnchor,
                     paddingLeft: 8, paddingRight: 8)
            sc.centerY(inView: view)
        }
        
        let separator = UIView()
        separator.backgroundColor = .lightGray
        separator.alpha = 0.5
        
        view.addSubview(separator)
        separator.anchor(left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, height: 0.5)
        
        return view
    }
    
    
    //MARK: - Constraint
    func anchor(top: NSLayoutYAxisAnchor? = nil,
                left: NSLayoutXAxisAnchor? = nil,
                bottom: NSLayoutYAxisAnchor? = nil,
                right: NSLayoutXAxisAnchor? = nil,
                paddingTop: CGFloat = 0,
                paddingLeft: CGFloat = 0,
                paddingBottom: CGFloat = 0,
                paddingRight: CGFloat = 0,
                width: CGFloat? = nil,
                height: CGFloat? = nil) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom{
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    func centerX(inView view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func centerY(inView view: UIView){
        translatesAutoresizingMaskIntoConstraints = false
        
        centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

extension UITextField {
    func customizedTextField(placeHolder: String, isSecure: Bool) -> UITextField {
        
        borderStyle = .none
        font = UIFont.systemFont(ofSize: 16)
        textColor = .white
        keyboardAppearance = .dark
        isSecureTextEntry = isSecure
        attributedPlaceholder = NSAttributedString(string: placeHolder,
                                                   attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        return self
    }
}

extension MKPlacemark {
    var address: String? {
        get{
            guard let subThoroughfare = subThoroughfare else {return nil}
            guard let thoroughfare = thoroughfare else {return nil}
            guard let locality = locality else {return nil}
            guard let adminArea = administrativeArea else {return nil}
            
            return "\(subThoroughfare) \(thoroughfare) \(locality) \(adminArea)"
        }
    }
}

extension MKMapView {
    func zoomToFit(annotations: [MKAnnotation], view: UIView) {
        
        var zoomRect = MKMapRect.null
        annotations.forEach { annotation in
            let annotationPoint = MKMapPoint(annotation.coordinate)
            let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y,
                                      width: 0.01, height: 0.01)
            zoomRect = zoomRect.union(pointRect)
        }
        let paddingY: CGFloat = view.frame.height / 7
        let paddingX: CGFloat = view.frame.width / 7
        let insets = UIEdgeInsets(top: paddingY, left: paddingX,
                                  bottom: view.frame.height / 2.5, right: paddingX)
        setVisibleMapRect(zoomRect, edgePadding: insets, animated: true)
    }
}
