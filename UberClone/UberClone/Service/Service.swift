//
//  Service.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/10.
//
//import FirebaseAuth
import FirebaseDatabase
import GeoFire
import CoreLocation
import UIKit

let DB_Ref = Database.database().reference()
let Ref_Users = DB_Ref.child("users")
let Ref_Driver_Location = DB_Ref.child("driverLocations")

class Service{
    static let shared = Service()
    
    ///I am no sure I need this
    func fetchUserData(uid: String, completion: @escaping(User)-> Void) {
        //guard let currentUser = Auth.auth().currentUser else {return}
        
        Ref_Users.child(uid).observeSingleEvent(of: .value) { snapshot in
            guard let userInfo = snapshot.value as? [String: Any] else {return }
            let uid = snapshot.key 
            completion(User(uid: uid, dictionary: userInfo))
        }
    }
    
    func fetchDrivers(location: CLLocation, completion: @escaping (User)-> Void) {
        let geofire = GeoFire(firebaseRef: Ref_Driver_Location)
        
        Ref_Driver_Location.observe(.value) { snapshot in
            geofire.query(at: location, withRadius: 50).observe(.keyEntered, with: { (uid, location) in
                self.fetchUserData(uid: uid) { user in
                    
                    var driver = user
                    driver.location = location
                    completion(driver)
                }
            })
        }
    }
    
}
