//
//  User.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/10.
//
import CoreLocation
import UIKit

struct User {
    let uid: String
    let fullName: String
    let email: String
    let accountType: Int
    var location: CLLocation?
    
    
    init(uid:String, dictionary: [String: Any]) {
        self.uid = uid
        self.fullName = dictionary["fullName"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
        self.accountType = dictionary["userType"] as? Int ?? 0
    }
}
