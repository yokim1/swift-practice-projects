//
//  DriverAnnotation.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/12.
//
import MapKit
import UIKit

class DriverAnnotation: NSObject, MKAnnotation {
    static let identifier = "DriverAnnotation"
    dynamic var coordinate: CLLocationCoordinate2D
    var uid:String
    
    init (uid:String, coordinate: CLLocationCoordinate2D){
        self.uid = uid
        self.coordinate = coordinate
        super.init()
    }
    
    func updateAnnotationPosition(with coordinate: CLLocationCoordinate2D){
        UIView.animate(withDuration: 0.2) {
            self.coordinate = coordinate
        }
    }
}
