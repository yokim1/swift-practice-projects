//
//  LoginViewController.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/02.
//
import FirebaseAuth
import UIKit

class LoginViewController: UIViewController {

    //MARK: - Property
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "해줘!"
        label.font = UIFont(name: "Avenir-Light", size: 36)
        label.textColor = UIColor(white: 1, alpha: 0.8)
        return label
    }()
    
    //MARK: - Email
    private lazy var emailContainerView: UIView = {
        guard let image = UIImage(systemName: "mail") else {return UIView()}
        return UIView().customizedInputContainerView(image: image, textField: emailTextField)
    }()
    
    private let emailTextField: UITextField = {
        let tf = UITextField()
        tf.autocapitalizationType = .none
        tf.customizedTextField(placeHolder: "Email", isSecure: false)
        return tf
    }()
    
    //MARK: - Password
    private lazy var passwordContainer: UIView = {
        guard let image = UIImage(systemName: "lock") else {return UIView()}
        return UIView().customizedInputContainerView(image: image, textField: passwordTextField)
    }()
    
    private let passwordTextField: UITextField = {
        return UITextField().customizedTextField(placeHolder: "Password", isSecure: true)
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Log In", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 8
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.addTarget(self, action: #selector(loginButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    @objc private func loginButtonDidTap() {
        guard let emailString = emailTextField.text else {return }
        guard let passwordString = passwordTextField.text else {return }
        
        Auth.auth().signIn(withEmail: emailString, password: passwordString) { result, error in
            if let error = error {
                print(error)
                return
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: - SignUp Button
    private lazy var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        let attributedTitle =
            NSMutableAttributedString(string: "Don't have an account? ",
                                        attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                                                     NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        attributedTitle.append(
            NSMutableAttributedString(string: "Sign Up",
                                        attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                                                     NSAttributedString.Key.foregroundColor: UIColor.systemBlue]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        button.addTarget(self, action: #selector(signUpButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    @objc private func signUpButtonDidTap() {
        let vc = SignUpViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUINavigationController()
        
        view.backgroundColor = UIColor(red: 25/255, green: 25/255, blue: 25/255, alpha: 1)
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor)
        titleLabel.centerX(inView: view)
        
        emailContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        passwordContainer.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let containerViewStack = UIStackView(arrangedSubviews: [emailContainerView, passwordContainer, loginButton])
        containerViewStack.axis = .vertical
        containerViewStack.spacing = 16
        containerViewStack.distribution = .fillEqually
        
        view.addSubview(containerViewStack)
        containerViewStack.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 40, paddingLeft: 14, paddingRight: 14)
        
        view.addSubview(signUpButton)
        signUpButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor)
        signUpButton.centerX(inView: view)
    }
    
    private func configureUINavigationController() {
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
    }
}
