//
//  SignUpViewController.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/03.
//
import FirebaseAuth
//import FirebaseDatabase
import GeoFire
import UIKit

class SignUpViewController: UIViewController {
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "해줘!"
        label.font = UIFont(name: "Avenir-Light", size: 36)
        label.textColor = UIColor(white: 1, alpha: 0.8)
        return label
    }()
    
    private lazy var emailContainerView: UIView = {
        guard let image = UIImage(systemName: "mail") else {return UIView()}
        return UIView().customizedInputContainerView(image: image, textField: emailTextField)
    }()
    
    private lazy var fullNameContainerView: UIView = {
        guard let image = UIImage(systemName: "person") else {return UIView()}
        return UIView().customizedInputContainerView(image: image, textField: fullNameTextField)
    }()
    
    private lazy var passwordContainerView: UIView = {
        guard let image = UIImage(systemName: "lock") else {return UIView()}
        return UIView().customizedInputContainerView(image: image, textField: passwordTextField)
    }()
    //
    //    private lazy var segmentContainerView: UIView = {
    //        guard let image = UIImage(systemName: "Person") else {return UIView()}
    //        return UIView().customizedInputContainerView(image: image, segmentControl: accountTypeSegmentedControl)
    //    }()
    
    lazy var userTypeSelectorContainerView: UIView = {
        let view = UIView()
        let imageView = UIImageView(image: UIImage(systemName: "paperplane"))
        view.addSubview(imageView)
        imageView.tintColor = UIColor(white: 1, alpha: 0.75)
        imageView.anchor(top: view.topAnchor, left: view.leftAnchor, paddingLeft: 8, width: 24, height: 24)
        
        let sc = accountTypeSegmentedControl
        sc.backgroundColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        sc.tintColor = UIColor(white: 1, alpha: 0.87)
        sc.selectedSegmentIndex = 0
        view.addSubview(sc)
        sc.anchor(top: imageView.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 10)
        return view
    }()
    
    private let emailTextField: UITextField = {
        return UITextField().customizedTextField(placeHolder: "Email", isSecure: false)
    }()
    
    private let fullNameTextField: UITextField = {
        return UITextField().customizedTextField(placeHolder: "Full Name", isSecure: false)
    }()
    
    private let passwordTextField: UITextField = {
        return UITextField().customizedTextField(placeHolder: "Password", isSecure: true)
    }()
    
    private let accountTypeSegmentedControl: UISegmentedControl = {
        let sc = UISegmentedControl(items: ["Rider", "Driver"])
        sc.backgroundColor = UIColor(red: 25/255, green: 25/255, blue: 25/255, alpha: 0.5)
        sc.tintColor = UIColor(white: 1, alpha: 0.87)
        sc.selectedSegmentIndex = 0
        return sc
    }()
    
    private let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 8
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.addTarget(self, action: #selector(signUpButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    @objc private func signUpButtonDidTap() {
        
        guard let emailString = emailTextField.text else {return}
        guard let fullNameString = fullNameTextField.text else {return }
        guard let passwordString = passwordTextField.text else {return }
        let userType = accountTypeSegmentedControl.selectedSegmentIndex
        
        Auth.auth().createUser(withEmail: emailString, password: passwordString) { result, error in
            if let error = error {
                print(error)
            }
            
            
            guard let uid = result?.user.uid else {return }
            let value = ["email": emailString, "fullName": fullNameString, "password": passwordString, "userType": userType] as [String: Any]
            
            if userType == 1 {
                let geofire = GeoFire(firebaseRef: Ref_Driver_Location)
                guard let location = LocationHandler.shared.locationManager?.location else {return }
                
                geofire.setLocation(location, forKey: uid)
            }
            
            Ref_Users.child(uid).updateChildValues(value)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: - SignIn Button
    private lazy var signInButton: UIButton = {
        let button = UIButton(type: .system)
        let attributedTitle =
            NSMutableAttributedString(string: "Already have an account? ",
                                      attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                                                   NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        attributedTitle.append(
            NSMutableAttributedString(string: "Sign In",
                                      attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                                                   NSAttributedString.Key.foregroundColor: UIColor.systemBlue]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        button.addTarget(self, action: #selector(signInButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    @objc private func signInButtonDidTap() {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUINavigationController()
        configureUI()
        
    }
    
    private func configureUINavigationController() {
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func configureUI(){
        view.backgroundColor = UIColor(red: 25/255, green: 25/255, blue: 25/255, alpha: 1)
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor)
        titleLabel.centerX(inView: view)
        
        emailContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        fullNameContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        passwordContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        userTypeSelectorContainerView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        let containerViewStack = UIStackView(arrangedSubviews: [emailContainerView, fullNameContainerView, passwordContainerView, userTypeSelectorContainerView, signUpButton])
        containerViewStack.axis = .vertical
        containerViewStack.spacing = 16
        containerViewStack.distribution = .equalSpacing
        
        view.addSubview(containerViewStack)
        containerViewStack.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 40, paddingLeft: 14, paddingRight: 14)
        
        view.addSubview(signInButton)
        signInButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor)
        signInButton.centerX(inView: view)
    }
}
