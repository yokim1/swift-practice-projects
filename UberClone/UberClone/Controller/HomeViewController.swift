//
//  HomeViewController.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/03.
//
import MapKit
//import CoreLocation
import FirebaseAuth
import UIKit

enum actionButtonConfiguration {
    case menuButton
    case dismissButton
    
    init(){
        self = .menuButton
    }
}

class HomeViewController: UIViewController {
    
    private let locationManager = LocationHandler.shared.locationManager
    
    //MARK: - UI Objects
    private let mapView = MKMapView()
    private let searchLocationInputActivatorView = SearchLocationInputActivatorView()
    private let searchLocationInputView = SearchLocationInputView()
    private let tableView = UITableView()
    private let rideActionView = RideActionView()
    private let actionButton = UIButton(type: .system)
    private var actionButtonStatus = actionButtonConfiguration()
    
    private var route: MKRoute?
    
    var globallyKnownSelectedPlce: MKPlacemark?
    
    //MARK: - Model
    private var searchPlaceResults = [MKPlacemark]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        signOut()
        checkIfUserIsLoggedIn()
        enableLocationService()
        
        configureUI()
        fetchUserData()
        fetchDrivers()
    }
    
    private func fetchUserData() {
        //        Service.shared.fetchUserData(uid: "") { user in
        //
        //        }
    }
    
    private func fetchDrivers() {
        //Does this work?
        //        locationManager?.distanceFilter = 700.0
        guard let location = locationManager?.location else {return }
        Service.shared.fetchDrivers(location: location) { driver in
            
            guard let coordinate = driver.location?.coordinate else { return }
            let annotation = DriverAnnotation(uid: driver.uid, coordinate: coordinate)
            
            var driverIsVisible: Bool {
                return self.mapView.annotations.contains { annotation in
                    guard let driverAnno = annotation as? DriverAnnotation else {return false}
                    if driverAnno.uid == driver.uid {
                        driverAnno.updateAnnotationPosition(with: coordinate)
                        return true
                    }
                    return false
                }
            }
            
            if !driverIsVisible {
                self.mapView.addAnnotation(annotation)
            }
        }
    }
    
    private func checkIfUserIsLoggedIn() {
        if Auth.auth().currentUser?.uid == nil {
            DispatchQueue.main.async {
                let nav = UINavigationController(rootViewController: LoginViewController())
                nav.modalPresentationStyle = .fullScreen
                nav.modalTransitionStyle = .crossDissolve
                self.present(nav, animated: true, completion: nil)
            }
            
        } else {
            
        }
    }
    
    private func signOut() {
        do{
            try Auth.auth().signOut()
        } catch {
            
        }
    }
    
}

//MARK: - Selector
extension HomeViewController {
    
    @objc private func actionButtonDidTap() {
        switch actionButtonStatus {
        case .menuButton:
            print("show menu")
            
            
        case .dismissButton:
            removeAnnotations()
            removePolyLine()
            
            UIView.animate(withDuration: 0.3) {
                self.searchLocationInputActivatorView.alpha = 1
                self.actionButton.setImage(UIImage(systemName: "line.horizontal.3"), for: .normal)
                self.actionButtonStatus = .menuButton
            }
            
            mapView.showAnnotations(mapView.annotations, animated: true)
            animateRideActionView(show: false)
        }
    }
}

//MARK: - MKMapViewDelegate
extension HomeViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? DriverAnnotation else {return nil}
        let view = MKAnnotationView(annotation: annotation, reuseIdentifier: DriverAnnotation.identifier)
        view.image = UIImage(systemName: "person.fill")
        return view
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let route = route {
            let polyline = route.polyline
            let lineRenderer = MKPolylineRenderer(overlay: polyline)
            lineRenderer.strokeColor = .red
            lineRenderer.lineWidth = 3
            return lineRenderer
        }
        return MKOverlayRenderer()
    }
    
    //This updates polyline
    //    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
    //
    //        guard let place = globallyKnownSelectedPlce else { return }
    //        UIView.animate(withDuration: 1) {
    //            self.generatePolyLine(to: MKMapItem(placemark: place))
    //        } completion: { _ in
    //            self.removePolyLine()
    //        }
    //    }
}

//MARK: - Search Location by using User Input String
extension HomeViewController {
    func search(by naturalLanguageQuery: String, completion: @escaping ([MKPlacemark])-> Void) {
        var results = [MKPlacemark]()
        
        let request = MKLocalSearch.Request()
        //mapView.region.span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        request.region = mapView.region
        request.naturalLanguageQuery = naturalLanguageQuery
        print("MAPVIEW: \(mapView.region)")
        print("naturalLanguage: \(naturalLanguageQuery)")
        print(request)
        
        
        ///There is chance MKLocalSearch would not work properly,
        ///if user's device connection is not right or
        ///user uses wifi instead of cellular
        MKLocalSearch(request: request).start { response, error in
            //            response?.mapItems.forEach({ item in
            //                results.append(item.placemark)
            //            })
            //            print("PlaceMarks")
            //            print(results)
            //            completion(results)
            //
            if (error == nil) {
                print("No Error")
                let placeMarks: NSMutableArray = NSMutableArray()
                
                guard let response = response else { print("response is nil"); return; }
                
                for res in response.mapItems {
                    if let mi = res as? MKMapItem {
                        placeMarks.add(mi.placemark)
                    }
                }
                print(placeMarks)
                completion(placeMarks as! [MKPlacemark])
                //
                //                    self.mapView.removeAnnotations(self.mapView.annotations)
                //                    self.mapView.showAnnotations(placeMarks, animated: true)
            } else {
                print("Error")
                print(error)
                print(error.debugDescription)
                completion([])
            }
        }
    }
    
    func generatePolyLine(to destination: MKMapItem) {
        let request = MKDirections.Request()
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destination
        request.transportType = .automobile
        
        let directionRequest = MKDirections(request: request)
        directionRequest.calculate { response, error in
            guard let response = response else {return }
            self.route = response.routes.first
            guard let polyline = self.route?.polyline else {return }
            self.mapView.addOverlay(polyline)
        }
    }
    
    private func removeAnnotations() {
        mapView.annotations.forEach { annotation in
            if let anno = annotation as? MKAnnotation {
                self.mapView.removeAnnotation(anno)
            }
        }
    }
    
    private func removePolyLine() {
        mapView.overlays.forEach { overlay in
            mapView.removeOverlay(overlay)
        }
    }
}

extension HomeViewController {
    func enableLocationService() {
        
        switch locationManager?.authorizationStatus {
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            break
            
        case .authorizedAlways:
            locationManager?.startUpdatingLocation()
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            break
            
        case .authorizedWhenInUse:
            locationManager?.requestAlwaysAuthorization()
            break
            
        case .none:
            break
            
        @unknown default:
            break
        }
    }
    
    func animateRideActionView(show: Bool) {
        let height = view.frame.height / 3
        let originY = show ? (view.frame.height - height) : view.frame.height
        
        UIView.animate(withDuration: 0.5) {
            self.rideActionView.frame.origin.y = originY
        }
    }
}

extension HomeViewController: SearchLocationInputActivatorViewDelegate {
    func searchLocationInputActivatorViewDidTap() {
        configureTableView()
        configureSearchLocationInputView()
        
    }
    
    private func configureSearchLocationInputView() {
        view.addSubview(searchLocationInputView)
        
        searchLocationInputView.anchor(top: mapView.topAnchor,
                                       left: mapView.leftAnchor,
                                       right: mapView.rightAnchor)
        
        let searchLocationInputViewHeight =  mapView.frame.height / 3.8
        searchLocationInputView.heightAnchor.constraint(equalToConstant: searchLocationInputViewHeight).isActive = true
        searchLocationInputView.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.searchLocationInputView.delegate = self
            self.searchLocationInputView.alpha = 1
        }
    }
    
    private func configureTableView() {
        let tableViewHeight = mapView.frame.height / 1.30
        let searchLocationInputViewHeight = mapView.frame.height / 3.8
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(LocationTableViewCell.self, forCellReuseIdentifier: LocationTableViewCell.identifier)
        tableView.rowHeight = LocationTableViewCell.height
        tableView.frame = CGRect(x: 0,
                                 y: mapView.frame.height,
                                 width: mapView.frame.width,
                                 height: tableViewHeight)
        
        UIView.animate(withDuration: 0.5) {
            self.tableView.frame = CGRect(x: 0,
                                          y: searchLocationInputViewHeight,
                                          width: self.mapView.frame.width,
                                          height: tableViewHeight)
        }
    }
}

extension HomeViewController: SearchLocationInputViewDelegate {
    func excuteSearch(query: String) {
        search(by: query) { places in
            
            DispatchQueue.main.async {
                self.searchPlaceResults = places
                self.tableView.reloadData()
            }
        }
    }
    
    func searchLocationInputViewBackButtonDidTap() {
        dismissLocationInputView()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "section"
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        return searchPlaceResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LocationTableViewCell.identifier, for: indexPath) as? LocationTableViewCell else {return UITableViewCell()}
        
        if indexPath.section == 0 {
            cell.configure(data: "Section Seoul seocho gu heunlleung ro")
            return cell
        }
        cell.placemark = searchPlaceResults[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedPlace = searchPlaceResults[indexPath.row]
        var annotations = [MKAnnotation]()
        let destination =  MKMapItem(placemark: selectedPlace)
        generatePolyLine(to: destination)
        
        globallyKnownSelectedPlce = selectedPlace
        
        dismissLocationInputView { _ in
            let annotation = MKPointAnnotation()
            annotation.coordinate = selectedPlace.coordinate
            self.mapView.addAnnotation(annotation)
            self.mapView.selectAnnotation(annotation, animated: true)
            
            UIView.animate(withDuration: 0.3) {
                self.actionButton.setImage(UIImage(systemName: "arrowshape.turn.up.left.fill"), for: .normal)
                self.searchLocationInputActivatorView.alpha = 0
            }
            annotations = self.mapView.annotations.filter { !$0.isKind(of: DriverAnnotation.self)}
            //self.mapView.showAnnotations(annotations, animated: true)
            self.mapView.zoomToFit(annotations: annotations, view: self.view)
            self.rideActionView.destination = selectedPlace
            self.animateRideActionView(show: true)
        }
    }
}


extension HomeViewController {
    private func dismissLocationInputView(completion: ((Bool)-> Void)? = nil) {
        UIView.animate(withDuration: 0.3, animations: {
            self.searchLocationInputView.alpha = 0
            self.tableView.frame = CGRect(x: 0,
                                          y: self.mapView.frame.height,
                                          width: self.mapView.frame.width,
                                          height: self.mapView.frame.height / 1.40)
            self.searchLocationInputView.removeFromSuperview()
            UIView.animate(withDuration: 0.3) {
                self.searchLocationInputView.alpha = 1
                self.actionButtonStatus = .dismissButton
            }
        }, completion: completion)
    }
}

extension HomeViewController{
    
    //MARK: - Configure
    private func configureUI() {
        configureMapView()
        configureActionButton()
        configureSearchLocationInputActivatorView()
        configureRideActionView()
    }
    
    private func configureActionButton() {
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        
        actionButton.setImage(UIImage(systemName: "line.horizontal.3"), for: .normal)
        actionButton.tintColor = .label
        actionButton.addTarget(self, action: #selector(actionButtonDidTap), for: .touchUpInside)
        actionButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, paddingTop: 20, paddingLeft: 20)
    }
    
    private func configureMapView() {
        mapView.frame = view.bounds
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        mapView.delegate = self
        view.addSubview(mapView)
    }
    
    private func configureSearchLocationInputActivatorView() {
        searchLocationInputActivatorView.delegate = self
        view.addSubview(searchLocationInputActivatorView)
        searchLocationInputActivatorView.translatesAutoresizingMaskIntoConstraints = false
        searchLocationInputActivatorView.anchor(top: actionButton.bottomAnchor,//view.safeAreaLayoutGuide.topAnchor,
                                                left: view.leftAnchor,
                                                right: view.rightAnchor,
                                                paddingTop: 32,
                                                paddingLeft: 20,
                                                paddingRight: 20)
        searchLocationInputActivatorView.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    private func configureRideActionView() {
        view.addSubview(rideActionView)
        let height = view.frame.height / 3
        rideActionView.frame = CGRect(x: 0, y: view.frame.height,
                                      width: view.frame.width, height: height)
    }
}
