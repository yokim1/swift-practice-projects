//
//  File.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/04.
//

import UIKit

protocol SearchLocationInputActivatorViewDelegate: AnyObject {
    func searchLocationInputActivatorViewDidTap()
}

class SearchLocationInputActivatorView: UIView {
    
    weak var delegate: SearchLocationInputActivatorViewDelegate?
    
    lazy var smallSquarwView: UIView = {
       let uv = UIView()
        uv.backgroundColor = .black
        uv.widthAnchor.constraint(equalToConstant: 10).isActive = true
        uv.heightAnchor.constraint(equalToConstant: 10).isActive = true
        uv.translatesAutoresizingMaskIntoConstraints = false
        return uv
    }()
    
    lazy var placeHolderLabel: UILabel = {
        let label = UILabel()
        label.text = "Search here..."
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.55
        layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        layer.masksToBounds = false
        
        addSubview(smallSquarwView)
        addSubview(placeHolderLabel)
        
        NSLayoutConstraint.activate([
            smallSquarwView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            smallSquarwView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            placeHolderLabel.leadingAnchor.constraint(equalTo: smallSquarwView.trailingAnchor, constant: 20),
            placeHolderLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(searchBarViewDidTap))
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc private func searchBarViewDidTap() {
        delegate?.searchLocationInputActivatorViewDidTap()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
