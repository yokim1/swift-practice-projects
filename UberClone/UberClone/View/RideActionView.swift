//
//  RideActionView.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/23.
//

import UIKit
import MapKit

protocol RideActionViewDelegate: AnyObject {
    func confirmButtonDidTap()
}

class RideActionView: UIView {
    
    var destination: MKPlacemark? {
        didSet{
            titleLabel.text = destination?.name
            addressLabel.text = destination?.address
        }
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15)
        label.text = "Title"
        label.textAlignment = .center
        return label
    }()
    
    let addressLabel: UILabel = {
      let label = UILabel()
        label.font = .systemFont(ofSize: 15)
        label.textColor = .lightGray
        label.text = "Address"
        label.textAlignment = .center
        return label
    }()
    
    private lazy var infoView: UIView = {
       let view = UIView()
        view.backgroundColor = .black
        
        let label = UILabel()
        label.text = "X"
        label.font = .systemFont(ofSize: 30, weight: .bold)
        label.textColor = .white
        
        view.addSubview(label)
        label.centerX(inView: view)
        label.centerY(inView: view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 60).isActive = true
        view.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        view.layer.cornerRadius = 60 / 2
        return view
    }()
    
    private let uberXLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 18)
        label.text = "Do it!"
        label.textAlignment = .center
        return label
    }()
    
    private let actionButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Confirm Order", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .black
        button.titleLabel?.font = .boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(acitonButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    @objc private func acitonButtonDidTap() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        let LabelStackView = UIStackView(arrangedSubviews: [titleLabel, addressLabel])
        LabelStackView.axis = .vertical
        LabelStackView.spacing = 4
        LabelStackView.distribution = .fillEqually
        
        addSubview(LabelStackView)
        LabelStackView.centerX(inView: self)
        LabelStackView.anchor(top: topAnchor, paddingTop: 12)
        
        let infoViewStackView = UIStackView(arrangedSubviews: [infoView, uberXLabel])
        infoViewStackView.axis = .vertical
        infoViewStackView.spacing = 2
        
        
//        addSubview(infoView)
//        infoView.centerX(inView: self)
//        infoView.anchor(top: LabelStackView.bottomAnchor, paddingTop: 16)
//        infoView.heightAnchor.constraint(equalToConstant: 60).isActive = true
//        infoView.widthAnchor.constraint(equalToConstant: 60).isActive = true
//        infoView.layer.cornerRadius = 60 / 2
        
        addSubview(infoViewStackView)
        infoViewStackView.centerX(inView: self)
        infoViewStackView.anchor(top: LabelStackView.bottomAnchor, paddingTop: 16)
        
        addSubview(actionButton)
        actionButton.centerX(inView: self)
        actionButton.anchor(top: infoViewStackView.bottomAnchor, left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: rightAnchor, paddingTop: 12, paddingLeft: 18, paddingBottom: 28, paddingRight: 18, height: 50)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
