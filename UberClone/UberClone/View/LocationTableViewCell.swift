//
//  LocationTableViewCell.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/05.
//

import UIKit
import MapKit

class LocationTableViewCell: UITableViewCell {
    static let identifier = "LocationCell"
    static let height: CGFloat = 60
    
    var placemark: MKPlacemark? {
        didSet{
            titleLabel.text = placemark?.name
            addressLabel.text = placemark?.address
        }
    }
    
    private lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return label
    }()
    
    private lazy var addressLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.textColor = .systemGray2
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, addressLabel])
        stackView.axis = .vertical
        stackView.spacing = 6
        stackView.distribution = .equalSpacing
        
        addSubview(stackView)
        stackView.anchor(left: leftAnchor, paddingLeft: 14)
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(data: String) {
        titleLabel.text = data
        addressLabel.text = data
    }
}
