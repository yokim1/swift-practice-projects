//
//  File.swift
//  UberClone
//
//  Created by 김윤석 on 2021/09/04.
//

import UIKit

protocol SearchLocationInputViewDelegate: AnyObject {
    func searchLocationInputViewBackButtonDidTap()
    func excuteSearch(query: String)
}

class SearchLocationInputView: UIView {
    
    weak var delegate: SearchLocationInputViewDelegate?
    
    private lazy var backButton: UIButton = {
       let button = UIButton()
        button.setImage(UIImage(systemName: "arrow.left")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    @objc private func backButtonDidTap() {
        delegate?.searchLocationInputViewBackButtonDidTap()
    }
    
    private lazy var currentLocationTextField: UITextField = {
       let tf = UITextField()
        tf.placeholder = "Current Location..."
        tf.backgroundColor = .systemGray
        
        let paddingView = UIView()
        paddingView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        paddingView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        tf.leftView = paddingView
        tf.leftViewMode = .always
        
        tf.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return tf
    }()
    
    private lazy var destinationLocationTextField: UITextField = {
       let tf = UITextField()
        tf.placeholder = "Detination Location..."
        tf.backgroundColor = .systemGray3
        
        let paddingView = UIView()
        paddingView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        paddingView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        tf.leftView = paddingView
        tf.leftViewMode = .always
        
        tf.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        tf.delegate = self
        return tf
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureStyle()
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SearchLocationInputView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let query = textField.text else {return false}
        delegate?.excuteSearch(query: query)
        return true
    }
}

//MARK: - SetUp UI
extension SearchLocationInputView {
    private func configureStyle() {
        backgroundColor = .white
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.55
        layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        layer.masksToBounds = false
    }
    
    private func configureUI() {
        addSubview(backButton)
        backButton.anchor(top: safeAreaLayoutGuide.topAnchor, left: leftAnchor, paddingLeft: 12)
        
        addSubview(currentLocationTextField)
        currentLocationTextField.anchor(top: backButton.bottomAnchor, left: leftAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingRight: 30)
        
        addSubview(destinationLocationTextField)
        destinationLocationTextField.anchor(top: currentLocationTextField.bottomAnchor, left: currentLocationTextField.leftAnchor,right: currentLocationTextField.rightAnchor,paddingTop: 20)
    }
}
