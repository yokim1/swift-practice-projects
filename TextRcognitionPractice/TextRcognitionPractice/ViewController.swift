//
//  ViewController.swift
//  TextRcognitionPractice
//
//  Created by 김윤석 on 2021/05/09.
//

import Vision
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var textLabel: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        image.image = UIImage(named: "example1")
        
        recognize(image: image.image)
    }
    
    func recognize(image: UIImage?){
        guard let cgImage = image?.cgImage else {return}
        
        let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        
      
        let request = VNRecognizeTextRequest { [weak self] request, error in
            guard let observation = request.results as?[VNRecognizedTextObservation],
                  error == nil else {return }
            
            
//            let text = observation.compactMap {
//                $0.topCandidates(1).first?.string
//            }.joined()
            
//            let text = observation.compactMap {
//                $0.topCandidates(1).first?.string
//            }.joined(separator: " | ")
            
            let text = observation.compactMap { VNRecognizedTextObservation in
                VNRecognizedTextObservation.topCandidates(1).forEach({ VNRecognizedText in
                    print(VNRecognizedText.string)
                })
            }
            
            let text2 = observation.map { <#VNRecognizedTextObservation#> in
                <#code#>
            }
//            DispatchQueue.main.async {
//                self?.textLabel.text = text
//            }
        }
        
        do {
            try handler.perform([request])
        } catch {
            print(error)
        }
    }


}

