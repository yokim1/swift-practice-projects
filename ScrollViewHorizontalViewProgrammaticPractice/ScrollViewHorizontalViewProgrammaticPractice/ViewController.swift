//
//  ViewController.swift
//  ScrollViewHorizontalViewProgrammaticPractice
//
//  Created by 김윤석 on 2021/06/14.
//

import UIKit

class ViewController: UIViewController {

    var scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = .gray
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let horizontalView1: UIStackView = {
       let hv = UIStackView()
        hv.backgroundColor = .red
        hv.axis = .horizontal
        hv.translatesAutoresizingMaskIntoConstraints = false
        return hv
    }()
    
    let horizontalView2: UIStackView = {
       let hv = UIStackView()
        hv.backgroundColor = .blue
        hv.axis = .horizontal
        hv.translatesAutoresizingMaskIntoConstraints = false
        return hv
    }()
    
    let horizontalView3: UIStackView = {
       let hv = UIStackView()
        hv.backgroundColor = .purple
        hv.axis = .horizontal
        hv.translatesAutoresizingMaskIntoConstraints = false
        return hv
    }()
    
    let horizontalView4: UIStackView = {
       let hv = UIStackView()
        hv.backgroundColor = .green
        hv.axis = .horizontal
        hv.translatesAutoresizingMaskIntoConstraints = false
        return hv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 2200))
        view.addSubview(scrollView)
//        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
//        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
//        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
//        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.height + scrollView.contentInset.bottom)
//        scrollView.setContentOffset(bottomOffset, animated: true)
        
        
        
        scrollView.addSubview(horizontalView1)
        horizontalView1.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        horizontalView1.heightAnchor.constraint(equalToConstant: 200).isActive = true
        horizontalView1.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        horizontalView1.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        scrollView.addSubview(horizontalView2)
        horizontalView2.topAnchor.constraint(equalTo: horizontalView1.bottomAnchor).isActive = true
        horizontalView2.heightAnchor.constraint(equalToConstant: 200).isActive = true
        horizontalView2.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        horizontalView2.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        scrollView.addSubview(horizontalView3)
        horizontalView3.topAnchor.constraint(equalTo: horizontalView2.bottomAnchor).isActive = true
        horizontalView3.heightAnchor.constraint(equalToConstant: 200).isActive = true
        horizontalView3.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        horizontalView3.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        scrollView.addSubview(horizontalView4)
        horizontalView4.topAnchor.constraint(equalTo: horizontalView3.bottomAnchor).isActive = true
        horizontalView4.heightAnchor.constraint(equalToConstant: 200).isActive = true
        horizontalView4.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        horizontalView4.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: 2200)
    }


}

