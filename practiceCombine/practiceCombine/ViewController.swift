//
//  ViewController.swift
//  practiceCombine
//
//  Created by 김윤석 on 2021/09/21.
//

import UIKit
import Combine

extension Notification.Name {
    static let newBlogPost = Notification.Name(rawValue: "newBlogPost")
}

struct BlogPost {
    let title: String
}

class ViewController: UIViewController, UITextFieldDelegate {

    private lazy var blogTextField: UITextField = {
       let tf = UITextField()
        tf.backgroundColor = .red
        tf.heightAnchor.constraint(equalToConstant: 50).isActive = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.addTarget(self, action: #selector(blogTextFieldChanged), for: .editingChanged)
        return tf
    }()
    
    @objc func blogTextFieldChanged(_ sender: UITextField) {
        blog = sender.text ?? ""
    }
    
    private lazy var passwordTextField: UITextField = {
       let tf = UITextField()
        tf.backgroundColor = .red
        tf.heightAnchor.constraint(equalToConstant: 50).isActive = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.addTarget(self, action: #selector(passwordTextFieldChanged), for: .editingChanged)
        return tf
    }()
    
    @objc private func passwordTextFieldChanged(_ sender: UITextField) {
        password = sender.text ?? ""
    }
    
    private lazy var confirmPsswordTextField: UITextField = {
       let tf = UITextField()
        tf.backgroundColor = .red
        tf.heightAnchor.constraint(equalToConstant: 50).isActive = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.addTarget(self, action: #selector(confirmPasswordTextFieldChanged), for: .editingChanged)
        return tf
    }()
    
    @objc private func confirmPasswordTextFieldChanged( _ sender: UITextField) {
        confirmPassword = sender.text ?? ""
    }
    
    private lazy var publishButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Publish", for: .normal)
        button.setTitleColor(.label, for: .normal)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(buttonDidTap), for: .touchUpInside)
        return button
    }()
    
//    private lazy var subscribedLabel: UILabel = {
//        let label = UILabel()
//        label.backgroundColor = .systemPink
//        label.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
    
    @objc private func buttonDidTap() {
        print("published")
        print(blog)
        print(password)
        print(confirmPassword)
    }
    
    private var validToSubmit: AnyPublisher<(Bool, UIColor?), Never> {
        return Publishers.CombineLatest3($blog, $password, $confirmPassword)
            .map { blog, password, confirmpassword in
                if !blog.isEmpty && !password.isEmpty && !confirmpassword.isEmpty && password == confirmpassword{
                    return (true, .systemOrange) //.systemBlue
                }
                return (false, .systemGray)
            }.eraseToAnyPublisher()
    }
    
    private var validateToChangeColor: AnyPublisher<(UIColor?), Never> {
        return Publishers.CombineLatest3($blog, $password, $confirmPassword)
            .map { blog, password, confirmpassword in
                if !blog.isEmpty && !password.isEmpty && !confirmpassword.isEmpty && password == confirmpassword{
                    return (.systemOrange) //.systemBlue
                }
                return (.systemGray)
            }.eraseToAnyPublisher()
    }
    
    private var buttonSubscriber: AnyCancellable?
    
    @Published var blog = ""
    @Published var password = ""
    @Published var confirmPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        blogTextField.delegate = self
        
        buttonSubscriber = validToSubmit
            .receive(on: RunLoop.main)
            .assign(to: (UIButton().isEnabled, UIButton().backgroundColor), on: publishButton)
        
        buttonSubscriber = validateToChangeColor
            .receive(on: RunLoop.main)
            .assign(to: \.backgroundColor, on: publishButton)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            let textFieldText = textField.text ?? ""
            let text = (textFieldText as NSString).replacingCharacters(in: range, with: string)
                
            if textField == blogTextField {
                blog = text
                print(text)
            }
            if textField == passwordTextField { password = text }
            if textField == confirmPsswordTextField { confirmPassword = text }
        
        return true
    }
    
    private func setupUI() {
        
        let stackView = UIStackView(arrangedSubviews: [blogTextField, passwordTextField, confirmPsswordTextField, publishButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            stackView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
    }
}

