//
//  Extension.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/30.
//

import UIKit

extension UIView {
    func addSubviews(_ views: UIView...) {
        for view in views {
            addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
        }
    }
}
