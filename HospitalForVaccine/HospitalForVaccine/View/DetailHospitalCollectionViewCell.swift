//
//  DetailHospitalCollectionViewCell.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/31.
//

import UIKit

class DetailHospitalCollectionViewCell: UICollectionViewCell {
    static let identifier = "DetailHospitalCollectionViewCell"
    
    private lazy var imageView: UIImageView = {
       let imageView = UIImageView()
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var contentsLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let imageSideLength: CGFloat = 50
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 8
        configureUI()
    }
    
    
    private func configureUI() {
        let stackView = UIStackView(arrangedSubviews: [imageView, titleLabel, contentsLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        
        addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.widthAnchor.constraint(equalToConstant: frame.width)
        ])
    }
    
    func configure(with centerInfo: DetailHospitalViewModel) {
        DispatchQueue.main.async {
            self.imageView.image     = centerInfo.imageView.image
            self.titleLabel.text     = centerInfo.title
            self.contentsLabel.text  = centerInfo.contents
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
