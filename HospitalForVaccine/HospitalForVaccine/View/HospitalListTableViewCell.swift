//
//  HospitalListTableViewCell.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/30.
//

import UIKit

class HospitalListTableViewCell: UITableViewCell {
    static let identifier = "HospitalListTableViewCell"
    static let prefferedHeight: CGFloat = 150
    
    private lazy var centerNameContentLabel: UILabel = {
       let label = UILabel()
        return label
    }()
    
    private lazy var facilityNameContentLabel: UILabel = {
       let label = UILabel()
        return label
    }()
    
    private lazy var addressContentLabel: UILabel = {
       let label = UILabel()
        return label
    }()
    
    private lazy var updateAtContentLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureUI()
    }
    
    func configure(with data: HospitalListViewModel) {
        DispatchQueue.main.async {
            self.centerNameContentLabel.text     = data.centerName
            self.facilityNameContentLabel.text   = data.facilityName
            self.addressContentLabel.text        = data.address
            self.updateAtContentLabel.text       = data.updatedAt
        }
    }
    
    private func configureUI() {
        let centerNameLabel     = UILabel()
        let facilityNameLabel   = UILabel()
        let addressLabel        = UILabel()
        let updateAtLabel       = UILabel()
        
        centerNameLabel.text    = "센터명"
        facilityNameLabel.text  = "건물명"
        addressLabel.text       = "주소"
        updateAtLabel.text      = "업데이트 시간"
        
        let labelStackView = UIStackView(arrangedSubviews: [centerNameLabel, facilityNameLabel, addressLabel, updateAtLabel])
        labelStackView.axis = .vertical
        labelStackView.distribution = .equalSpacing
        labelStackView.spacing = 4
        
        let contentsLabelStackView = UIStackView(arrangedSubviews: [centerNameContentLabel, facilityNameContentLabel, addressContentLabel, updateAtContentLabel])
        contentsLabelStackView.axis = .vertical
        contentsLabelStackView.distribution = .equalSpacing
        contentsLabelStackView.spacing = 4
        
        addSubviews(labelStackView, contentsLabelStackView)
        let padding: CGFloat = 8
        
        NSLayoutConstraint.activate([
            labelStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            labelStackView.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            labelStackView.widthAnchor.constraint(equalToConstant: frame.width/3),
            labelStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            
            contentsLabelStackView.leadingAnchor.constraint(equalTo: labelStackView.trailingAnchor),
            contentsLabelStackView.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            contentsLabelStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            contentsLabelStackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        centerNameContentLabel.text     = nil
        facilityNameContentLabel.text   = nil
        addressContentLabel.text        = nil
        updateAtContentLabel.text       = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
