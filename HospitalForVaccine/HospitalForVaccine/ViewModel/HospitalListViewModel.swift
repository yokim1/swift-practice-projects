//
//  ViewModel.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/09/01.
//

import Foundation

struct HospitalListViewModel {
    let centerName: String
    let facilityName: String
    let address: String
    let updatedAt: String
    
    init(model: VacinneCenterInfo) {
        centerName      = model.centerName
        facilityName    = model.facilityName
        address         = model.address
        updatedAt       = model.updatedAt
    }
}


