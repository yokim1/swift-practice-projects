//
//  ViewModel.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/09/01.
//

import UIKit

struct DetailHospitalViewModel: Equatable {
    let imageView: UIImageView
    let title: String
    let contents: String
    
    init(imageView: UIImageView, title: String, contents: String){
        self.imageView  = imageView
        self.title      = title
        self.contents   = contents
    }
}
