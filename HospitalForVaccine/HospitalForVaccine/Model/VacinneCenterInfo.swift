//
//  HospitalVaccineLocation.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/30.
//

import Foundation

struct CenterLocationResponse: Codable {
    let currentCount: Int
    let data: [VacinneCenterInfo]
}

struct VacinneCenterInfo: Codable {
    let id: Int
    
    let centerName: String
    
    let sido: String
    let sigungu: String
  
    let facilityName: String

    let zipCode: String
    let address: String
  
    let lat: String
    let lng: String
  
    let createdAt: String
    let updatedAt: String
    let centerType: String

    let org: String

    let phoneNumber: String
}
