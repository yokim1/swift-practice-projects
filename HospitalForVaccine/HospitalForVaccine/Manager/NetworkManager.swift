//
//  NetworkManager.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/30.
//

import UIKit

/// API Errors
enum NetworkError: Error {
    case noDataReturned
    case invalidUrl
}

/// Constants
struct NetworkConstants {
    static let apiKey = "&serviceKey=0IPKAzuqgRL%2BlJLFKeV4YTkgTV%2B90JKXrqf81CWgLhY%2BmtJBbP61uC7JH%2BiiYAlIfP2cFE7bKY1vmI5MGX45Pg%3D%3D"
    static let baseUrl = "https://api.odcloud.kr/api/15077586/v1/"
}

enum Endpoint: String {
    case centers = "centers"
}

class NetworkManager {
    static let shared = NetworkManager()
    
    public func fetchCenterLocation(for page: Int, contentsPerpage: Int = 10, completion: @escaping (Result<CenterLocationResponse, Error>) -> Void) {
        
        request(url: url(for: .centers, queryParams: ["page": "\(page)", "perPage": "\(contentsPerpage)"]),
                expecting: CenterLocationResponse.self, completion: completion)
    }
    
    private func url( for endpoint: Endpoint, queryParams: [String: String] = [:]) -> URL? {
        var urlString = NetworkConstants.baseUrl + endpoint.rawValue

        var queryItems = [URLQueryItem]()
        // Add any parameters
        for (name, value) in queryParams {
            queryItems.append(.init(name: name, value: value))
        }

        // Add token
        queryItems.append(.init(name: "token", value: NetworkConstants.apiKey))

        // Convert queri items to suffix string
        urlString += "?" + queryItems.map { "\($0.name)=\($0.value ?? "")" }.joined(separator: "&")
        
        return URL(string: urlString)
    }

    private func request<T: Codable>( url: URL?, expecting: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        guard let url = url else {
            // Invalid url
            completion(.failure(NetworkError.invalidUrl))
            return
        }

        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.failure(NetworkError.noDataReturned))
                }
                return
            }

            do {
                let result = try JSONDecoder().decode(expecting, from: data)
                completion(.success(result))
            }
            catch {
                completion(.failure(error))
            }
        }

        task.resume()
    }
    
    /// Private constructor
    private init() {}
    
}
