//
//  HospitalMapViewController.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/30.
//

import MapKit
import CoreLocation
import UIKit

class HospitalMapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    private lazy var mapView = MKMapView()
    private let manager = CLLocationManager()
    
    private lazy var myLocationButton: UIButton = {
       let button = UIButton()
        button.setTitle("My Location", for: .normal)
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(myLocationButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    private lazy var CenterLocationButton: UIButton = {
       let button = UIButton()
        button.setTitle("Center Location", for: .normal)
        button.backgroundColor = .systemRed
        button.addTarget(self, action: #selector(centerLocationDidTap), for: .touchUpInside)
        return button
    }()
    
    private var myLocation: CLLocationCoordinate2D?
    
    var vacinneCenterInfo: VacinneCenterInfo?
    
    //MARK: - Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMapView()
        configureLocationManager()
        configureButtons()
    }
    
    private func configureMapView() {
        view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .followWithHeading
        
        NSLayoutConstraint.activate([
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func configureLocationManager() {
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    private func configureButtons() {
        
        let stackView = UIStackView(arrangedSubviews: [myLocationButton, CenterLocationButton])
        stackView.spacing = 8
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        mapView.addSubview(stackView)
        
        let padding: CGFloat = 50
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: mapView.leadingAnchor, constant: padding),
            stackView.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -padding),
            stackView.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -padding),
        ])
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            manager.stopUpdatingLocation()
            
            self.myLocation = location.coordinate
            
            locateCenter()
        }
    }
    
    //MARK: - Selector
    
    @objc private func myLocationButtonDidTap() {
        locateUser()
    }
    
    @objc private func centerLocationDidTap() {
        locateCenter()
    }
    
    private func locateUser() {
        guard let lat = self.myLocation?.latitude,
              let lng = self.myLocation?.longitude else {return }
        let targetCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: targetCoordinate, span: span)
        
        mapView.setRegion(region, animated: true)
        
//        let pin = MKPointAnnotation()
//        pin.coordinate = targetCoordinate
//
//        mapView.addAnnotation(pin)
    }
    
    private func locateCenter() {
        guard let lat = Double(vacinneCenterInfo?.lat ?? ""),
              let lng = Double(vacinneCenterInfo?.lng ?? "") else {return }
        
        let targetCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: targetCoordinate, span: span)
        
        mapView.setRegion( region, animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = targetCoordinate
        pin.title = vacinneCenterInfo?.centerName
        
        mapView.addAnnotation(pin)
    }
}
