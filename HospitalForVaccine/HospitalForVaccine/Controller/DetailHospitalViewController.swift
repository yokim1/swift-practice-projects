//
//  ViewController.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/30.
//

import UIKit

class DetailHospitalViewController: UIViewController {

    
    lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.register(DetailHospitalCollectionViewCell.self, forCellWithReuseIdentifier: DetailHospitalCollectionViewCell.identifier)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .systemGray4
        cv.frame = view.bounds
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    
    private let padding: CGFloat = 15
    
    //MARK: - Model
    var centerInfo: VacinneCenterInfo?
    
    //MARK: - ViewModel
    private var viewModels: [DetailHospitalViewModel] = []
    
    //MARK: - Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        
        configureNavigationRightBarButton()
        createViewModel()
    }
    
    private func configureNavigationRightBarButton(){
        let mapButton = UIBarButtonItem(title: "지도", style: .done, target: self, action: #selector(mapButtonDidTap))
        
        navigationItem.rightBarButtonItems = [mapButton]
    }
    
    private func createViewModel() {
        guard let centerInfo = centerInfo else {return }
        
        viewModels.append(.init(
                            imageView: UIImageView(image: UIImage(named: "hospital")),
                            title: "센터명",
                            contents: centerInfo.centerName))
        viewModels.append(.init(
                            imageView: UIImageView(image: UIImage(named: "building")),
                            title: "건물명",
                            contents: centerInfo.facilityName))
        viewModels.append(.init(
                            imageView: UIImageView(image: UIImage(named: "telephone")),
                            title: "전화번호",
                            contents: centerInfo.phoneNumber))
        viewModels.append(.init(
                            imageView: UIImageView(image: UIImage(named: "chat")),
                            title: "업데이트 시간",
                            contents: centerInfo.updatedAt))
        viewModels.append(.init(
                            imageView: UIImageView(image: UIImage(named: "placeholder")),
                            title: "주소",
                            contents: centerInfo.address))
    }
    
    //MARK: - Selector
    @objc func mapButtonDidTap() {
        let vc = HospitalMapViewController()
        vc.vacinneCenterInfo = centerInfo
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension DetailHospitalViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailHospitalCollectionViewCell.identifier, for: indexPath) as? DetailHospitalCollectionViewCell else {return UICollectionViewCell()}
        cell.configure(with: viewModels[indexPath.item])
        return cell
    }
}

extension DetailHospitalViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemPadding = padding * 2
        
        if viewModels[indexPath.item] == viewModels.last {
            return .init(width: view.frame.width - itemPadding, height: view.frame.height/4)
        }
        return .init(width: view.frame.width/2 - itemPadding, height: view.frame.height/4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        .init(top: padding, left: padding, bottom: padding, right: padding)
    }
}
