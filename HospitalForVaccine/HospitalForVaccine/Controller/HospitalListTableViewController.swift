//
//  ViewController.swift
//  HospitalForVaccine
//
//  Created by 김윤석 on 2021/08/30.
//

import UIKit

class HospitalListTableViewController: UITableViewController {

    //MARK: - Property
    private var page: Int = 1
    private var isLoadingCenterInfo = false
    
    //MARK: - Model
    private var vacinneCenters = [VacinneCenterInfo]()
    
    //MARK: - ViewModel
    private var viewModels = [HospitalListViewModel]()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "예방접종센터 리스트"
        
        navigationController?.navigationBar.isTranslucent = false
        
        configureTableView()
        fetchData()
    }
    
    private func fetchData(page: Int = 1) {
        isLoadingCenterInfo = true
        NetworkManager.shared.fetchCenterLocation(for: page) {[weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                self.vacinneCenters.append(contentsOf: response.data)
                DispatchQueue.main.async {
                    self.createViewModel()
                    self.tableView.reloadData()
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
            
            self.isLoadingCenterInfo = false
        }
    }
    
    private func createViewModel() {
        var viewModels = [HospitalListViewModel]()
        
        vacinneCenters.forEach { viewModels.append(.init(model: $0))}
        
        self.viewModels = viewModels.sorted(by: { $0.updatedAt > $1.updatedAt })
    }
    
    private func configureTableView() {
        tableView.register(HospitalListTableViewCell.self, forCellReuseIdentifier: HospitalListTableViewCell.identifier)
        tableView.rowHeight = HospitalListTableViewCell.prefferedHeight
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let offsetY       = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height        = scrollView.frame.size.height
        
        if offsetY > contentHeight - height {
            if isLoadingCenterInfo == false {
                page += 1
                fetchData(page: page)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        vacinneCenters.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HospitalListTableViewCell.identifier, for: indexPath) as? HospitalListTableViewCell else { return UITableViewCell()}
        cell.configure(with: viewModels[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailHospitalViewController()
        vc.centerInfo = vacinneCenters[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

