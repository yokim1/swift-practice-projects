//
//  DetailProRoutineViewControllerDelegate.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/25.
//

import UIKit

extension DetailProRoutineViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        TodoManager.shared.createTodo(contents: todos[indexPath.row].contents, count: todos[indexPath.row].count)
    }
}
