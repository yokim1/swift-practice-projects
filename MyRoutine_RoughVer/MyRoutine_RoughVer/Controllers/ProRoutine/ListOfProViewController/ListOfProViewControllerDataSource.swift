//
//  ListOfProViewControllerDataSource.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/27.
//
import UIKit

extension ListOfProViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namesOfPro.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProNameCell") as? ProNameCell else { return UITableViewCell() }
        cell.nameOfProLabel.text = namesOfPro[indexPath.row]
        return cell
    }
}
