//
//  ListOfProViewController.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/27.
//

import UIKit

class ListOfProViewController: UIViewController {
    override func viewDidLoad() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.navigationItem.title = "Pro"
    }
    
    @IBOutlet weak var tableView: UITableView!
    var namesOfPro = ["조준", "황철순"]
}
