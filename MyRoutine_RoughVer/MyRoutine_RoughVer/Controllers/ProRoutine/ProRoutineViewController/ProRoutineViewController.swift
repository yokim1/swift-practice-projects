//
//  File.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/21.
//

import UIKit

class ProRoutineViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        
        self.navigationItem.title = "Work Out"
    }
    
    @IBOutlet weak var tableview: UITableView!
    
    var selectedProName: String = ""
    
    let listOfBodyPart = ["가슴", "등", "어깨", "다리"]
    
    let ChoJunWorkOutSectionedByBodyParts = [
        ["가슴 전면 운동", "남자는 가슴으로 말한다", "옷 핏 만드는 가슴 운동"],  //chest
        ["프레임 넓히기", "등판 커지는 지름길"],                           // back
        ["50분만에 어깨 깡패 만들기", "핏 사는 넓은 어깨 운동"],               // Shoulder
    ]
    
    let HwangChulSoonWorkOutSectionedByBodyParts = [
        ["볼륨감을 줄수 있는 가슴 운동", "가슴 안쪽 채우기 운동", "비시즌 가슴 운동"],
        ["등 두께 운동","등 상부 운동", "등 근육 세분화"],
        ["어깨 후면 운동", "어깨 프레임 잡기"],
        ["다리 터트리기 루틴"]
    ]
    
   
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(listOfBodyPart[section])"
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedProName == "조준" {
            return ChoJunWorkOutSectionedByBodyParts.count
        }
        else if selectedProName == "황철순"{
            return HwangChulSoonWorkOutSectionedByBodyParts.count
        }
        return 0
    }
}
