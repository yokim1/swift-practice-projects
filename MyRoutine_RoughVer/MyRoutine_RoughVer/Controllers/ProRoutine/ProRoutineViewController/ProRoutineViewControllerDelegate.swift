//
//  ProRoutineViewControllerDelegate.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/25.
//

import UIKit

extension ProRoutineViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailProRoutineViewController = storyboard?.instantiateViewController(identifier: "DetailProRoutineViewController") as? DetailProRoutineViewController else { return }
        
        if selectedProName == "조준" {
            detailProRoutineViewController.title = ChoJunWorkOutSectionedByBodyParts[indexPath.section][indexPath.row]
            detailProRoutineViewController.proName = self.selectedProName
            detailProRoutineViewController.targetMuscle = self.ChoJunWorkOutSectionedByBodyParts[indexPath.section][indexPath.row]
        }
        else if selectedProName == "황철순" {
            detailProRoutineViewController.title = HwangChulSoonWorkOutSectionedByBodyParts[indexPath.section][indexPath.row]
            detailProRoutineViewController.proName = self.selectedProName
            detailProRoutineViewController.targetMuscle = self.HwangChulSoonWorkOutSectionedByBodyParts[indexPath.section][indexPath.row]
        }
        
        self.navigationController?.pushViewController(detailProRoutineViewController, animated: true)
    }
}
