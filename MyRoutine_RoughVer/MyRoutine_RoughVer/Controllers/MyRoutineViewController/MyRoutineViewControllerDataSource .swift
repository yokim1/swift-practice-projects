//
//  MyRoutineViewControllerDataSource .swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/22.
//

import UIKit

extension MyRoutineViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TodoManager.shared.todos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyRoutineCell", for: indexPath) as? MyRoutineCell else {return UICollectionViewCell()}
        
        cell.taskLabel.text = TodoManager.shared.todos[indexPath.row].contents
        
        cell.updateUI(for: TodoManager.shared.todos[indexPath.row])
        
        buttonTapHandlers(for: cell, at: indexPath)
        
        return cell
    }
}

private extension MyRoutineViewController {
    func buttonTapHandlers(for cell: MyRoutineCell, at indexPath: IndexPath) {
        
        cell.completeButtonTapHandler = {
            TodoManager.shared.todos[indexPath.row].isCompleted = !TodoManager.shared.todos[indexPath.row].isCompleted
            cell.completeButton.isSelected = !cell.completeButton.isSelected
            
            self.countCompletedWorkout(cell: cell)
            
            cell.taskLabel.alpha = cell.completeButton.isSelected ? 0.2 : 1
            Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
        }
        
        cell.deleteButtonTapHandler = {
            TodoManager.shared.todos.remove(at: indexPath.row)
            
            TodoManager.shared.decreaseNumberOfCompletedWorkout()
            
            Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
            self.collectionView.reloadData()
        }
    }
    
    func countCompletedWorkout(cell: MyRoutineCell) {
        if cell.completeButton.isSelected == true {
            TodoManager.shared.increaseNumberOfCompletedWorkout()
        }
        else if cell.completeButton.isSelected == false {
            
            TodoManager.shared.decreaseNumberOfCompletedWorkout()
        }
        
        print(TodoManager.shared.numberOfCompletedWorkout)
        
        if TodoManager.shared.numberOfCompletedWorkout == TodoManager.shared.todos.count {
            self.showactionSheet()
        }
    }
    
    func showactionSheet(){
        let actionSheet = UIAlertController(title: "Finished!!", message: "Do you want to Delete all of the finished Workout", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            print("Clear Them")
            TodoManager.shared.todos = []
            TodoManager.shared.numberOfCompletedWorkout = 0
            Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
            self.collectionView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            print("Keep Them")
        }))
        
        present(actionSheet, animated: true)
    }
}
