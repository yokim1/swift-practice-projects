//
//  TableViewController.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/04/01.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    

    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var inputViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tableViewBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(adjustInputViewPositionAndCollectionViewSize), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustInputViewPositionAndCollectionViewSize), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        TodoManager.shared.todos = Storage.retrive("todos.json", from: .documents, as: [Todo].self) ?? []
        
        for index in 0..<TodoManager.shared.todos.count {
            if TodoManager.shared.todos[index].isCompleted {
                TodoManager.shared.increaseNumberOfCompletedWorkout()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        animateTable()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        TodoManager.shared.todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyRoutineCell1", for: indexPath) as? MyRoutineCell1 else {return UITableViewCell()}
        
        cell.taskLabel.text = TodoManager.shared.todos[indexPath.row].contents
        
        cell.updateUI(for: TodoManager.shared.todos[indexPath.row])
        
        buttonTapHandlers(for: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tableView.beginUpdates()
            
            TodoManager.shared.todos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            tableView.endUpdates()
        }
    }
    
    func animateTable() {
        tableView.reloadData()
        let cells = tableView.visibleCells
        let tableViewHeight = tableView.bounds.size.height
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delayCounter = 0
        for cell in cells {
            UIView.animate(withDuration: 1.75,
                           delay: Double(delayCounter) * 0.05,
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: {cell.transform = CGAffineTransform.identity },
                           completion: nil)
            delayCounter += 1
        }
    }
}

extension TableViewController {
    @IBAction func backGroundDidTappedToReleaseKeyBoard(_ sender: Any) {
        inputTextField.resignFirstResponder()
    }
}

extension TableViewController {
    @objc func adjustInputViewPositionAndCollectionViewSize(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        // 키보드 높이에 따른 인풋뷰 위치 변경
        guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillShowNotification {
            let adjustmentHeight = keyboardFrame.height - view.safeAreaInsets.bottom
            
            print(inputViewBottom.constant)
            inputViewBottom.constant = adjustmentHeight
            print(inputViewBottom.constant)
            
            print(tableViewBottom.constant)
            tableViewBottom.constant = (adjustmentHeight + 30) * -1
            print(tableViewBottom.constant)
            
        } else {
            inputViewBottom.constant = 0
            
            tableViewBottom.constant = (30) * -1
        }
    }
}


private extension TableViewController {
    func buttonTapHandlers(for cell: MyRoutineCell1, at indexPath: IndexPath) {
        
        cell.completeButtonTapHandler = {
            TodoManager.shared.todos[indexPath.row].isCompleted = !TodoManager.shared.todos[indexPath.row].isCompleted
            cell.completeButton.isSelected = !cell.completeButton.isSelected
            
            self.countCompletedWorkout(cell: cell)
            
            cell.taskLabel.alpha = cell.completeButton.isSelected ? 0.2 : 1
            Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
        }
        
        cell.deleteButtonTapHandler = {
            TodoManager.shared.todos.remove(at: indexPath.row)
            
            TodoManager.shared.decreaseNumberOfCompletedWorkout()
            
            Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
            self.tableView.reloadData()
        }
    }
    
    func countCompletedWorkout(cell: MyRoutineCell1) {
        if cell.completeButton.isSelected == true {
            TodoManager.shared.increaseNumberOfCompletedWorkout()
        }
        else if cell.completeButton.isSelected == false {
            
            TodoManager.shared.decreaseNumberOfCompletedWorkout()
        }
        
        print(TodoManager.shared.numberOfCompletedWorkout)
        
        if TodoManager.shared.numberOfCompletedWorkout == TodoManager.shared.todos.count {
            self.showactionSheet()
        }
    }
    
    func showactionSheet(){
        let actionSheet = UIAlertController(title: "Finished!!", message: "Do you want to Delete all of the finished Workout", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            print("Clear Them")
            TodoManager.shared.todos = []
            TodoManager.shared.numberOfCompletedWorkout = 0
            Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
            self.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            print("Keep Them")
        }))
        
        present(actionSheet, animated: true)
    }
}

class MyRoutineCell1: UITableViewCell {
    
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func completeButtonDidTapped(_ sender: Any) {
        completeButtonTapHandler?()
    }
    
    @IBAction func deleteButtonDidTapped(_ sender: Any) {
        deleteButtonTapHandler?()
    }
    
    var completeButtonTapHandler: (() -> Void)?
    var deleteButtonTapHandler: (() -> Void)?
    
    
    func updateUI(for todo: Todo){
        completeButton.isSelected = todo.isCompleted
        taskLabel.text = todo.contents
        taskLabel.alpha = todo.isCompleted ? 0.2 : 1
    }
}
