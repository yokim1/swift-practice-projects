//
//  ListOfWorkOut.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/23.
//

import Foundation

struct WorkOutList {
    let benchPress = "BenchPress"
    let chestFly = "ChestFly"
    let dips = "Dips"
    
    let pullUp = "PullUp"
    let row = "Row"
    let deadLift = "DeadLift"
    
    let squat = "Squat"
    
    let typesOfWorkOut: String
    
    init(typesOfWorkOut: String) {
        self.typesOfWorkOut = typesOfWorkOut
    }
    
    func getWorkOuts() -> [Todo] {
        switch(typesOfWorkOut) {
        
        case "Chest":
            return [Todo(contents: benchPress, count: 4, isCompleted: false),
                    Todo(contents: chestFly, count: 4, isCompleted: false),
                    Todo(contents: dips, count: 4, isCompleted: false)]
            
        case "Back":
            return [Todo(contents: deadLift, count: 4, isCompleted: false),
                    Todo(contents: pullUp, count: 4, isCompleted: false),
                    Todo(contents: row, count: 4, isCompleted: false)]
        case "Leg":
            return [Todo(contents: squat, count: 4, isCompleted: false)]
            
        default:
            break
        }
        return []
    }
}
