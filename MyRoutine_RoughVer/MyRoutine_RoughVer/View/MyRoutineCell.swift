//
//  MyRoutineCell.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/22.
//

import UIKit

class MyRoutineCell: UICollectionViewCell {
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    var completeButtonTapHandler: (() -> Void)?
    var deleteButtonTapHandler: (() -> Void)?

    @IBAction func completeButtonDidTapped(_ sender: Any) {
        completeButtonTapHandler?()
    }

    @IBAction func deleteButtonDidTapped(_ sender: Any) {
        deleteButtonTapHandler?()
    }

    func updateUI(for todo: Todo){
        completeButton.isSelected = todo.isCompleted
        taskLabel.text = todo.contents
        taskLabel.alpha = todo.isCompleted ? 0.2 : 1
    }
}

