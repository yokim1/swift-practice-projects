//
//  ViewController.swift
//  UpBitClone
//
//  Created by 김윤석 on 2021/09/05.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    lazy var tableView: UITableView = {
       let tv = UITableView()
        let uv = UIView()
        uv.backgroundColor = .red
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: view.frame.height/3, right: 0)
        return tv
    }()
    
    var coinPreviewInfo = [CoinPreviewInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMockData()
        
        configureNavigationController()
        configureNavigationRightButtons()
        configureTableView()
    }
    
    private func configureNavigationController() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 18/255, green: 50/255, blue: 133/255, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func configureNavigationRightButtons() {
        let chatButton = UIBarButtonItem(image: UIImage(systemName: "text.bubble.rtl"), style: .done, target: self, action: #selector(settingButtonDidTap))
    
        let settingButton = UIBarButtonItem(image: UIImage(systemName: "gearshape"), style: .done, target: self, action: #selector(settingButtonDidTap))
        
        navigationItem.rightBarButtonItems = [chatButton, settingButton]
        
        navigationItem.rightBarButtonItems?.forEach({ button in
            button.tintColor = UIColor(red: 18/255, green: 50/255, blue: 133/255, alpha: 1)
        })
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        let uv = HeaderView()
        /// Instead of defining CGRect
        /// I used dynamic height
        //uv.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/10)
        
        tableView.tableHeaderView = uv
        
        //I used dynamic height to make the views fit with autolayout
        if let headerView = tableView.tableHeaderView {

                let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
                var headerFrame = headerView.frame
                
                //Comparison necessary to avoid infinite loop
                //if height != headerFrame.size.height {
                    headerFrame.size.height = height
                    headerView.frame = headerFrame
                    tableView.tableHeaderView = headerView
                //}
        }
        
        tableView.register(CoinTableViewCell.self, forCellReuseIdentifier: CoinTableViewCell.identifier)
        
        tableView.frame = view.bounds
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        coinPreviewInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CoinTableViewCell.identifier, for: indexPath) as? CoinTableViewCell else { return UITableViewCell()}
        cell.configure(with: coinPreviewInfo[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let uv = SectionView()
        uv.backgroundColor = .systemBackground
        return uv
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        SectionView.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        CoinTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - Selector
    @objc func settingButtonDidTap() {
        
    }

}

//MARK: - configure Mock Data
extension HomeViewController{
    private func configureMockData() {
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "헌트", displayName: "Hunt/KRW", price: "1,325", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "리플", displayName: "XRP/KRW", price: "550,787,000", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "비트코인", displayName: "BTC/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "아이오에스티", displayName: "IOST/KRW", price: "2,870", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "세럼", displayName: "SRM/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "헌트", displayName: "Hunt/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "리플", displayName: "XRP/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "비트코인", displayName: "BTC/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "아이오에스티", displayName: "IOST/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "세럼", displayName: "SRM/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "헌트", displayName: "Hunt/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "리플", displayName: "XRP/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "비트코인", displayName: "BTC/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "아이오에스티", displayName: "IOST/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "세럼", displayName: "SRM/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "헌트", displayName: "Hunt/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "리플", displayName: "XRP/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "비트코인", displayName: "BTC/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "아이오에스티", displayName: "IOST/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
        coinPreviewInfo.append(
            CoinPreviewInfo(name: "세럼", displayName: "SRM/KRW", price: "970", changePercentage: "80.63%", priceChanage: "433", totalPrice: "2021879백만"))
    }
}
