//
//  File.swift
//  UpBitClone
//
//  Created by 김윤석 on 2021/09/06.
//

import UIKit

class CoinTableViewCell: UITableViewCell {
    static let identifier = "coinTableViewCell"
    static let height: CGFloat = 60
    
    let nameLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let dispalyNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = .lightGray
        return label
    }()
    
    let priceLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.textAlignment = .right
        return label
    }()
    
    let changePricePercentageLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        return label
    }()
    
    let changePriceLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    let totalLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    private func configureUI() {
        let nameLabelsStackView = UIStackView(arrangedSubviews: [nameLabel, dispalyNameLabel])
        nameLabelsStackView.axis = .vertical
        nameLabelsStackView.spacing = 3
        nameLabelsStackView.distribution = .fillProportionally
        
        addSubview(nameLabelsStackView)
        nameLabelsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            nameLabelsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            nameLabelsStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
        
        //거래 대금 label
        addSubview(totalLabel)
        totalLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            totalLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            totalLabel.bottomAnchor.constraint(equalTo: nameLabelsStackView.centerYAnchor)
        ])
        
        //전일 대비 label
        let changeLabelsStackView = UIStackView(arrangedSubviews: [changePricePercentageLabel, changePriceLabel])
        changeLabelsStackView.axis = .vertical
        changeLabelsStackView.spacing = 3
        changeLabelsStackView.distribution = .fillProportionally
        changeLabelsStackView.alignment = .trailing
        addSubview(changeLabelsStackView)
        changeLabelsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            changeLabelsStackView.trailingAnchor.constraint(equalTo: totalLabel.leadingAnchor, constant: -14),
            changeLabelsStackView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        //Price label
        addSubview(priceLabel)
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            priceLabel.trailingAnchor.constraint(equalTo: centerXAnchor, constant: 13),
            priceLabel.bottomAnchor.constraint(equalTo: nameLabelsStackView.centerYAnchor)
        ])
    }
    
    func configure(with data: CoinPreviewInfo) {
        nameLabel.text = data.name
        dispalyNameLabel.text = data.displayName
        priceLabel.text = data.price
        changePricePercentageLabel.text = data.changePercentage
        changePriceLabel.text = data.priceChanage
        totalLabel.text = data.totalPrice
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
