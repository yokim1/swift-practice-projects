//
//  File.swift
//  UpBitClone
//
//  Created by 김윤석 on 2021/09/06.
//

import UIKit

class HeaderView: UIView {
    
    private lazy var searchBarView: UIView = {
        let view = UIView()
        
        let imageView = UIImageView(image: UIImage(systemName: "magnifyingglass"))
        imageView.tintColor = UIColor(red: 18/255, green: 50/255, blue: 133/255, alpha: 1)
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let tf = searchField
        view.addSubview(tf)
        tf.translatesAutoresizingMaskIntoConstraints = false
        
        let separator = UIView()
        separator.backgroundColor = UIColor(red: 18/255, green: 50/255, blue: 133/255, alpha: 1)
        view.addSubview(separator)
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            imageView.heightAnchor.constraint(equalToConstant: 24),
            imageView.widthAnchor.constraint(equalToConstant: 24),
            
            tf.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8),
            tf.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
            tf.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tf.heightAnchor.constraint(equalTo: imageView.heightAnchor),

            separator.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            separator.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: imageView.frame.height / 2),
            separator.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            separator.heightAnchor.constraint(equalToConstant: 3),
        ])
        
        return view
    }()

    var searchField: UITextField {
        let tf = UITextField()
        tf.attributedPlaceholder = NSAttributedString(string: "코인명/심볼 검색",
                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 18/255, green: 50/255, blue: 133/255, alpha: 0.8)])
         tf.translatesAutoresizingMaskIntoConstraints = false
         return tf
    }
    
    //MARK: - Button
    let krwButton: UIButton = {
        let button = UIButton()
        button.setTitle("KRW", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.setTitleColor(.lightGray, for: .normal)
        return button
    }()
    
    let btcButton: UIButton = {
       let button = UIButton()
        button.setTitle("BTC", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.setTitleColor(.lightGray, for: .normal)
        return button
    }()
    
    let usdtButton: UIButton = {
        let button = UIButton()
        button.setTitle("USDT", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.setTitleColor(.lightGray, for: .normal)
        return button
    }()
    
    let interestButton: UIButton = {
       let button = UIButton()
        button.setTitle("관심", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 12)
        button.setTitleColor(.lightGray, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(searchBarView)
        searchBarView.translatesAutoresizingMaskIntoConstraints = false
        
        let buttons = [krwButton, btcButton, usdtButton, interestButton]
        
        buttons.forEach { button in
            button.layer.borderColor = UIColor.lightGray.cgColor
            button.layer.borderWidth = 1
            button.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(button)
        }
        
        let horizontalStackView = UIStackView(arrangedSubviews: buttons)
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = -2
        horizontalStackView.distribution = .fillEqually
        addSubview(horizontalStackView)
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            searchBarView.leadingAnchor.constraint(equalTo: leadingAnchor),
            searchBarView.trailingAnchor.constraint(equalTo: trailingAnchor),
            searchBarView.topAnchor.constraint(equalTo: topAnchor),
            searchBarView.heightAnchor.constraint(equalToConstant: 50),

            horizontalStackView.topAnchor.constraint(equalTo: searchBarView.bottomAnchor, constant: 4),
            horizontalStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            horizontalStackView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -8),
            horizontalStackView.heightAnchor.constraint(equalToConstant: 30),
            horizontalStackView.widthAnchor.constraint(equalToConstant: 200),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
