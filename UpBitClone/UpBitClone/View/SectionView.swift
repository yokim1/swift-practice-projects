//
//  SectionView.swift
//  UpBitClone
//
//  Created by 김윤석 on 2021/09/06.
//

import UIKit

class SectionView: UIView {
    
    static let height: CGFloat = 30
    
    lazy var nameLabel: UIButton = {
       let button = UIButton()
        button.setTitle("한글명", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.setTitleColor(.systemGray, for: .normal)
        return button
    }()
    
    lazy var priceButton: UIButton = {
       let button = UIButton()
        button.setTitle("현재가", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.setTitleColor(.systemGray, for: .normal)
        return button
    }()
    
    lazy var chanageButton: UIButton = {
       let button = UIButton()
        button.setTitle("전일대비", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.setTitleColor(.systemGray, for: .normal)
        return button
    }()
    
    lazy var totalPriceButton: UIButton = {
       let button = UIButton()
        button.setTitle("거래대금", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.setTitleColor(.systemGray, for: .normal)
        
        return button
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let topSeparator = UIView()
        topSeparator.backgroundColor = .systemGray2
        addSubview(topSeparator)
        topSeparator.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(nameLabel)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.addTarget(self, action: #selector(labelDidTap), for: .touchUpInside)
        
//        let labels = [ priceButton, chanageButton, totalPriceButton]
//        let stackView = UIStackView(arrangedSubviews: labels)
//        stackView.translatesAutoresizingMaskIntoConstraints = false
//        stackView.axis = .horizontal
//        stackView.spacing = 50
//        stackView.distribution = .equalSpacing
//        addSubview(stackView)
        
        addSubview(priceButton)
        priceButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(chanageButton)
        chanageButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(totalPriceButton)
        totalPriceButton.translatesAutoresizingMaskIntoConstraints = false
        
        let bottomSeparator = UIView()
        bottomSeparator.backgroundColor = .systemGray2
        addSubview(bottomSeparator)
        bottomSeparator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            topSeparator.leadingAnchor.constraint(equalTo: leadingAnchor),
            topSeparator.trailingAnchor.constraint(equalTo: trailingAnchor),
            topSeparator.topAnchor.constraint(equalTo: topAnchor),
            topSeparator.heightAnchor.constraint(equalToConstant: 1),
            
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            priceButton.trailingAnchor.constraint(equalTo: centerXAnchor, constant: 13),
            
            totalPriceButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            totalPriceButton.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/5),
            
            chanageButton.trailingAnchor.constraint(equalTo: totalPriceButton.leadingAnchor, constant: -15),
            
            bottomSeparator.leadingAnchor.constraint(equalTo: leadingAnchor),
            bottomSeparator.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottomSeparator.bottomAnchor.constraint(equalTo: bottomAnchor),
            bottomSeparator.heightAnchor.constraint(equalToConstant: 1),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func labelDidTap() {
        print("123")
    }
}
