//
//  Models.swift
//  UpBitClone
//
//  Created by 김윤석 on 2021/09/06.
//

import Foundation

struct CoinPreviewInfo {
    let name: String
    let displayName: String
    let price: String
    let changePercentage: String
    let priceChanage: String
    let totalPrice: String
}
