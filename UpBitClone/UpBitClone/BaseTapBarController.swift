//
//  File.swift
//  UpBitClone
//
//  Created by 김윤석 on 2021/09/09.
//

import UIKit

class BaseTapBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = [
            configureTabBarItem(for: HomeViewController(),
                                title: "거래소",
                                image: UIImage(systemName: "house"),
                                selectedImage: UIImage(systemName: "house.fill")),
            
            configureTabBarItem(for: UIViewController(),
                                title: "코인정보",
                                image: UIImage(systemName: "chart.bar"),
                                selectedImage: UIImage(systemName: "chart.bar.fill")),
            
            configureTabBarItem(for: UIViewController(),
                                title: "투자내역",
                                image: UIImage(systemName: "note.text"),
                                selectedImage: UIImage(systemName: "note.text")),
            
            configureTabBarItem(for: UIViewController(),
                                title: "입출금",
                                image: UIImage(systemName: "arrow.left.arrow.right"),
                                selectedImage: UIImage(systemName: "arrow.left.arrow.right")),
            
            configureTabBarItem(for: UIViewController(),
                                title: "내정보",
                                image: UIImage(systemName: "person"),
                                selectedImage: UIImage(systemName: "person.fill")),
            
            ]
        
        UITabBar.appearance().barTintColor = UIColor(red: 18/255, green: 50/255, blue: 133/255, alpha: 1)
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = .white
    }
    
    private func configureTabBarItem(for vc: UIViewController, title: String, image: UIImage?, selectedImage: UIImage?) -> UIViewController {
        
        let navController = UINavigationController(rootViewController: vc)
        
        vc.navigationItem.title                    = title
        vc.view.backgroundColor                    = .white
        
        navController.tabBarItem.title             = title
        navController.tabBarItem.image             = image
        navController.tabBarItem.selectedImage     = selectedImage
        return navController
    }
}

