//
//  Constants.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

struct Constants {
    static let separatorPadding: CGFloat = 20
}
