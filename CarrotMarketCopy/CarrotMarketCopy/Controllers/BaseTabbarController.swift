//
//  ViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/16.
//

import UIKit

class BaseTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        UINavigationBar.appearance().tintColor = .black
        UITabBar.appearance().tintColor = .black
        
        view.backgroundColor = .white
        
        viewControllers = [
            configureNavigationController(vc: HomeViewController(),
                                          title: "홈",
                                          image: UIImage(systemName: "house"),
                                          selectedImage: UIImage(systemName: "house.fill")),
            
            configureNavigationController(vc: TownLifeViewController(),
                                          title: "동네생활",
                                          image: UIImage(systemName: "newspaper"),
                                          selectedImage: UIImage(systemName: "newspaper.fill")),
            
            configureNavigationController(vc: MyLocalViewController(),
                                          title: "내 근처",
                                          image: UIImage(systemName: "mappin.circle"),
                                          selectedImage: UIImage(systemName: "mappin.circle.fill")),
            
            configureNavigationController(vc: ChattingViewController(),
                                          title: "채팅",
                                          image: UIImage(systemName: "bubble.left.and.bubble.right"),
                                          selectedImage: UIImage(systemName: "bubble.left.and.bubble.right.fill")),
            
            configureNavigationController(vc: MyCarrotViewController(),
                                          title: "나의 당근",
                                          image: UIImage(systemName: "person"),
                                          selectedImage: UIImage(systemName: "person.fill")),
        ]
    }
    
    fileprivate func configureNavigationController(vc: UIViewController, title: String?, image: UIImage?, selectedImage: UIImage?)-> UINavigationController {
        
        let navVC = UINavigationController(rootViewController: vc)
        navVC.tabBarItem = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        
        return navVC
    }
}

