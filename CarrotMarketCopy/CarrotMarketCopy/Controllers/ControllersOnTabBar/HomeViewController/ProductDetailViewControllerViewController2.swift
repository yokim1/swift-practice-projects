////
////  ProductDetailViewControllerViewController2.swift
////  CarrotMarketCopy
////
////  Created by 김윤석 on 2021/07/17.
////
//
//import UIKit
//
//
//class ProductDetailViewControllerViewController2: UIViewController {
//
//    var productDetail: ProductDetail = ProductDetail(
//        productImages: [
//            ProductImage(productImageUrl: "ProductImage")
//        ],
//
//        sellerProfilePhoto: "",
//        sellerName: "골드바매니아",
//        sellerLocation: "도곡동",
//        sellerTemparture: "40.5",
//
//        productTitle: "골드바 지금 즉시 거래",
//        productCategory: "돈/골드",
//        time: "3분전",
//        productDescription: "골드바 지금 즉시 도곡역에서 직거래 가능합니다. 네고 안됩니다.",
//
//        productPrice: "10,000",
//        priceSuggestion: "가격제안 불가",
//        isLiked: false,
//
//        sellerOtherProduct: [
//            ProductPreview(productImage: "GD.jpeg", productTitle: "GD 팝니다", productPrice: "2,000"),
//            ProductPreview(productImage: "gun.jpg", productTitle: "(미개봉)권총 팝니다", productPrice: "15,000"),
//            ProductPreview(productImage: "gun.jpg", productTitle: "탄창 팝니다.", productPrice: "8,000"),
//        ],
//
//        otherRecommendedProduct: [
//            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
//            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
//            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
//            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
//            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
//            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
//        ])
//
//    let tableView: UITableView = {
//        let tableView = UITableView()
//        tableView.translatesAutoresizingMaskIntoConstraints = false
//        tableView.backgroundColor = .blue
//        return tableView
//    }()
//
//    let buyTabBar = BuyTabBarView()
//
//    var delegate: ProductDetailViewControllerDelegate?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        view.backgroundColor = .red
//
//        self.navigationItem.hidesBackButton = true
//        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(back(sender:)))
//        self.navigationItem.leftBarButtonItem = newBackButton
//
//        configureNavigationBar()
//        configureTableView()
//        configureBuyTapBar()
//    }
//
//    @objc func back(sender: UIBarButtonItem) {
//        delegate?.productDetailViewControllerBackButtonDidTapped()
//    }
//
//    fileprivate func configureNavigationBar() {
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.isTranslucent = true
//        navigationController?.view.backgroundColor = .clear
//    }
//
//    fileprivate func configureTableView() {
//        view.addSubview(tableView)
//        tableView.dataSource = self
//        tableView.delegate = self
//
//        tableView.register(ProductDetailPhotoPagingCell.self, forCellReuseIdentifier: ProductDetailPhotoPagingCell.identifier)
//
//        tableView.register(ProductDetailSellerProfilePreviewCell.self, forCellReuseIdentifier: ProductDetailSellerProfilePreviewCell.identifier)
//
//        tableView.register(ProductDetailProductDescriptionCell.self, forCellReuseIdentifier: ProductDetailProductDescriptionCell.identifier)
//
//        tableView.register(ProductDetailReportThisProductCell.self, forCellReuseIdentifier: ProductDetailReportThisProductCell.identifier)
//
//        tableView.register(ProductDetailSellerOtherProductsCell.self, forCellReuseIdentifier: ProductDetailSellerOtherProductsCell.identifier)
//
//        tableView.register(ProductDetailOtherProductRecommendCell.self, forCellReuseIdentifier: ProductDetailOtherProductRecommendCell.identifier)
//
//        tableView.contentInset = UIEdgeInsets(top: -100, left: 0, bottom: 0, right: 0)
//        tableView.separatorStyle = .none
//        tableView.tableFooterView = UIView(frame: .zero)
//
//        NSLayoutConstraint.activate([
//            tableView.topAnchor.constraint(equalTo: view.topAnchor),
//            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
//            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
//        ])
//    }
//
//    fileprivate func configureBuyTapBar() {
//        buyTabBar.priceLabel.text = productDetail.productPrice
//        buyTabBar.sellerOptionLabel.text = productDetail.priceSuggestion
//
//        view.addSubview(buyTabBar)
//
//        NSLayoutConstraint.activate([
//            buyTabBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//            buyTabBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
//            buyTabBar.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//            buyTabBar.heightAnchor.constraint(equalToConstant: 100),
//        ])
//    }
//}
//
//extension ProductDetailViewControllerViewController2: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        6
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        switch indexPath.row {
//        case 0:
//            guard let cell = collectionView.dequeueReusableCell(withIdentifier: ProductDetailPhotoPagingCell.identifier, for: indexPath) as? ProductDetailPhotoPagingCell  else {return UITableViewCell()}
//            cell.set(productDetail: productDetail)
//            return cell
//
//        case 1:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailSellerProfilePreviewCell.identifier, for: indexPath) as? ProductDetailSellerProfilePreviewCell  else {return UITableViewCell()}
//            cell.set(productDetail: productDetail)
//            return cell
//
//        case 2:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailProductDescriptionCell.identifier, for: indexPath) as? ProductDetailProductDescriptionCell  else {return UITableViewCell()}
//            cell.set(productDetail: productDetail)
//            return cell
//
//        case 3:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailReportThisProductCell.identifier, for: indexPath) as? ProductDetailReportThisProductCell  else {return UITableViewCell()}
//            cell.set(productDetail: productDetail)
//            return cell
//
//        case 4:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailSellerOtherProductsCell.identifier, for: indexPath) as? ProductDetailSellerOtherProductsCell  else {return UITableViewCell()}
//            cell.set(productDetail: productDetail)
//            return cell
//
//        case 5:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailOtherProductRecommendCell.identifier, for: indexPath) as? ProductDetailOtherProductRecommendCell  else {return UITableViewCell()}
//            cell.set(productDetail: productDetail)
//            return cell
//
//        default:
//            return UITableViewCell()
//        }
//    }
//}
//
//extension ProductDetailViewControllerViewController2: UITableViewDelegate {
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//
//        switch indexPath.row {
//        case 0:
//            print("photo")
//
//            break
//
//        case 1:
//            print("seller profile")
//            break
//
//        case 2:
//            print("title, description etc..")
//
//            break
//
//        case 3:
//            print("이 글 신고하기")
//            break
//
//        case 4:
//            print("the seller's other product")
//            break
//
//        case 5:
//            print("recommend product list")
//            break
//
//        default:
//            return
//        }
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch indexPath.row {
//        case 0:
//            return view.frame.width
//
//        case 1:
//            return ProductDetailSellerProfilePreviewCell.prefferedHeight
//
//        case 2:
//            return ProductDetailProductDescriptionCell.prefferedHeight
//
//        case 3:
//            return ProductDetailReportThisProductCell.prefferedHeight
//
//        case 4:
//
//            if productDetail.sellerOtherProduct.isEmpty {
//                return 0
//            }
//
//            return ProductDetailSellerOtherProductsCell.prefferedHeight
//
//        case 5:
//
//            if productDetail.otherRecommendedProduct.isEmpty {
//                return 0
//            }
//
//            break
//
//        default:
//            break
//        }
//
//        return 100
//    }
//}
