//
//  ProductDetailViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

protocol ProductDetailViewControllerDelegate: AnyObject {
    func productDetailViewControllerBackButtonDidTapped()
}

class ProductDetailViewController: UIViewController {
    
    var productDetailVCDelegate: ProductDetailViewControllerDelegate?
    
    let backButton: UIButton = {
        let backButton = UIButton()
        backButton.imageView?.image = UIImage(systemName: "arrow.backward")
        backButton.backgroundColor = .red
        return backButton
    }()
    
    let productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .gray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let tabBar = BuyTabBarView()
    
    let scrollView = UIScrollView()
    let contenView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        configureScrollView()
        configureNavigationBar()
        configureBuyTabBar()
        
        //configureProductImage()
        //configureSellerPreviewInfo()
    }
    
    private func configureScrollView() {
        scrollView.backgroundColor = .green
        contenView.backgroundColor = .blue
        
        view.addSubview(scrollView)
        scrollView.addSubview(contenView)
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contenView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.contentSize = CGSize(width: view.frame.width, height: 3000)
        //contenView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 3000)
        
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            contenView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contenView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contenView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contenView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            
            contenView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor),
            contenView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
        ])
    }
    
    private func configureNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    private func configureBuyTabBar() {
        view.addSubview(tabBar)
        
        NSLayoutConstraint.activate([
            tabBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tabBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tabBar.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tabBar.heightAnchor.constraint(equalToConstant: 100),
        ])
    }
    
    private func configureProductImage() {
        //productImageView.frame = CGRect(x: 0, y: -100, width: view.frame.width, height: 300)
        scrollView.addSubview(productImageView)
//        productImageView.translatesAutoresizingMaskIntoConstraints = false
        
//        NSLayoutConstraint.activate([
//            productImageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
//            productImageView.topAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.topAnchor),
//            productImageView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
//            productImageView.heightAnchor.constraint(equalToConstant: 300),
//        ])
    }
    
    private func configureSellerPreviewInfo() {

        let sellerProfilePhoto = UIImageView(image: UIImage(systemName: "person"))
        
        let sellerName = CMBoldLabel(fontSize: 20)
        let sellerLocation = CMSecondaryLabel(fontSize: 10)
        
        let labelStackView = UIStackView(arrangedSubviews: [ sellerName, sellerLocation])
        labelStackView.axis = .horizontal
        labelStackView.spacing = 12
        labelStackView.distribution = .fillEqually
        
        let profileStackView = UIStackView(arrangedSubviews: [ sellerProfilePhoto, labelStackView])
        
        scrollView.addSubview(profileStackView)
        
        let padding: CGFloat = 12
        
        NSLayoutConstraint.activate([
            profileStackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: padding),
            profileStackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: padding),
            profileStackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -padding),
            profileStackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -padding)
        ])
    }
    
    fileprivate func configureBackButton() {
        backButton.addTarget(self, action: #selector(backButtonDidTapped), for: .touchUpInside)
        
        productImageView.addSubview(backButton)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: productImageView.leadingAnchor),
            backButton.topAnchor.constraint(equalTo: productImageView.topAnchor),
            backButton.widthAnchor.constraint(equalToConstant: 50),
            backButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    @objc private func backButtonDidTapped() {
        productDetailVCDelegate?.productDetailViewControllerBackButtonDidTapped()
    }
    
}

extension ProductDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: view.frame.width, height: 300)
    }
}
