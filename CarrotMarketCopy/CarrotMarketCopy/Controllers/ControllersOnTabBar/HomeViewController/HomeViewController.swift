//
//  HomeViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/16.
//

import JJFloatingActionButton
import UIKit

class HomeViewController: UITableViewController {
    
    var buttonScrolledNewYPosition: CGFloat?
    
    let products:[ProductList] = [
        ProductList(productImage: "goldBar.png",
                    productTitle: "14K 골드바 지금 즉시 거래",
                    location: "도곡동",
                    time: "3분전",
                    price: "10,000원",
                    numberOfChatting: 10,
                    numberOfHeart: 5),
        
        ProductList(productImage: "goldBar.png",
                    productTitle: "24K 골드바 지금 즉시 거래",
                    location: "도곡동",
                    time: "3분전",
                    price: "10,000원",
                    numberOfChatting: 10,
                    numberOfHeart: 5),
        
        ProductList(productImage: "gun.jpg",
                    productTitle: "권총 팝니다",
                    location: "도곡동",
                    time: "5분전",
                    price: "50,000원",
                    numberOfChatting: nil,
                    numberOfHeart: 3),
        
        ProductList(productImage: "marijuana",
                    productTitle: "마리화나 팝니다",
                    location: "양재동",
                    time: "5분전",
                    price: "50,000원",
                    numberOfChatting: nil,
                    numberOfHeart: 3),
        
        ProductList(productImage: "GD.jpeg",
                    productTitle: "(새것) 연애인 잡지 직거래",
                    location: "도곡동",
                    time: "10분전",
                    price: "20,000원",
                    numberOfChatting: 2,
                    numberOfHeart: nil),
        
        ProductList(productImage: "GD.jpeg",
                    productTitle: "(새것) 연애인 잡지 직거래",
                    location: "도곡동",
                    time: "10분전",
                    price: "20,000원",
                    numberOfChatting: nil,
                    numberOfHeart: nil)
    ]
        
    var isAddNewPostButtonTapped = false

//    let addNewPostButton = UIButton()
//    let addNewProductButton = UIButton()
//    let addNewAdvertisemnetButton = UIButton()
    
    let floatingButton = JJFloatingActionButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        configureNavigationRightBarButtonItems()
        configureFloatingButton()
        
        tableView.register(ProductListCell.self, forCellReuseIdentifier: ProductListCell.identifier)
        
        
        
        //floatingButton.display(inViewController: self)
    }
    
    private func configureFloatingButton() {
        floatingButton.buttonColor = .systemOrange
        
        floatingButton.overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        floatingButton.addItem(title: "중고거래", image: UIImage(systemName: "pencil.circle")) { _ in
            let vc = PostProductToSellViewController()
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
            
        }
        
        floatingButton.addItem(title: "동네홍보",
                               image: UIImage(systemName: "newspaper.fill")) { _ in
            print("동네 홍보")
        }
        
        floatingButton.items.forEach { item in
            item.buttonColor = .systemOrange
            item.buttonImageColor = .white
        }
        
        tableView.addSubview(floatingButton)
        floatingButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            floatingButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            floatingButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        configureCustomTitle()
        
        configureNavigationBar()
        
        //setInitialValueForAddNewPostButton()
        
        //setInitialValueForAddNewProductButton()
        //setInitialValueForAddNewAdvertisemnetButton()
    }
    
    fileprivate func configureNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.tintColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        
        let separator: UIView = {
           let uiView = UIView()
            uiView.backgroundColor = .lightGray
            uiView.alpha = 0.3
            uiView.translatesAutoresizingMaskIntoConstraints = false
            return uiView
        }()
        
        navigationController?.navigationBar.addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.bottomAnchor.constraint(equalTo: (navigationController?.navigationBar.bottomAnchor)!),
            separator.leadingAnchor.constraint(equalTo: (navigationController?.navigationBar.leadingAnchor)!),
            separator.trailingAnchor.constraint(equalTo: (navigationController?.navigationBar.trailingAnchor)!),
            separator.heightAnchor.constraint(equalToConstant: 1),
        ])
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        products.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.set(product: products[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hideTabBarWithAnimation()
        let vc = ProductDetailViewController3()
        vc.delegate = self
        titleView.removeFromSuperview()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ProductListCell.prefferedHeight
    }
    
    let titleView: UIView = {
        let uiView = UIView()
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    fileprivate func configureCustomTitle() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(townRangeSettingButtonDidTapped))
        tapGesture.numberOfTapsRequired = 1
        
        titleView.frame = CGRect(x: 0, y: 0, width: view.frame.width / 2, height: navigationController?.navigationBar.frame.height ?? 100)
        titleView.addGestureRecognizer(tapGesture)
        titleView.isUserInteractionEnabled = true
        
        let titleLabel = CMBoldLabel(fontSize: 25)
        titleLabel.text = "양재동"
        titleLabel.addGestureRecognizer(tapGesture)
        titleLabel.isUserInteractionEnabled = true
        
        let townRangeSettingButton = UIButton()
        townRangeSettingButton.setImage(UIImage(systemName: "chevron.down"), for: .normal)
        townRangeSettingButton.addTarget(self, action: #selector(townRangeSettingButtonDidTapped), for: .touchUpInside)
        townRangeSettingButton.translatesAutoresizingMaskIntoConstraints = false
        
        titleView.addSubviews(titleLabel, townRangeSettingButton)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: titleView.leadingAnchor, constant: 10),
            titleLabel.topAnchor.constraint(equalTo: titleView.topAnchor, constant: 5),
            titleLabel.bottomAnchor.constraint(equalTo: titleView.bottomAnchor),
            
            townRangeSettingButton.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 5),
            townRangeSettingButton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
        ])
        
        navigationController?.navigationBar.addSubview(titleView)
    }
    
    @objc func townRangeSettingButtonDidTapped() {
        let vc = TownRangeViewController()
        vc.delegate = self
        
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .fullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    fileprivate func configureNavigationRightBarButtonItems() {
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(image: UIImage(systemName: "bell"), style: .plain, target: self, action: #selector(bellButtonDidTapped)),
            UIBarButtonItem(image: UIImage(systemName: "line.horizontal.3"), style: .plain, target: self, action: #selector(threeLinesButtonDidTapped)),
            UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .plain, target: self, action:  #selector(searchButtonDidTapped)),
        ]
    }
    
    @objc private func searchButtonDidTapped() {
        hideTabBarWithAnimation()
        let vc = SearchViewController()
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func threeLinesButtonDidTapped() {
        
    }
    
    @objc private func bellButtonDidTapped() {
        
    }
}

extension HomeViewController: SearchViewControllerDelegate {
    func searchViewControllerBackButtonDidTapped() {
        navigationController?.popViewController(animated: true)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension HomeViewController: ProductDetailViewControllerDelegate {
    func productDetailViewControllerBackButtonDidTapped() {
        showTabBarWithAnimation()
        navigationController?.popViewController(animated: true)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension HomeViewController: TownRangeViewControllerDelegate {
    func dismissTownRangeViewController() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension HomeViewController: ButtonActionViewControllerDelegate {
    func dismissButtonActionViewController() {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - AddNewPostButton,
//extension HomeViewController {
//    fileprivate func setInitialValueForAddNewPostButton() {
//
//        print(tabBarController!.tabBar.frame.height)
//
//        let buttonSideLength:CGFloat = 60
//        let padding:CGFloat = 20
//        let xPosition = view.bounds.width - buttonSideLength - padding
//        let yPosition = view.bounds.height - buttonSideLength - padding - 100 * 2
//
//        addNewPostButton.frame = CGRect(x: xPosition,
//                                        y: yPosition,
//                                        width: buttonSideLength,
//                                        height: buttonSideLength)
//        addNewPostButton.backgroundColor = .systemOrange
//        addNewPostButton.setImage(UIImage(systemName: "plus"), for: .normal)
//        addNewPostButton.imageView?.tintColor = .white
//        addNewPostButton.layer.cornerRadius = addNewPostButton.frame.width / 2
//        addNewPostButton.clipsToBounds = true
//        addNewPostButton.addTarget(self, action: #selector(ButtonClick(_:)), for: .touchUpInside)
//        view.addSubview(addNewPostButton)
//    }
//
//    @objc func ButtonClick(_ sender: UIButton) {
//
////
//        //isAddNewPostButtonTapped = !isAddNewPostButtonTapped
//
//        //if isAddNewPostButtonTapped {
//        // tableView.isScrollEnabled = false
//        let buttonSideLength:CGFloat = 60
//        let padding:CGFloat = 20
//
//            UIView.animate(withDuration: 0.5) {
////                self.addNewPostButton.layer.transform = CATransform3DMakeRotation(-CGFloat.pi/4, 0, 0, 1)
////                self.addNewPostButton.backgroundColor = .lightGray
////
////                self.addAddNewProductButton()
////                self.addNewProductButton.frame.origin.y = self.addNewPostButton.frame.origin.y - self.addNewProductButton.frame.height
////
////                self.addAddNewAdvertisementButton()
////                self.addNewAdvertisemnetButton.frame.origin.y = self.addNewPostButton.frame.origin.y - self.addNewAdvertisemnetButton.frame.height * 2 - 20
//            } completion: { _ in
//                let vc = ButtonActionViewController()
//                vc.modalTransitionStyle = .crossDissolve
//                vc.modalPresentationStyle = .overFullScreen
//                vc.delegate = self
//                vc.buttonPositionOrigin = self.addNewPostButton.frame.origin
//                vc.buttonScrolledNewYPosition = self.buttonScrolledNewYPosition ?? (self.view.frame.height - buttonSideLength - padding - 100 * 2)
//                self.present(vc, animated: true, completion: nil)
//            }
//
//       // } else {
////            tableView.isScrollEnabled = true
////            UIView.animate(withDuration: 0.5) {
////                self.addNewPostButton.layer.transform = CATransform3DMakeRotation(CGFloat.pi/2, 0, 0, 1)
////                self.addNewPostButton.backgroundColor = .systemOrange
////
////                self.addNewProductButton.frame.origin.y = self.addNewPostButton.frame.origin.y
////                self.addNewAdvertisemnetButton.frame.origin.y = self.addNewPostButton.frame.origin.y
////            } completion: { _ in
////                self.removeAddNewProductButtonFromSuperview()
////                self.removeAddNewAdvertisemnetButtonFromSuperview()
////
////            }
//       // }
//    }
//
//    fileprivate func addAddNewProductButton() {
//        addNewProductButton.backgroundColor = .lightGray
//        addNewProductButton.layer.cornerRadius = addNewProductButton.frame.width / 2
//        addNewProductButton.setImage(UIImage(systemName: "pencil.circle"), for: .normal)
//        view.addSubview(addNewProductButton)
//    }
//
//    fileprivate func addAddNewAdvertisementButton() {
//        addNewAdvertisemnetButton.backgroundColor = .lightGray
//        addNewAdvertisemnetButton.layer.cornerRadius = addNewProductButton.frame.width / 2
//        addNewAdvertisemnetButton.setImage(UIImage(systemName: "circle"), for: .normal)
//        view.addSubview(addNewAdvertisemnetButton)
//    }
//
//    fileprivate func setInitialValueForAddNewProductButton() {
//        let buttonSideLength:CGFloat = 40
//        let xPosition = addNewPostButton.frame.origin.x + buttonSideLength/4
//        let yPosition = addNewPostButton.frame.origin.y
//
//        addNewProductButton.frame = CGRect(x: xPosition, y: yPosition, width: buttonSideLength, height: buttonSideLength)
//    }
//
//    fileprivate func setInitialValueForAddNewAdvertisemnetButton() {
//        let buttonSideLength:CGFloat = 40
//        let xPosition = addNewPostButton.frame.origin.x + buttonSideLength/4
//        let yPosition = addNewPostButton.frame.origin.y
//
//        addNewAdvertisemnetButton.frame = CGRect(x: xPosition, y: yPosition, width: buttonSideLength, height: buttonSideLength)
//    }
//
//    fileprivate func removeAddNewProductButtonFromSuperview() {
//        addNewProductButton.removeFromSuperview()
//    }
//
//    fileprivate func removeAddNewAdvertisemnetButtonFromSuperview() {
//        addNewAdvertisemnetButton.removeFromSuperview()
//    }
//
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let buttonSideLength:CGFloat = 60
//        let padding:CGFloat = 20
//        let yPosition = view.frame.height - buttonSideLength - padding - tabBarController!.tabBar.frame.height
//
//        addNewPostButton.frame.origin.y = yPosition + scrollView.contentOffset.y
//        addNewProductButton.frame.origin.y = yPosition + scrollView.contentOffset.y
//        addNewAdvertisemnetButton.frame.origin.y = yPosition + scrollView.contentOffset.y
//
//        buttonScrolledNewYPosition = yPosition + scrollView.contentOffset.y
//    }
//}
