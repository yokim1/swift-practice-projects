//
//  ButtonActionViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/19.
//

import UIKit

protocol ButtonActionViewControllerDelegate {
    func dismissButtonActionViewController()
}

class ButtonActionViewController: UIViewController {

    
    
    var isAddNewPostButtonTapped = false

    let addNewPostButton = UIButton()
    let addNewProductButton = UIButton()
    let addNewAdvertisemnetButton = UIButton()
    
    var buttonScrolledNewYPosition: CGFloat = 0
    
    var delegate: ButtonActionViewControllerDelegate?
    var buttonPositionOrigin: CGPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(isViewTapped)))
        
        configureAddNewPostButton()
        configureAddNewProductButton()
        configureAddNewAdvertisemnetButton()
        ButtonClick()
    }
    
    @objc func isViewTapped() {
        delegate?.dismissButtonActionViewController()
    }
}

extension ButtonActionViewController {
    fileprivate func configureAddNewPostButton() {
        let buttonSideLength:CGFloat = 60
        let padding: CGFloat = 20
        //let xPosition = view.frame.width - buttonSideLength - padding
        //let yPosition = view.frame.height - buttonSideLength - padding - 83.0 * 2
        print(buttonPositionOrigin)
        
        addNewPostButton.frame = CGRect(x: (buttonPositionOrigin?.x ?? 0),
                                        y: 1300 - (buttonPositionOrigin?.y ?? 0),
                                        width: buttonSideLength,
                                        height: buttonSideLength)
        addNewPostButton.frame.origin.y = buttonScrolledNewYPosition
        addNewPostButton.backgroundColor = .systemOrange
        addNewPostButton.setImage(UIImage(systemName: "plus"), for: .normal)
        addNewPostButton.imageView?.tintColor = .systemOrange
        addNewPostButton.layer.cornerRadius = addNewPostButton.frame.width / 2
        addNewPostButton.clipsToBounds = true
        addNewPostButton.addTarget(self, action: #selector(ButtonClick), for: .touchUpInside)
        addNewPostButton.layer.shadowColor = UIColor.red.cgColor
        addNewPostButton.layer.shadowOpacity = 1.0
        addNewPostButton.layer.shadowOffset = CGSize.zero
        addNewPostButton.layer.shadowRadius = 6
        view.addSubview(addNewPostButton)
    }
    
    @objc func ButtonClick() {
        isAddNewPostButtonTapped = !isAddNewPostButtonTapped
        
        if isAddNewPostButtonTapped {
            UIView.animate(withDuration: 0.5) {
//                self.addNewPostButton.layer.transform = CATransform3DMakeRotation(-CGFloat.pi/4, 0, 0, 1)
//                self.addNewPostButton.backgroundColor = .lightGray
//
//                self.addAddNewProductButton()
//                self.addNewProductButton.frame.origin.y = self.addNewPostButton.frame.origin.y - self.addNewProductButton.frame.height
//
//                self.addAddNewAdvertisementButton()
//                self.addNewAdvertisemnetButton.frame.origin.y = self.addNewPostButton.frame.origin.y - self.addNewAdvertisemnetButton.frame.height * 2 - 20
            } completion: { _ in
                
                UIView.animate(withDuration: 0.5) {
                    self.addNewPostButton.layer.transform = CATransform3DMakeRotation(-CGFloat.pi/4, 0, 0, 1)
                    self.addNewPostButton.backgroundColor = .white
                    
                    self.addAddNewProductButton()
                    self.addNewProductButton.frame.origin.y = self.addNewPostButton.frame.origin.y - self.addNewProductButton.frame.height
                    
                    self.addAddNewAdvertisementButton()
                    self.addNewAdvertisemnetButton.frame.origin.y = self.addNewPostButton.frame.origin.y - self.addNewAdvertisemnetButton.frame.height * 2 - 20
                }
            }
            
        } else {
            UIView.animate(withDuration: 0.5) {
                self.addNewPostButton.layer.transform = CATransform3DMakeRotation(CGFloat.pi/2, 0, 0, 1)
                self.addNewPostButton.backgroundColor = .systemOrange
                
                self.addNewProductButton.frame.origin.y = self.addNewPostButton.frame.origin.y
                self.addNewAdvertisemnetButton.frame.origin.y = self.addNewPostButton.frame.origin.y
            } completion: { _ in
                self.removeAddNewProductButtonFromSuperview()
                self.removeAddNewAdvertisemnetButtonFromSuperview()
                self.delegate?.dismissButtonActionViewController()
            }
        }
    }
    
    fileprivate func addAddNewProductButton() {
        addNewProductButton.backgroundColor = .systemOrange
        addNewProductButton.layer.cornerRadius = addNewProductButton.frame.width / 2
        addNewProductButton.setImage(UIImage(systemName: "pencil.circle"), for: .normal)
        addNewProductButton.imageView?.tintColor = .white
        view.addSubview(addNewProductButton)
    }
    
    fileprivate func addAddNewAdvertisementButton() {
        addNewAdvertisemnetButton.backgroundColor = .systemOrange
        addNewAdvertisemnetButton.layer.cornerRadius = addNewProductButton.frame.width / 2
        addNewAdvertisemnetButton.setImage(UIImage(systemName: "circle"), for: .normal)
        addNewAdvertisemnetButton.imageView?.tintColor = .white
        view.addSubview(addNewAdvertisemnetButton)
    }
    
    fileprivate func configureAddNewProductButton() {
        let buttonSideLength: CGFloat = 40
        let xPosition = addNewPostButton.frame.origin.x + buttonSideLength/4
        let yPosition = addNewPostButton.frame.origin.y
        
        addNewProductButton.frame = CGRect(x: xPosition, y: yPosition, width: buttonSideLength, height: buttonSideLength)
    }
    
    fileprivate func configureAddNewAdvertisemnetButton() {
        let buttonSideLength: CGFloat = 40
        let xPosition = addNewPostButton.frame.origin.x + buttonSideLength/4
        let yPosition = addNewPostButton.frame.origin.y
        
        addNewAdvertisemnetButton.frame = CGRect(x: xPosition, y: yPosition, width: buttonSideLength, height: buttonSideLength)
    }
    
    fileprivate func removeAddNewProductButtonFromSuperview() {
        addNewProductButton.removeFromSuperview()
    }
    
    fileprivate func removeAddNewAdvertisemnetButtonFromSuperview() {
        addNewAdvertisemnetButton.removeFromSuperview()
    }
}
