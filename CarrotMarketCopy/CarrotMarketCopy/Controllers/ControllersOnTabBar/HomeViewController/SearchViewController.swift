//
//  SearchViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

protocol SearchViewControllerDelegate: AnyObject {
    func searchViewControllerBackButtonDidTapped()
}

class SearchViewController: UIViewController, UISearchBarDelegate {
    
    var delegate: SearchViewControllerDelegate?
    
    let customTabBarView = UIView()
    
    let searchBar = UISearchBar()
    
    let newBackButton = UIButton()
    
    var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        configureCustomTabBar()
        configureBackButton()
        configureSearchBar()
        configureCollectionView()
    }
    
    fileprivate func configureCustomTabBar() {
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        view.addSubview(customTabBarView)
        customTabBarView.backgroundColor = .systemBackground
        customTabBarView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            customTabBarView.topAnchor.constraint(equalTo: view.topAnchor),
            customTabBarView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            customTabBarView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            customTabBarView.heightAnchor.constraint(equalToConstant: 100),
        ])
    }
    
    fileprivate func configureBackButton() {
        
        newBackButton.setImage(UIImage(systemName: "arrow.backward"), for: .normal)
        newBackButton.imageView?.contentMode = .scaleToFill
        newBackButton.addTarget(self, action: #selector(backButtonDidTapped), for: .touchUpInside)
        
        customTabBarView.addSubview(newBackButton)
        newBackButton.backgroundColor = .systemBackground
        newBackButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            newBackButton.bottomAnchor.constraint(equalTo: customTabBarView.bottomAnchor),
            newBackButton.leadingAnchor.constraint(equalTo: customTabBarView.leadingAnchor),
            newBackButton.heightAnchor.constraint(equalToConstant: 50),
            newBackButton.widthAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    @objc fileprivate func backButtonDidTapped() {
        showTabBarWithAnimation()
        delegate?.searchViewControllerBackButtonDidTapped()
    }
    
    fileprivate func configureSearchBar() {
        
        searchBar.delegate = self
        customTabBarView.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            searchBar.leadingAnchor.constraint(equalTo: newBackButton.trailingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: customTabBarView.trailingAnchor),
            searchBar.centerYAnchor.constraint(equalTo: newBackButton.centerYAnchor),
        ])
    }
    
    fileprivate func configureCollectionView() {
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.backgroundColor = .systemBackground
        
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: customTabBarView.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension SearchViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        cell.backgroundColor = .red
        return cell
    }
}

extension SearchViewController: UICollectionViewDelegate {
    
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: view.frame.width, height: 300)
    }
}
