//
//  TownRangeViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

protocol TownRangeViewControllerDelegate: AnyObject {
    func dismissTownRangeViewController()
}

class TownRangeViewController: UITableViewController {
    
    var delegate: TownRangeViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        configureViewController()
        configureNagationTabBarLeftButton()
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TownRangeSelectCell.identifier, for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TownRangeScrollBarCell.identifier, for: indexPath)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            
            return 200
            
        } else {
            
            return 500
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension TownRangeViewController {
    fileprivate func configureTableView() {
        tableView.register(TownRangeSelectCell.self, forCellReuseIdentifier: TownRangeSelectCell.identifier)
        tableView.register(TownRangeScrollBarCell.self, forCellReuseIdentifier: TownRangeScrollBarCell.identifier)
    }
    
    fileprivate func configureViewController() {
        view.backgroundColor = .systemBackground
        title = "내 동네 설정하기"
    }
    
    fileprivate func configureNagationTabBarLeftButton() {
        navigationItem.leftBarButtonItems = [
            UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(dismissViewControllerButtonDidTapped))
        ]
    }
    
    @objc private func dismissViewControllerButtonDidTapped() {
        delegate?.dismissTownRangeViewController()
    }
}
