//
//  ProductDetailViewControllerViewController2.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

class ProductDetailViewController3: UIViewController {
    
    var collectionView: UICollectionView!
    
    var delegate: ProductDetailViewControllerDelegate?
    
    var productDetail: ProductDetail = ProductDetail(
        productImages: [
            ProductImage(productImageUrl: "ProductImage")
        ],
        
        sellerProfilePhoto: "",
        sellerName: "골드바매니아",
        sellerLocation: "도곡동",
        sellerTemparture: "40.5",
        
        productTitle: "골드바 지금 즉시 거래",
        productCategory: "돈/골드",
        time: "3분전",
        productDescription: "골드바 지금 즉시 도곡역에서 직거래 가능합니다. 네고 안됩니다.",
        
        productPrice: "10,000",
        priceSuggestion: "가격제안 불가",
        isLiked: false,
        
        sellerOtherProduct: [
            ProductPreview(productImage: "GD.jpeg", productTitle: "GD 팝니다", productPrice: "2,000"),
            ProductPreview(productImage: "gun.jpg", productTitle: "(미개봉)권총 팝니다", productPrice: "15,000"),
            ProductPreview(productImage: "gun.jpg", productTitle: "탄창 팝니다.", productPrice: "8,000"),
        ],
        
        otherRecommendedProduct: [
            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
            //            ProductPreview(productImage: <#T##String#>, productTitle: <#T##String#>, productPrice: <#T##String#>),
        ])
    
    let buyTabBar = BuyTabBarView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController?.navigationBar.isTranslucent = true
        
        configureNavigationBar()
        configureLeftButton()
        configureRightButton()
        
        configureCollectionView()
        configureBuyTapBar()
    }
    
    fileprivate func configureNavigationBar() {
//        let separator: UIView = {
//           let uiView = UIView()
//            uiView.backgroundColor = .lightGray
//            uiView.alpha = 0.3
//            uiView.translatesAutoresizingMaskIntoConstraints = false
//            return uiView
//        }()
//
//        navigationController?.navigationBar.addSubview(separator)
//
//        NSLayoutConstraint.activate([
//            separator.bottomAnchor.constraint(equalTo: (navigationController?.navigationBar.bottomAnchor)!),
//            separator.leadingAnchor.constraint(equalTo: (navigationController?.navigationBar.leadingAnchor)!),
//            separator.trailingAnchor.constraint(equalTo: (navigationController?.navigationBar.trailingAnchor)!),
//            separator.heightAnchor.constraint(equalToConstant: 1),
//        ])
    }
    
    fileprivate func configureLeftButton() {
        navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(back(sender:)))
        newBackButton.tintColor = .white
        navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        delegate?.productDetailViewControllerBackButtonDidTapped()
    }
    
    fileprivate func configureRightButton() {
        let shareButton = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up"), style: .done, target: self, action: #selector(shareButtonDidTapped))
        shareButton.tintColor = .white
        let reportThisButton = UIBarButtonItem(image: UIImage(systemName: "ellipsis.circle"), style: .plain, target: self, action: #selector(reportThisButtonDidButton))
        reportThisButton.tintColor = .white
        
        navigationItem.rightBarButtonItems = [reportThisButton, shareButton]
    }
    
    @objc fileprivate func shareButtonDidTapped() {
        
    }
    
    @objc fileprivate func reportThisButtonDidButton() {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > (view.frame.height/2 - (navigationController?.navigationBar.frame.height ?? 100)) {
            UIView.animate(withDuration: 0.3) {
                self.navigationController?.navigationBar.isTranslucent = false
                self.navigationItem.leftBarButtonItem?.tintColor = .black
                self.navigationItem.rightBarButtonItems?.forEach({ item in
                    item.tintColor = .black
                })
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationItem.leftBarButtonItem?.tintColor = .white
                self.navigationItem.rightBarButtonItems?.forEach({ item in
                    item.tintColor = .white
                })
            }
        }
    }
    
    fileprivate func configureCollectionView() {
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - BuyTabBarView.preferredHeight), collectionViewLayout: UICollectionViewFlowLayout())
        
        collectionView.register(ProductDetailPhotoPagingCell.self, forCellWithReuseIdentifier: ProductDetailPhotoPagingCell.identifier)
        
        collectionView.register(ProductDetailSellerProfilePreviewCell.self, forCellWithReuseIdentifier: ProductDetailSellerProfilePreviewCell.identifier)
        
        collectionView.register(ProductDetailProductDescriptionCell.self, forCellWithReuseIdentifier: ProductDetailProductDescriptionCell.identifier)
        
        collectionView.register(ProductDetailReportThisProductCell.self, forCellWithReuseIdentifier: ProductDetailReportThisProductCell.identifier)
        
        collectionView.register(ProductDetailSellerOtherProductsCell.self, forCellWithReuseIdentifier: ProductDetailSellerOtherProductsCell.identifier)
        
        collectionView.register(ProductDetailOtherProductRecommendCell.self, forCellWithReuseIdentifier: ProductDetailOtherProductRecommendCell.identifier)
        
        view.addSubview(collectionView)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.contentInset = UIEdgeInsets(top: -100, left: 0, bottom: 0, right: 0)
        collectionView.backgroundColor = .systemBackground
    }
    
    fileprivate func configureBuyTapBar() {
        buyTabBar.priceLabel.text = productDetail.productPrice
        buyTabBar.sellerOptionLabel.text = productDetail.priceSuggestion
        
        view.addSubview(buyTabBar)
        
        NSLayoutConstraint.activate([
            buyTabBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            buyTabBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            buyTabBar.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            buyTabBar.heightAnchor.constraint(equalToConstant: BuyTabBarView.preferredHeight),
        ])
    }
}

extension ProductDetailViewController3: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.item {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailPhotoPagingCell.identifier, for: indexPath) as? ProductDetailPhotoPagingCell  else {return UICollectionViewCell()}
            cell.set(productDetail: productDetail)
            return cell
            
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailSellerProfilePreviewCell.identifier, for: indexPath) as? ProductDetailSellerProfilePreviewCell  else {return UICollectionViewCell()}
            cell.set(productDetail: productDetail)
            return cell
            
        case 2:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailProductDescriptionCell.identifier, for: indexPath) as? ProductDetailProductDescriptionCell  else {return UICollectionViewCell()}
            cell.set(productDetail: productDetail)
            return cell
            
        case 3:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailReportThisProductCell.identifier, for: indexPath) as? ProductDetailReportThisProductCell  else {return UICollectionViewCell()}
            cell.set(productDetail: productDetail)
            return cell
            
        case 4:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailSellerOtherProductsCell.identifier, for: indexPath) as? ProductDetailSellerOtherProductsCell  else {return UICollectionViewCell()}
            cell.set(productDetail: productDetail)
            cell.backgroundColor = .blue
            return cell
            
        case 5:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailOtherProductRecommendCell.identifier, for: indexPath) as? ProductDetailOtherProductRecommendCell  else {return UICollectionViewCell()}
            cell.set(productDetail: productDetail)
            cell.backgroundColor = .red
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
}

extension ProductDetailViewController3: UICollectionViewDelegate {
    
}

extension ProductDetailViewController3: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 0
        
        switch indexPath.item {
        case 0:
            //height = ProductDetailPhotoPagingCell.prefferedHeight
            height = view.frame.height / 2
            break
            
        case 1:
            height = ProductDetailPhotoPagingCell.prefferedHeight
            break
            
        case 2:
            height = ProductDetailProductDescriptionCell.prefferedHeight
            break
            
        case 3:
            height = ProductDetailReportThisProductCell.prefferedHeight
            break
            
        case 4:
            height = ProductDetailSellerOtherProductsCell.prefferedHeight
            break
            
        case 5:
            height = ProductDetailOtherProductRecommendCell.prefferedHeight
            break
            
        default:
            break
        }
        
        return .init(width: view.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
}
