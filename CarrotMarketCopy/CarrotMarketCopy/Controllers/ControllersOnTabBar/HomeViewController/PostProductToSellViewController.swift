//
//  PostProductToSellViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/26.
//

import UIKit

class ProductPostingCell: UICollectionViewCell {
    
    static let identifier = "ProductPostingCell"
    
    static let photoPickerSectionHeight:CGFloat = 100
    
    static let titleSectionHeight:CGFloat = 80
    
    static let categorySectionHeight:CGFloat = 80
    
    static let priceSectionHeight:CGFloat = 80
    
    static let descriptionSectionHeight:CGFloat = 400
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        backgroundColor = .systemBackground
        
        setCellSeparator()
    }
    
    func setPhotoPicker(){
        backgroundColor = .red
    }
    
    let titleTextFied: UITextField = {
       let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    func setTitle(){
       addSubview(titleTextFied)
        titleTextFied.placeholder = "글 제목"
        
        NSLayoutConstraint.activate([
            titleTextFied.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.separatorPadding),
            titleTextFied.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleTextFied.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
        
    }
    
    let categorySelectionLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setCategory() {
        addSubview(categorySelectionLabel)
        categorySelectionLabel.text = "카테고리 선택"
        
        NSLayoutConstraint.activate([
            categorySelectionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.separatorPadding),
            categorySelectionLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
    let priceTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    func setPrice() {
        addSubview(priceTextField)
        priceTextField.placeholder = "가격 (선택사항)"
        
        NSLayoutConstraint.activate([
            priceTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.separatorPadding),
            priceTextField.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
    let descriptionTextField = UITextView()
//        = {
//        let textView = UITextView()
//        textView.translatesAutoresizingMaskIntoConstraints = false
//        return textView
//    }
    
    func setDescription() {
        addSubview(descriptionTextField)
        descriptionTextField.translatesAutoresizingMaskIntoConstraints = false
        descriptionTextField.backgroundColor = .blue
        
        NSLayoutConstraint.activate([
            descriptionTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.separatorPadding),
            descriptionTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.separatorPadding),
            
            descriptionTextField.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            descriptionTextField.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PostProductToSellViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "중고거래 글쓰기"
        
        collectionView.backgroundColor = .systemBackground
        collectionView.register(ProductPostingCell.self, forCellWithReuseIdentifier: ProductPostingCell.identifier)
        
        configureNavigationBar()
    }
    
    private func configureNavigationBar(){
        navigationController?.navigationBar.backgroundColor = .white
        
        navigationController?.navigationBar.isTranslucent = false
        
        navigationItem.leftBarButtonItems = [
            UIBarButtonItem(title: "닫기", style: .done, target: self, action: #selector(closeButtonDidTapped))
        ]
        
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(title: "완료", style: .plain, target: self, action: #selector(completeButtonDidTapped))
        ]
    }
    
    @objc private func closeButtonDidTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func completeButtonDidTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        5
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductPostingCell.identifier, for: indexPath) as! ProductPostingCell
        
        switch indexPath.row {
        case 0:
            cell.setPhotoPicker()
            break
            
        case 1:
            cell.setTitle()
            break
            
        case 2:
            cell.setCategory()
            break
            
        case 3:
            cell.setPrice()
            break
            
        case 4:
            cell.setDescription()
            break
            
        default:
            return UICollectionViewCell()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.row {
        case 0:
            return CGSize(width: view.frame.width, height: ProductPostingCell.photoPickerSectionHeight)
        case 1:
            return CGSize(width: view.frame.width, height: ProductPostingCell.titleSectionHeight)
        case 2:
            return CGSize(width: view.frame.width, height: ProductPostingCell.categorySectionHeight)
        case 3:
            return CGSize(width: view.frame.width, height: ProductPostingCell.priceSectionHeight)
        case 4:
            return CGSize(width: view.frame.width, height: ProductPostingCell.descriptionSectionHeight)
        default:
            return CGSize(width: view.frame.width, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
  
    init(){
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
