//
//  ProductTwoRowCollectionViewController.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

private let reuseIdentifier = "Cell"

class ProductTwoRowCollectionViewController: UICollectionViewController {

    let products: [ProductPreview] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        collectionView.collectionViewLayout = createThreeColumnFlowLayout()
    }
    
    func createThreeColumnFlowLayout() -> UICollectionViewFlowLayout {
        let width                       = view.bounds.width
        let padding: CGFloat            = Constants.separatorPadding
        let minimumItemSpacing: CGFloat = 10
        let availableWidth              = (width - (padding * 2) - (minimumItemSpacing * 2)) / 2
        
        let flowLayout                  = UICollectionViewFlowLayout()
        flowLayout.sectionInset         = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        flowLayout.itemSize             = CGSize(width: availableWidth, height: availableWidth + 40)
        
        return flowLayout
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
    
        return cell
    }

}
