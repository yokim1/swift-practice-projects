//
//  ProductDetailSellerPreviewProfile.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

class ProductDetailSellerPreviewProfile: UIView {
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let sellerUserName = CMBoldLabel(fontSize: 20)
    let sellerLocation = CMSecondaryLabel(fontSize: 10)
    
    let mannerTemperatureNumber = CMSecondaryLabel(fontSize: 25)
    let mannerTemperature = CMSecondaryLabel(fontSize: 15)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        backgroundColor = .gray
        translatesAutoresizingMaskIntoConstraints = false
        profileImageView.backgroundColor = .brown
        sellerUserName.text = "hahahaiOS"
        sellerLocation.text = "도곡동"
        
        
        let labelStackView = UIStackView(arrangedSubviews: [sellerUserName, sellerLocation])
        labelStackView.axis = .vertical
        
        let userInfoWithProfileStackView = UIStackView(arrangedSubviews: [profileImageView, labelStackView])
        userInfoWithProfileStackView.axis = .horizontal
        
        let sellerMannerTemperatureStackView = UIStackView(arrangedSubviews: [mannerTemperatureNumber, mannerTemperature])
        sellerMannerTemperatureStackView.axis = .vertical
        
        let totalStackView = UIStackView(arrangedSubviews: [userInfoWithProfileStackView, sellerMannerTemperatureStackView])
        totalStackView.axis = .horizontal
        totalStackView.spacing = 20
        
        addSubview(totalStackView)
        NSLayoutConstraint.activate([
            totalStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            totalStackView.topAnchor.constraint(equalTo: topAnchor),
            totalStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
}
