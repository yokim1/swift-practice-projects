//
//  CMView.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

class CMLabel: UIView {
    
    let textLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(title: String, textAlignment: NSTextAlignment, textColor: UIColor, backgroundColor: UIColor) {
        self.init(frame: .zero)
        
        textLabel.text = title
        textLabel.textAlignment = textAlignment
        textLabel.textColor = textColor
        textLabel.backgroundColor = backgroundColor
        self.backgroundColor = backgroundColor
    }
    
    private func configure() {
        layer.cornerRadius = 6
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(textLabel)
        
        NSLayoutConstraint.activate([
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            textLabel.topAnchor.constraint(equalTo: topAnchor),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            
        ])
    }
}
