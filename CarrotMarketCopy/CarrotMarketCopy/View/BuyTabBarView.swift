//
//  BuyTabBarView.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

class BuyTabBarView: UIView {
    static let preferredHeight:CGFloat = 100
    
    let heartButton = UIButton()
    
    let verticalSeparator = UIView()
    
    let priceLabel = CMBoldLabel(fontSize: 17)
    
    let sellerOptionLabel = CMSecondaryLabel(fontSize: 12)
    
    let connectToChattingButton = CMButton(title: "거래하기")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureConnectToChattingButton()
        configureBuyTabBar()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureBuyTabBar() {
        backgroundColor = .systemBackground
        
        heartButton.setImage(UIImage(systemName: "heart"), for: .normal)
        
        verticalSeparator.backgroundColor = .lightGray
        
        let priceLabelStackView = UIStackView(arrangedSubviews: [priceLabel, sellerOptionLabel])
        priceLabelStackView.axis = .vertical
        priceLabelStackView.distribution = .fillEqually
        
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(heartButton)
        addSubview(verticalSeparator)
        addSubview(priceLabelStackView)
        
        heartButton.translatesAutoresizingMaskIntoConstraints = false
        verticalSeparator.translatesAutoresizingMaskIntoConstraints = false
        priceLabelStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            heartButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            heartButton.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            
            verticalSeparator.leadingAnchor.constraint(equalTo: heartButton.trailingAnchor, constant: 15),
            verticalSeparator.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            verticalSeparator.bottomAnchor.constraint(equalTo: priceLabelStackView.bottomAnchor),
            verticalSeparator.widthAnchor.constraint(equalToConstant: 1),
            
            priceLabelStackView.centerYAnchor.constraint(equalTo: connectToChattingButton.centerYAnchor),
            priceLabelStackView.leadingAnchor.constraint(equalTo: heartButton.trailingAnchor, constant: 40),
        ])
        
        setViewSeparator()
    }
    
    fileprivate func configureConnectToChattingButton() {
        connectToChattingButton.addTarget(self, action: #selector(connectToChattingButtonDidTapped), for: .touchUpInside)
        
        addSubview(connectToChattingButton)
        
        NSLayoutConstraint.activate([
            connectToChattingButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            connectToChattingButton.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            connectToChattingButton.widthAnchor.constraint(equalToConstant: 150),
        ])
    }
    
    @objc func connectToChattingButtonDidTapped(){
        print("tapped")
    }
    
    fileprivate func setViewSeparator() {
        let separator: UIView = {
           let uiView = UIView()
            uiView.backgroundColor = .lightGray
            uiView.alpha = 0.3
            uiView.translatesAutoresizingMaskIntoConstraints = false
            return uiView
        }()
        
        addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.topAnchor.constraint(equalTo: topAnchor),
            separator.leadingAnchor.constraint(equalTo: leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor),
            separator.heightAnchor.constraint(equalToConstant: 1),
        ])
    }
}
