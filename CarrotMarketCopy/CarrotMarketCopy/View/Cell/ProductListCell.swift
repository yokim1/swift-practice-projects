//
//  ProductListCell.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

class ProductListCell: UITableViewCell {

    static let identifier = "ProductListCell"
    static let prefferedHeight: CGFloat = 140
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureProductInfo()
        configureChattingInfo()
        configureHeartInfo()
    }
    
    let productImagView:UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .lightGray
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    let productTitleLabel = CMTitleLabel(textAlignment: .left, fontSize: 20)
    
    let locationLabel = CMSecondaryLabel(fontSize: 15)
    
    let timeLabel = CMSecondaryLabel(fontSize: 15)
    
    let priceLabel = CMBoldLabel(fontSize: 20)
    
    let chattingImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(systemName: "bubble.left.and.bubble.right")
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    let numberOfChattingLabel = CMSecondaryLabel(fontSize: 12)
    
    let heartImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(systemName: "heart")
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    let numberOfHeartLabel = CMSecondaryLabel(fontSize: 12)
    
    private func configureProductInfo() {
        [productImagView, productTitleLabel, locationLabel, timeLabel, priceLabel, chattingImageView, heartImageView, numberOfHeartLabel, numberOfChattingLabel ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        
        let imagePadding: CGFloat = 10
        let paddingBetweenText:CGFloat = 5
        
        NSLayoutConstraint.activate([
            productImagView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: imagePadding),
            productImagView.centerYAnchor.constraint(equalTo: centerYAnchor),
            productImagView.topAnchor.constraint(equalTo: topAnchor, constant: imagePadding),
            
            productImagView.widthAnchor.constraint(equalToConstant: ProductListCell.prefferedHeight - imagePadding),
            productImagView.heightAnchor.constraint(equalToConstant: ProductListCell.prefferedHeight),
            
            productTitleLabel.leadingAnchor.constraint(equalTo: productImagView.trailingAnchor, constant: imagePadding),
            productTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            productTitleLabel.topAnchor.constraint(equalTo: productImagView.topAnchor, constant: paddingBetweenText),
            
            locationLabel.leadingAnchor.constraint(equalTo: productTitleLabel.leadingAnchor),
            locationLabel.topAnchor.constraint(equalTo: productTitleLabel.bottomAnchor, constant: paddingBetweenText),
            
            timeLabel.leadingAnchor.constraint(equalTo: locationLabel.trailingAnchor, constant: paddingBetweenText),
            timeLabel.topAnchor.constraint(equalTo: locationLabel.topAnchor),
            
            priceLabel.leadingAnchor.constraint(equalTo: productTitleLabel.leadingAnchor),
            priceLabel.topAnchor.constraint(equalTo: locationLabel.bottomAnchor, constant: paddingBetweenText),
        ])
    }
    
    private func configureChattingInfo() {
        let imagePadding: CGFloat = 10
        let paddingBetweenText:CGFloat = 5
        
        if numberOfHeartLabel.isHidden == true {
            NSLayoutConstraint.activate([
                numberOfChattingLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -imagePadding),
                numberOfChattingLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -imagePadding),
                
                numberOfChattingLabel.trailingAnchor.constraint(equalTo: numberOfHeartLabel.leadingAnchor, constant: -paddingBetweenText),
                numberOfChattingLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -imagePadding),
            ])
        } else {
            NSLayoutConstraint.activate([
                numberOfChattingLabel.trailingAnchor.constraint(equalTo: heartImageView.leadingAnchor, constant: -imagePadding),
                numberOfChattingLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -imagePadding),
                
                chattingImageView.trailingAnchor.constraint(equalTo: numberOfChattingLabel.leadingAnchor, constant: -paddingBetweenText),
                chattingImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -imagePadding),
            ])
        }
    }
    
    private func configureHeartInfo() {
        let imagePadding: CGFloat = 10
        let paddingBetweenText:CGFloat = 5
        
        if numberOfHeartLabel.isHidden == true{
            
        } else {
            NSLayoutConstraint.activate([
                numberOfHeartLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -imagePadding),
                numberOfHeartLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -imagePadding),
                
                heartImageView.trailingAnchor.constraint(equalTo: numberOfHeartLabel.leadingAnchor, constant: -paddingBetweenText),
                heartImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -imagePadding),
            ])
        }
       
    }
    
    func set(product: ProductList) {
        productImagView.image = UIImage(named: product.productImage ?? "")
        productTitleLabel.text = product.productTitle
        
        locationLabel.text = product.location
        timeLabel.text = product.time
        priceLabel.text = product.price
        
        if product.numberOfHeart == nil {
            heartImageView.isHidden = true
            numberOfHeartLabel.isHidden = true
        } else {
            numberOfHeartLabel.text = "\(product.numberOfHeart ?? 0) "
        }
        
        if product.numberOfChatting == nil {
            chattingImageView.isHidden = true
            numberOfChattingLabel.isHidden = true
        } else {
            numberOfChattingLabel.text = "\(product.numberOfChatting ?? 0)"
        }
        
        configureChattingInfo()
        //configureHeartInfo()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
