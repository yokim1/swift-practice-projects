//
//  TownRangeSelectCell.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

class TownRangeSelectCell: UITableViewCell {
    static let identifier = "TownRangeSelectCell"
    
    let townSelectTitleLabel = CMTitleLabel(textAlignment: .center, fontSize: 25)
    let townselectInstructionLabel = CMSecondaryLabel(fontSize: 15)
    
    let townSelectionOne = CMLabel(title: "양재동", textAlignment: .left, textColor: .white, backgroundColor: .systemOrange)
    let townSelectionTwo = CMLabel(title: "+", textAlignment: .center, textColor: .secondaryLabel, backgroundColor: .lightGray)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        townSelectTitleLabel.text = "동네 선택"
        townselectInstructionLabel.text = "지역은 최소 1개 이상 최대 2개까지 설정 가능해요."
        
        let paddingView = UIView()
        
        addSubview(townSelectTitleLabel)
        addSubview(townselectInstructionLabel)
        
        addSubview(townSelectionOne)
        addSubview(paddingView)
        addSubview(townSelectionTwo)
        
        paddingView.translatesAutoresizingMaskIntoConstraints = false
        townSelectionTwo.translatesAutoresizingMaskIntoConstraints = false
        
        //townSelectionOne.backgroundColor = .systemOrange
        paddingView.backgroundColor = .systemBackground
        //townSelectionTwo.backgroundColor = .orange
        
        townSelectionOne.layer.cornerRadius = 6
        townSelectionTwo.layer.cornerRadius = 6
        
        NSLayoutConstraint.activate([
            townSelectTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            townSelectTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            
            townselectInstructionLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            townselectInstructionLabel.topAnchor.constraint(equalTo: townSelectTitleLabel.bottomAnchor, constant: 10),
            
            paddingView.topAnchor.constraint(equalTo: townselectInstructionLabel.bottomAnchor, constant: 20),
            paddingView.centerXAnchor.constraint(equalTo: centerXAnchor),
            paddingView.heightAnchor.constraint(equalToConstant: 60),
            paddingView.widthAnchor.constraint(equalToConstant: 20),
            
            townSelectionOne.topAnchor.constraint(equalTo: paddingView.topAnchor),
            townSelectionOne.leadingAnchor.constraint(equalTo: townselectInstructionLabel.leadingAnchor),
            townSelectionOne.trailingAnchor.constraint(equalTo: paddingView.leadingAnchor),
            townSelectionOne.bottomAnchor.constraint(equalTo: paddingView.bottomAnchor),
            
            townSelectionTwo.topAnchor.constraint(equalTo: paddingView.topAnchor),
            townSelectionTwo.leadingAnchor.constraint(equalTo: paddingView.trailingAnchor),
            townSelectionTwo.trailingAnchor.constraint(equalTo: townselectInstructionLabel.trailingAnchor),
            townSelectionTwo.bottomAnchor.constraint(equalTo: paddingView.bottomAnchor),
        ])
    }
}
