//
//  File.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

class ProductDetailProductDescriptionCell: UICollectionViewCell{
    static let identifier = "ProductDetailProductDescription"
    static let prefferedHeight: CGFloat = 300
    
    let productTitleLabel = CMBoldLabel(fontSize: 25)
    let productCategoryLabel = CMSecondaryLabel(fontSize: 17)
    let timeLabel = CMSecondaryLabel(fontSize: 17)
    
    let productDescriptionLabel = CMBodyLabel(textAlignment: .left)
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        configure()
//    }
//
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        productDescriptionLabel.numberOfLines = 5
        
        addSubviews(productTitleLabel, productCategoryLabel, timeLabel, productDescriptionLabel)
        
        let paddingsAroundTexts: CGFloat = 20
        let paddingBetweenLabels: CGFloat = 15
        
        NSLayoutConstraint.activate([
            productTitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: paddingsAroundTexts),
            productTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: paddingsAroundTexts),
            
            productCategoryLabel.leadingAnchor.constraint(equalTo: productTitleLabel.leadingAnchor),
            productCategoryLabel.topAnchor.constraint(equalTo: productTitleLabel.bottomAnchor, constant: paddingBetweenLabels),
            
            timeLabel.leadingAnchor.constraint(equalTo: productCategoryLabel.trailingAnchor, constant: 10),
            timeLabel.centerYAnchor.constraint(equalTo: productCategoryLabel.centerYAnchor),
            
            productDescriptionLabel.leadingAnchor.constraint(equalTo: productCategoryLabel.leadingAnchor),
            productDescriptionLabel.topAnchor.constraint(equalTo: productCategoryLabel.bottomAnchor, constant: paddingBetweenLabels),
            productDescriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -paddingsAroundTexts),
        ])
        
        setCellSeparator()
    }
    
    func set(productDetail: ProductDetail) {
        productTitleLabel.text = productDetail.productTitle
        productCategoryLabel.text = productDetail.productCategory
        timeLabel.text = productDetail.time
        
        productDescriptionLabel.text = productDetail.productDescription
        
    }
}
