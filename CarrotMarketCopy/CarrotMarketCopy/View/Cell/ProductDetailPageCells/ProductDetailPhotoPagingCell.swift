//
//  ProductDetailPhotoPagingCell.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

class ProductDetailPhotoPagingCell: UICollectionViewCell {
    static let identifier = "ProductDetailCell"
    static let prefferedHeight: CGFloat = 100
    
    let horizontalScrollVC = HorizontalScrollCollectionViewController()
    
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        configure()
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        addSubview(horizontalScrollVC.view)
        horizontalScrollVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            horizontalScrollVC.view.topAnchor.constraint(equalTo: topAnchor),
            horizontalScrollVC.view.leadingAnchor.constraint(equalTo: leadingAnchor),
            horizontalScrollVC.view.bottomAnchor.constraint(equalTo: bottomAnchor),
            horizontalScrollVC.view.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
    func set(productDetail: ProductDetail) {
        productDetail.productImages
    }
}
