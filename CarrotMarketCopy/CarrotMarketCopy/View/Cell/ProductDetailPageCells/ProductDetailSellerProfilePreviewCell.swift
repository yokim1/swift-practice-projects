//
//  ProductDetailSellerProfilePreviewCell.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

class ProductDetailSellerProfilePreviewCell: UICollectionViewCell{
    static let identifier = "ProductDetailSellerProfilePreviewCell"
    static let prefferedHeight: CGFloat = 80
    
    let profileImageSideLength: CGFloat = 50
    
    let sellerProfileImageView: UIImageView = {
      let imageView = UIImageView(image: UIImage(systemName: "person"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .black
        
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
        return imageView
    }()
    let sellerName = CMBoldLabel(fontSize: 15)
    let sellerLocation = CMSecondaryLabel(fontSize: 15)
    
    let separator: UIView = {
       let uiView = UIView()
        uiView.backgroundColor = .lightGray
        uiView.alpha = 0.3
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        configure()
//    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        sellerProfileImageView.widthAnchor.constraint(equalToConstant: profileImageSideLength).isActive = true
        sellerProfileImageView.heightAnchor.constraint(equalToConstant: profileImageSideLength).isActive = true
        
        let labelStackView = UIStackView(arrangedSubviews: [sellerName, sellerLocation])
        labelStackView.axis = .vertical
        labelStackView.distribution = .equalSpacing
        
        let totalStackView = UIStackView(arrangedSubviews: [sellerProfileImageView, labelStackView])
        totalStackView.axis = .horizontal
        totalStackView.spacing = 10
        totalStackView.alignment = .center
        
        addSubviews(totalStackView)
        totalStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            totalStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            totalStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
        ])
        
        setCellSeparator()
    }
    
    func set(productDetail: ProductDetail) {
        sellerProfileImageView.image = UIImage(systemName: "person")
        sellerName.text = productDetail.sellerName
        sellerLocation.text = productDetail.sellerLocation
    }
}
