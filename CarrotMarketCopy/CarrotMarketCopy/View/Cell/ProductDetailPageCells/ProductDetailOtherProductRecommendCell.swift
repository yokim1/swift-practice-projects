//
//  File.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

class ProductDetailOtherProductRecommendCell: UICollectionViewCell {
    static let identifier = "ProductDetailOtherProductRecommendCell"
    static let prefferedHeight: CGFloat = 500
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//
//        configure()
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        
    }
    
    func set(productDetail: ProductDetail) {
        
    }
}
