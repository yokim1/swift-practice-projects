//
//  ProductDetail.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

struct ProductDetail {
    let productImages: [ProductImage]
    
    let sellerProfilePhoto: String
    let sellerName: String
    let sellerLocation: String
    let sellerTemparture: String
    
    let productTitle: String
    let productCategory: String
    let time: String
    
    let productDescription: String
    
    let productPrice: String
    let priceSuggestion: String
    let isLiked: Bool
    
    let sellerOtherProduct: [ProductPreview]
    
    let otherRecommendedProduct: [ProductPreview]
}

struct ProductImage{
    let productImageUrl: String
}

struct ProductPreview {
    let productImage: String
    let productTitle: String
    let productPrice: String
}
