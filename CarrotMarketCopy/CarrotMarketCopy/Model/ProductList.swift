//
//  ProductList.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

struct ProductList {
    
    let productImage: String?
    let productTitle: String
    let location: String
    let time: String
    let price: String
    
    let numberOfChatting: Int?
    let numberOfHeart: Int?
}
