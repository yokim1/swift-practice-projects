//
//  UITableViewCell+Extension.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

extension UITableViewCell {
    func setCellSeparator() {
        let separator: UIView = {
           let uiView = UIView()
            uiView.backgroundColor = .lightGray
            uiView.alpha = 0.3
            uiView.translatesAutoresizingMaskIntoConstraints = false
            return uiView
        }()
        
        addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            separator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.separatorPadding),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.separatorPadding),
            separator.heightAnchor.constraint(equalToConstant: 1),
        ])
    }
}

extension UICollectionViewCell {
    func setCellSeparator() {
        let separator: UIView = {
           let uiView = UIView()
            uiView.backgroundColor = .lightGray
            uiView.alpha = 0.3
            uiView.translatesAutoresizingMaskIntoConstraints = false
            return uiView
        }()
        
        addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            separator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.separatorPadding),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.separatorPadding),
            separator.heightAnchor.constraint(equalToConstant: 1),
        ])
    }
}
