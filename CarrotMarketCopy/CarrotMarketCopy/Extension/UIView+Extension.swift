//
//  UIView+Extension.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/18.
//

import UIKit

extension UIView {
    func addSubviews(_ views: UIView...) {
        for view in views {
            addSubview(view)
        }
    }
}
