//
//  UIViewController+Extension.swift
//  CarrotMarketCopy
//
//  Created by 김윤석 on 2021/07/17.
//

import UIKit

extension UIViewController{
    func hideTabBarWithAnimation(){
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut) {
            self.tabBarController?.tabBar.frame.origin.y += 100
        }
    }
    
    func showTabBarWithAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.5, options: .curveEaseOut) {
            
            self.tabBarController?.tabBar.frame.origin.y -= 100
        }
    }
    
    func addSubviews() {
        
    }
}
