//
//  NetworkManager.swift
//  practiceChart
//
//  Created by 김윤석 on 2021/07/29.
//

import UIKit

enum GFError: String, Error {
    case invalidUsername = "invalid username"
    case unableToComplete = "unable to complete"
    case invalidResponse = "invalid response"
    case invalidData = "invalid data"
    case alreadyExist = "The data already exist"
}


class NetworkManager {
    static let shared = NetworkManager()
    
    let apiKey = "&token=c3ecka2ad3ief4elg710"
    
    private let baseUrl = "/crypto/candle?symbol=BINANCE:BTCUSDT&resolution=D&from=1572651390&to=1575243390"
    
    private init(){ }
    
    func getData(for symbol: String, completionHandler: @escaping (Result<DataModel?, GFError>) -> Void) {
        let endPoint = "https://finnhub.io/api/v1/crypto/candle?symbol=\(symbol)&resolution=D&from=1572651390&to=1575243390" + apiKey
        
        guard let url = URL(string: endPoint) else { return completionHandler(.failure(.invalidUsername))}
        print(endPoint)
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let _ = error{
                print(url)
                completionHandler(.failure(.unableToComplete))
                return
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completionHandler(.failure(.invalidResponse))
                return
            }
            
            guard let data = data else {
                completionHandler(.failure(.invalidData))
                return
            }
            
            do {
                let decode = JSONDecoder()
                decode.keyDecodingStrategy = .convertFromSnakeCase
                let data = try decode.decode(DataModel.self, from: data)
                completionHandler(.success(data))
            } catch {
                print(error)
                completionHandler(.failure(.invalidData))
            }
            
        }.resume()
    }
}
