//
//  ViewController.swift
//  practiceChart
//
//  Created by 김윤석 on 2021/07/29.
//

import Charts
import UIKit

class Cell: UITableViewCell {
    static let identifier = "Cell"
    
    var viewModel: ViewModel = .init(data: [])
    
    private let chartView: LineChartView = {
       let chart = LineChartView()
        chart.pinchZoomEnabled = false
        chart.setScaleEnabled(true)
        chart.xAxis.enabled = false
        chart.drawGridBackgroundEnabled = false
        chart.leftAxis.enabled = false
        //chart.rightAxis.enabled = false
        chart.legend.enabled = false
        chart.translatesAutoresizingMaskIntoConstraints = false
        return chart
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureChart()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureChart(){
        addSubview(chartView)
        
        NSLayoutConstraint.activate([
            chartView.leadingAnchor.constraint(equalTo: leadingAnchor),
            chartView.topAnchor.constraint(equalTo: topAnchor),
            chartView.trailingAnchor.constraint(equalTo: trailingAnchor),
            chartView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func configure(with viewModel: ViewModel){
        
        var dataEntries:[ChartDataEntry] = []
        
        for (index, value) in viewModel.data.enumerated() {
            dataEntries.append(
                .init(
                    x: Double(index),
                    y: value))
        }
        
        let dataSet = LineChartDataSet(entries: dataEntries, label: "Some Label")
        dataSet.fillColor = .systemBlue
        dataSet.drawFilledEnabled = true
        dataSet.drawIconsEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.drawCirclesEnabled = false
        let data = LineChartData(dataSet: dataSet)
        chartView.data = data
    }
}

struct ViewModel {
    var data: [Double]
}

class ViewController: UIViewController {

    private let tableView: UITableView = {
        let table = UITableView()
        table.register(Cell.self,
                       forCellReuseIdentifier: Cell.identifier)
        return table
    }()
    
    var closeData: [Double] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .brown
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .blue
        
        fetchData()
    }
    
    func fetchData() {
        NetworkManager.shared.getData(for: "BINANCE:BTCUSDT") { result in
            switch result{
            case .success(let data):
                
                print(data!.o)
                
                self.closeData = data!.o
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as? Cell else {return UITableViewCell()}
        cell.configure(with: .init(data: closeData))
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        300
    }
}
