//
//  ViewController.swift
//  practiceMapKit
//
//  Created by 김윤석 on 2021/08/31.
//
import MapKit
import CoreLocation
import UIKit

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    var myLocation: CLLocationCoordinate2D?
    
    let mapView = MKMapView()
    let manager = CLLocationManager()
    
    lazy var button1: UIButton = {
       let button = UIButton()
        button.setTitle("My Location", for: .normal)
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(button1DidTap), for: .touchUpInside)
        return button
    }()
    
    lazy var button2: UIButton = {
       let button = UIButton()
        button.setTitle("Center Location", for: .normal)
        button.backgroundColor = .red
        button.addTarget(self, action: #selector(button2DidTap), for: .touchUpInside)
        return button
    }()
    
    @objc private func button1DidTap() {
        guard let lat = self.myLocation?.latitude,
              let lng = self.myLocation?.longitude else {return }
        let targetCoordinate = CLLocationCoordinate2D(latitude: lat,
                                                      longitude: lng)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.1,
                                    longitudeDelta: 0.1)
        
        let region = MKCoordinateRegion(center: targetCoordinate, span: span)
        
        mapView.setRegion( region, animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = targetCoordinate
        
        mapView.addAnnotation(pin)
    }
    
    @objc private func button2DidTap() {
        //manager.stopUpdatingLocation()
        
        let targetCoordinate = CLLocationCoordinate2D(latitude: 37.567817,
                                                    longitude: 127.004501)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.1,
                                    longitudeDelta: 0.1)
        
        let region = MKCoordinateRegion(center: targetCoordinate, span: span)
        
        mapView.setRegion( region, animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = targetCoordinate
        
        mapView.addAnnotation(pin)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMapView()
        configureLocationManager()
        configureButtons()
    }
    
    private func configureMapView() {
        mapView.frame = view.bounds
        view.addSubview(mapView)
    }
    
    private func configureLocationManager() {
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    private func configureButtons() {
        
        let stackView = UIStackView(arrangedSubviews: [button1, button2])
        stackView.spacing = 8
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        mapView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: mapView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: mapView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: mapView.bottomAnchor),
        ])
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            manager.stopUpdatingLocation()
            
            self.myLocation = location.coordinate
            
            let targetCoordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                                          longitude: location.coordinate.longitude)
            
            let span = MKCoordinateSpan(latitudeDelta: 0.1,
                                        longitudeDelta: 0.1)
            
            let region = MKCoordinateRegion(center: targetCoordinate, span: span)
            
            mapView.setRegion( region, animated: true)
            
            let pin = MKPointAnnotation()
            pin.coordinate = targetCoordinate
            
            mapView.addAnnotation(pin)
        }
    }
}

