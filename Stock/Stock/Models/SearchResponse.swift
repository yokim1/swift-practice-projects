//
//  SearchResponse.swift
//  Stock
//
//  Created by 김윤석 on 2021/07/01.
//

import Foundation

struct SearchResponse: Codable{
    let count: Int
    let result: [SearchResult]
}

struct SearchResult: Codable {
    let description: String
    let displaySymbol: String
    let symbol: String
    let type: String
}


