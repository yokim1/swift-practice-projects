//
//  NetworkManager.swift
//  Stock
//
//  Created by 김윤석 on 2021/06/30.
//

import UIKit

final class NetworkManager {
    static let shared = NetworkManager()
    
    //MARK: - Public
    
    public func search(query: String, completionHandler: @escaping (Result<SearchResponse, APIError>) -> Void) {
        guard let safeQuery = query.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) else {return }
        guard let url = url(for: .search, queryParams: ["q" : safeQuery]) else {return }
        request(url: url, expecting: SearchResponse.self, completionHandler: completionHandler)
    }
    
    public func news(for type: Type, completionHandler: @escaping (Result<[NewsStory], APIError>)-> Void) {
        
        let urlCall: URL?
        
        switch type {
        case .topStories:
            urlCall = url(for: .topStories, queryParams: ["category": "general"])
            
        case .company(let symbol):
            let today = Date()
            let oneMonthBack = today.addingTimeInterval(-(Constants.day * 7))
            urlCall = url(for: .companyNews,
                          queryParams:
                            ["symbol" : symbol,
                             "from": DateFormatter.newsFormatter.string(from: oneMonthBack),
                             "to": DateFormatter.newsFormatter.string(from: today)])
            
        }
        
        request(url: urlCall, expecting: [NewsStory].self, completionHandler: completionHandler)
    }
    
    public func marketData(symbol: String, numberOfDays: TimeInterval = 7, completionHandler: @escaping (Result<MarketDataResponse, APIError>)-> Void) {
        let today = Date().addingTimeInterval(-(Constants.day))
        let prior = today.addingTimeInterval(-(Constants.day * numberOfDays))
        let url = url(for: .marketData, queryParams: ["symbol": symbol,
                                                      "resolution": "1",
                                                      "from": "\(Int(prior.timeIntervalSince1970))",
                                                      "to": "\(Int(today.timeIntervalSince1970))"])
        
        request(url: url, expecting: MarketDataResponse.self, completionHandler: completionHandler)
    }
    
    //MARK: - Private
    
    private struct Constants {
        static let apiKey = "c3ecka2ad3ief4elg710"
        static let sandboxApiKey = "sandbox_c3ecka2ad3ief4elg71g"
        static let baseUrl = "https://finnhub.io/api/v1/"
        static let day: TimeInterval = 3600 * 24
    }
    
    private enum EndPoint: String {
        case search
        case topStories = "news"
        case companyNews = "company-news"
        case marketData = "stock/candle"
    }
    
    enum APIError: Error {
        case noDataReturned
        case invalidUrl
    }
    
    private func url(for endPoint: EndPoint, queryParams: [String: String] = [:]) -> URL? {
        var urlString = Constants.baseUrl + endPoint.rawValue
        var queryItems = [URLQueryItem]()
        
        // Add any parameters
        queryParams.forEach { item in
            queryItems.append(.init(name: item.key, value: item.value))
        }
        
        // Add token
        queryItems.append(.init(name: "token", value: Constants.apiKey))
        
        urlString += "?" + queryItems.map { item in
            "\(item.name)=\(item.value ?? "")"
        }.joined(separator: "&")
        
        
        print(urlString)
        return URL(string: urlString)
    }
    
    private func request<T: Codable>(url: URL?, expecting: T.Type, completionHandler: @escaping (Result<T, APIError>)->Void){
        guard let url = url else {
            completionHandler(.failure(.invalidUrl))
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data,
                  error == nil else { return }
            
            do {
                let result = try JSONDecoder().decode(expecting, from: data)
                completionHandler(.success(result))
            } catch {
                print(error)
                completionHandler(.failure(.noDataReturned))
            }
        }.resume()
        
    }
    
    private init(){}
}
