//
//  ViewController.swift
//  Stock
//
//  Created by 김윤석 on 2021/06/30.
//

import UIKit
import FloatingPanel

class WatchListViewController: UIViewController {
    
    private var floatingPanel: FloatingPanelController?
    private var searchTimer: Timer?
    private var tableView: UITableView!
    
    private var viewModels: [WatchListTableViewCell.ViewModel] = []
    
    private var watchListMap: [String: [CandleStick]] = [:]
    
    static var maxChangeWidth:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //PersistanceManager.shared.removeFromWatchList(symbol: "APPL")
        
        view.backgroundColor = .clear
        setUpSearchController()
        setUpTableView()
        fetchWatchListData()
        setUpTitleView()
        setUpFloatingPanel()
        
        //print(viewModels)
    }
    
    //MARK: - Private

    private func setUpSearchController() {
        let resultVC = SearchResultsViewController()
        resultVC.delegate = self
        let searchVC = UISearchController(searchResultsController: resultVC)
        searchVC.searchResultsUpdater = self
        navigationItem.searchController = searchVC
    }
    
    private func setUpTableView() {
//        tableView.frame = view.bounds
        tableView = UITableView(frame: view.bounds, style: .plain)
        tableView.register(WatchListTableViewCell.self,
                           forCellReuseIdentifier: WatchListTableViewCell.identifier)
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func fetchWatchListData() {
        let symbols = PersistanceManager.shared.watchList
        
        let group = DispatchGroup()
        for symbol in symbols {
            group.enter()
            //watchListMap[symbol] = ["Some String"]
            NetworkManager.shared.marketData(symbol: symbol) {[weak self] result in
                defer{
                    group.leave()
                }
                switch result{
                case .success(let response):
                    let candleStickData = response.candleSticks
                    self?.watchListMap[symbol] = candleStickData
                    //print(candleStickData)
                case .failure(let error):
                    print(error)
                }
            }
        }
        
        tableView.reloadData()
        group.notify(queue: .main) {[weak self] in
            guard let self = self else {return}
            self.createViewModels()
            self.tableView.reloadData()
        }
    }
    
    private func createViewModels() {
        var viewModel = [WatchListTableViewCell.ViewModel]()
        for (symbol, candleStick) in watchListMap {
            let changePercentage = getChangePercentage(symbol: symbol, for: candleStick)
            viewModel.append(.init(symbol: symbol,
                                   companyName: UserDefaults.standard.string(forKey: symbol) ?? "Company",
                                   price: getLatestClosingPrice(from: candleStick),
                                   changeColor: changePercentage < 0 ? .systemRed : .systemGreen,
                                   changePercentage: .percentage(from: changePercentage),
                                   chartViewModel: .init(data: candleStick.reversed().map{ $0.close },
                                                         showLegend: false,
                                                         showAxis: false)))
        }
        print("\(viewModel)")
        self.viewModels = viewModel
    }
    
    private func getChangePercentage(symbol: String, for data: [CandleStick]) -> Double {
        //let priorDate = Date().addingTimeInterval(-(3600 * 24 * 2))
        let latestDate = data[0].date
        guard let latestClose = data.first?.close,
              let priorClose = data.first(where: {
                !Calendar.current.isDate($0.date, inSameDayAs: latestDate)
              })?.close else { return 0 }
        
//        print("\(symbol): Current \(latestDate) : \(latestClose) | prior: \(priorClose)")
//        print("\(1 - (priorClose / latestClose))")
//
        return (priorClose/latestClose)
    }
    
    private func getLatestClosingPrice(from data: [CandleStick]) -> String {
        guard let closingPrice = data.first?.close else {
            return ""
        }
        
        return .formatted(number: closingPrice)
    }
    
    
    private func setUpTitleView() {
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: navigationController?.navigationBar.frame.size.height ?? 100))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width - 20, height: titleView.frame.size.height))
        label.text = "Stocks"
        label.font = .systemFont(ofSize: 32, weight: .medium)
        titleView.addSubview(label)
        navigationItem.titleView = titleView
    }
    
    private func setUpFloatingPanel() {
        let vc = NewsViewController(type: .topStories)
        let panel = FloatingPanelController()
        panel.delegate = self
        panel.surfaceView.backgroundColor = .red
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
    }
}

extension WatchListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text,
              let resultsVC = searchController.searchResultsController as? SearchResultsViewController,
              !query.trimmingCharacters(in: .whitespaces).isEmpty else { return }
        
        // optimize to reduce number of searches for when users stop typing
        
        //reset timer
        searchTimer?.invalidate()
        
        // Network call
        searchTimer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { _ in
            NetworkManager.shared.search(query: query) { result in
                switch result {
                case .success(let searchResponse):
                    DispatchQueue.main.async {
                        resultsVC.update(with: searchResponse.result)
                    }
                
                case .failure(let error):
                    DispatchQueue.main.async {
                        resultsVC.update(with: [])                    }
                }
            }
        })
        
    }
}

extension WatchListViewController: SearchResultsViewControllerDelegate {
    func SearchResultsViewControllerDidSelect(searchResult: SearchResult) {
        let vc = StockDetailsViewController()
        vc.title = searchResult.description
        let navVC = UINavigationController(rootViewController: vc)
        present(navVC, animated: true, completion: nil)
    }
}

extension WatchListViewController: FloatingPanelControllerDelegate {
    func floatingPanelDidChangeState(_ fpc: FloatingPanelController) {
        navigationItem.titleView?.isHidden = fpc.state == .full
    }
}

extension WatchListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(watchListMap.count)
        return watchListMap.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WatchListTableViewCell.identifier, for: indexPath) as? WatchListTableViewCell else{
            return UITableViewCell()
        }
        cell.delegate = self
        cell.configure(with: viewModels[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        WatchListTableViewCell.prefferedHeight
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
//        if editingStyle == .delete {
//            tableView.beginUpdates()
//            // update persistance
//            PersistanceManager.shared.removeFromWatchList(symbol: viewModels[indexPath.row].symbol)
//
//            // update viewModels
//            viewModels.remove(at: indexPath.row)
//
//            // delete row
//            tableView.deleteRows(at: [indexPath], with: .fade)
//            tableView.endUpdates()
//        }
        
        if editingStyle == .delete {
            tableView.beginUpdates()

            // Update persistence
            PersistanceManager.shared.removeFromWatchList(symbol: viewModels[indexPath.row].symbol)

            // Update viewModels
            viewModels.remove(at: indexPath.row)

            // Delete Row
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.reloadData()
            tableView.endUpdates()
        }
        
    }
}
extension WatchListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension WatchListViewController: WatchListTableViewCellDelegate {
    func didUpdateMaxWidth() {
        tableView.reloadData()
    }
}
