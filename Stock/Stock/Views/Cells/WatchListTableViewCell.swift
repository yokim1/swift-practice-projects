//
//  WatchListTableViewCell.swift
//  Stock
//
//  Created by 김윤석 on 2021/07/03.
//

import UIKit

protocol WatchListTableViewCellDelegate: AnyObject {
    func didUpdateMaxWidth()
}

class WatchListTableViewCell: UITableViewCell {

    static let identifier = "WatchListTableViewCell"
    
    static let prefferedHeight: CGFloat = 60
    
    weak var delegate: WatchListTableViewCellDelegate?

    struct ViewModel {
        let symbol: String
        let companyName: String
        let price: String
        let changeColor: UIColor
        let changePercentage: String
        let chartViewModel: StockChartView.ViewModel
    }
    
    private let symbolLabel: UILabel = {
       let label = UILabel()
        label.font = .systemFont(ofSize: 16, weight: .heavy)
        return label
    }()
    
    private let companyLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15, weight: .medium)
         return label
    }()
    
    private let priceLabel: UILabel = {
       let label = UILabel()
        label.font = .systemFont(ofSize: 15, weight: .regular)
        return label
    }()
    
    private let priceChangeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 14, weight: .regular)
        label.textAlignment = .right
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 4
        return label
    }()
    
    private let miniChartView = StockChartView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubviews(symbolLabel, companyLabel, priceLabel, priceChangeLabel, miniChartView)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        symbolLabel.sizeToFit()
        companyLabel.sizeToFit()
        priceLabel.sizeToFit()
        priceChangeLabel.sizeToFit()
        
        let yStart: CGFloat = (contentView.height - symbolLabel.height - companyLabel.height)/2
        
        symbolLabel.frame = CGRect(x: separatorInset.left,
                                   y: yStart,
                                   width: symbolLabel.width,
                                   height: symbolLabel.height)
 
        companyLabel.frame = CGRect(x: separatorInset.left,
                                    y: symbolLabel.bottom,
                                   width: companyLabel.width,
                                   height: companyLabel.height)
        
        let currentWidth = max(max(priceLabel.width, priceChangeLabel.width), WatchListViewController.maxChangeWidth)
        
        if currentWidth > WatchListViewController.maxChangeWidth {
            WatchListViewController.maxChangeWidth = currentWidth
            delegate?.didUpdateMaxWidth()
        }
        
        priceLabel.frame = CGRect(x: contentView.width - 10 - currentWidth,
                                  y: (contentView.height - priceLabel.height - priceChangeLabel.height)/3,
                                  width: currentWidth,
                                  height: priceLabel.height)
        
        priceChangeLabel.frame = CGRect(x: contentView.width - 10 - currentWidth,
                                        y: priceLabel.bottom,
                                        width: currentWidth,
                                        height: priceChangeLabel.height)
        
        miniChartView.frame = CGRect(x: priceLabel.left - (contentView.width/3) - 5,
                                     y: 6,
                                     width: contentView.width/3,
                                     height: contentView.height - 12)
        
        //priceChangeLabel.frame = CGRect(x: <#T##CGFloat#>, y: <#T##CGFloat#>, width: <#T##CGFloat#>, height: <#T##CGFloat#>)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        symbolLabel.text = nil
        companyLabel.text = nil
        priceLabel.text = nil
        priceChangeLabel.text = nil
        miniChartView.reset()
    }
    
    func configure(with viewModel: ViewModel) {
        symbolLabel.text = viewModel.symbol
        companyLabel.text = viewModel.companyName
        priceLabel.text = viewModel.price
        priceChangeLabel.text = viewModel.changePercentage
        priceChangeLabel.backgroundColor = viewModel.changeColor
        //configure chart
    }
    
}


