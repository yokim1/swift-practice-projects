//
//  UserInformation.swift
//  LoginAndCreateAccount
//
//  Created by 김윤석 on 2021/01/20.
//

import UIKit

class UserInformation{
    static let shared = UserInformation()
    
    var imageView: UIImageView?
    var ID: String?
    var password: String?
    var description: String?
    var phoneNumber: String?
    var birthDate: String?
    
    private init(){
        
    }
}
