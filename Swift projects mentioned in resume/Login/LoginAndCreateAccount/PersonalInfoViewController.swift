//
//  PersonalInfoViewController.swift
//  LoginAndCreateAccount
//
//  Created by 김윤석 on 2021/01/14.
//

import UIKit

protocol PersonalInfoViewControllerDelegate: class {
    func didTapPreviousButton(_ personalInfoViewController: PersonalInfoViewController)
    func didTapCancelButton(_ personalInfoViewController: PersonalInfoViewController)
}

class PersonalInfoViewController: UIViewController {
    
    @IBOutlet weak var phoneNumberLabel: UITextField!
    @IBOutlet weak var birthDateLabel: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var userInformation = UserInformation.self
    
    var delegate : PersonalInfoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func datePickerWheel(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        birthDateLabel.text = dateFormatter.string(from: datePicker.date)
    }
    
    //확인 버튼을 누르면 전화번호와 생일을 받고, 그것을 기존 struct에 업데이트 해준다.
    //그리고 처음 페이지를 띄어준다.
    @IBAction func didTapSignUpButton(_ sender: Any) {
        userInformation.shared.phoneNumber = phoneNumberLabel.text
        userInformation.shared.birthDate = birthDateLabel.text
        //openViewController()
    }
    
    @IBAction func didTapPreviousButton(_ sender: Any) {
        self.delegate?.didTapPreviousButton(self)
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        userInformation.shared.imageView = nil
        userInformation.shared.ID = nil
        userInformation.shared.password = nil
        
        self.delegate?.didTapCancelButton(self)
    }
}
