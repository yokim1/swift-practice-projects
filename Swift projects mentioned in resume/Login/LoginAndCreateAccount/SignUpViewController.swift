//
//  SignUpViewController.swift
//  LoginAndCreateAccount
//
//  Created by 김윤석 on 2021/01/13.
//

import UIKit

protocol SignUpviewControllerDelegate: class{
    func didTapCancelButton(_ SingUpViewController: SignUpViewController)
}

class SignUpViewController: UIViewController, PersonalInfoViewControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var newIDLabel: UITextField!
    @IBOutlet weak var newPasswordLabel: UITextField!
    @IBOutlet weak var confirmNewPasswordLabel: UITextField!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    //var userInformation: UserInfo?
    let userInformation = UserInformation.self
    
    var delegate : SignUpviewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectImageButton(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = false
//        vc.modalPresentationStyle

        present(vc, animated: true, completion: nil)
    }
    
    //확인 버튼을 누르면, 새로운 사용자에 대한 정보를 가지고 struct를 만들어준다.
    //그리고 그 정보를 다음 viewController한테 pass
    @IBAction func didTapConfirmButton(_ sender: Any) {
        
        if ( isPasswordConfirm() && !isEmptyLabels()) {
            
            userInformation.shared.imageView = imageView
            userInformation.shared.ID = String(newIDLabel.text ?? "")
            userInformation.shared.password = String(newPasswordLabel.text ?? "")
            userInformation.shared.description = String(descriptionLabel.text)
            
            openPersonalInfoViewController()
        }
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        userInformation.shared.imageView = nil
        userInformation.shared.ID = nil
        userInformation.shared.password = nil
        self.delegate?.didTapCancelButton(self)
    }
    
    //new password and confirm password are same?
    private func isPasswordConfirm() -> Bool{
        if(newPasswordLabel.text == confirmNewPasswordLabel.text){
            return true
        }
        return false
    }
    
    // is there any empty labels?
    private func isEmptyLabels() -> Bool {
        if(newIDLabel.text?.count == 0 || newPasswordLabel.text?.count == 0 || confirmNewPasswordLabel.text?.count == 0){
            return true
        }
        return false
    }
    
    //show next slide
    private func openPersonalInfoViewController(){
        if let personalInfoViewController = storyboard?.instantiateViewController(identifier: "PersonalInfoViewController") as? PersonalInfoViewController {
                personalInfoViewController.modalTransitionStyle = .coverVertical
                personalInfoViewController.modalPresentationStyle = .fullScreen
                personalInfoViewController.delegate = self
            
                present(personalInfoViewController, animated: true)
            }
    }
    
    func didTapPreviousButton(_ personalInfoViewController: PersonalInfoViewController) {
        personalInfoViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapCancelButton(_ personalInfoViewController: PersonalInfoViewController) {
        personalInfoViewController.dismiss(animated: true)
        self.delegate?.didTapCancelButton(self)
    }
    
}

//For image upload
extension SignUpViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage{
            imageView.image = image
            imageView.contentMode = .scaleToFill
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
