//
//  Comment.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/10.
//

struct Comment : Encodable{
    let rating: Double
    let writer: String
    let movie_id: String
    let contents: String
}
