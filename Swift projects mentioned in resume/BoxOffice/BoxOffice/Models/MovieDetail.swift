//
//  MovieDetail.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/01.
//

struct MovieDetail : Decodable {
    let synopsis: String
    let userRating: Double
    let date: String
    let title: String
    let grade: Int
    let reservationRate: Double
    let duration: Int
    let director: String
    let image: String
    let reservationGrade: Int
    let genre: String
    let id: String
    let audience: Int
    let actor: String
    
    enum CodingKeys: String, CodingKey {
        case synopsis
        case userRating = "user_rating"
        case date
        case title
        case grade
        case reservationRate = "reservation_rate"
        case duration
        case director
        case image
        case reservationGrade = "reservation_grade"
        case genre
        case id
        case audience
        case actor
    }
}
