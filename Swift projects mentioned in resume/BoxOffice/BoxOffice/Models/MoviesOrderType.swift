//
//  MoviesOrderType.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/18.
//


enum MoviesOrderType: CaseIterable {
    case greatestReservationRate
    case latestDate
    case greatestUserRating
    
    var title: String {
        switch self {
        case .greatestReservationRate:
            return "예매율"
        case .greatestUserRating:
            return "큐레이션"
        case .latestDate:
            return "개봉일"
        }
    }
}
