//
//  WriteCommentViewController.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/04.
//

import UIKit
protocol WriteCommentViewControllerDelegate: class {
    func cancelButtonWasTapped(_ writeCommentViewController: WriteCommentViewController)
    func completeButtonWasTapped(_ writeCommentViewController: WriteCommentViewController)
}

class WriteCommentViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBOutlet weak var writerTextField: UITextField!
    @IBOutlet weak var commentTextFiedl: UITextField!
    
    @IBAction private func cancelButtonWasTapped(_ sender: Any) {
        self.delegate?.cancelButtonWasTapped(self)
    }
    
    @IBAction private func completeButtonWasTapped(_ sender: Any) {
        let info = MovieComment(id: "", rating: 5.0, timestamp: 0.0, writer: writerTextField.text ?? "", movieId: self.movieId, contents: commentTextFiedl.text ?? "")
        
        requestMovieCommentToPost(info)
        self.delegate?.completeButtonWasTapped(self)
    }
    
    var movieRequester = MovieRequester()
    var movieId: String = ""
    var delegate: WriteCommentViewControllerDelegate?
}

private extension WriteCommentViewController {
   func requestMovieCommentToPost(_ info: MovieComment) {
        do{
            let jsonData = try JSONEncoder().encode(info)
            
            if let url = BoxOfficeURLConfigurationMaker(path: "comment", query: ["":""]).url {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.post.rawValue
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = jsonData
                
                movieRequester.request(with: urlRequest) { _ in }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
