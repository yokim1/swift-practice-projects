//
//  Delegate.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/09.
//

import UIKit

extension CollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieGridCell", for: indexPath) as? MovieGridCell else { return UICollectionViewCell() }
        
        cell.titleLabel.text = self.movies[indexPath.row].title
        cell.reservationGradeLabel.text = "예매순위: \(self.movies[indexPath.row].reservationGrade)"
        cell.reservationRateLabel.text = "예매율: \(self.movies[indexPath.row].reservationRate)"
        cell.dateLabel.text = self.movies[indexPath.row].date
        
        if let urlString = movies[indexPath.row].thumb as String?,
           let url = URL(string: urlString)
        {
            URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                if let data = data {
                    DispatchQueue.main.async {
                        cell.movieImage.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        return cell
    }
}
