//
//  TableViewDataSource.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/10.
//

import UIKit

extension DetailMovieViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movieComments.count
    }
}
