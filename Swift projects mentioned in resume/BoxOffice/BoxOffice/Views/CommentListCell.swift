//
//  CommentListCell.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/09.
//

import UIKit

class CommentListCell: UITableViewCell {

    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var contentsLabel: UILabel!
}
