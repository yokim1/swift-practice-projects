//
//  MovieCommentRequester.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/10.
//

struct MovieCommentRequester: Requester {
    
    typealias ResponseType = MovieCommentResponse
    
}
