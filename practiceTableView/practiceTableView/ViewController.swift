//
//  ViewController.swift
//  practiceTableView
//
//  Created by 김윤석 on 2021/04/15.
//

import UIKit

class Section {
    let title: String
    let options: [String]
    var isOpened = false
    
    init(title: String,
         options: [String],
         isOpened: Bool = false) {
        self.title = title
        self.options = options
        self.isOpened = isOpened
    }
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var sections = [Section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        sections = [
            Section(title: "Section 1", options: [1,2,3].compactMap({return "Cell \($0)"})),
            Section(title: "Section 2", options: [4,5,6].compactMap({return "Cell \($0)"})),
            Section(title: "Section 3", options: [7,8,9].compactMap({return "Cell \($0)"}))
        ]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print("Sections Count: \(sections.count)")
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        
        //print("Section: \(section)")
        
        if section.isOpened {
            return section.options.count + 1
        }
        else {
            return sections.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        print(indexPath.row)
        
        if indexPath.row == 0 {
            cell.textLabel?.text = sections[indexPath.row].title
            
        } else {
            cell.textLabel?.text = sections[indexPath.row].options[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        sections[indexPath.section].isOpened = !sections[indexPath.row].isOpened
        tableView.reloadSections([indexPath.section], with: .none)
    }
}
