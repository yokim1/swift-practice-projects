//
//  Service.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//
import UIKit


// psick univ channel id: UCGX5sP4ehBkihHwt5bs5wvg
//key : AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28

//https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=20

//https://www.youtube.com/watch?v=xXWyJEo6VgA&list={playlist Id}

//https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date

//https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date

//https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date

// 비대면 id: PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp
//https://www.youtube.com/watch?v=xXWyJEo6VgA&list=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp
//list=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp
//https://youtube.com/playlist?list=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp


//https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date&maxResults=20

//https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date&maxResults=20

//김윤석의 재생 목록https://youtube.com/playlist?list=PLRmKGEHn3NPonxwDHafpif6mIHP4Adp1d

class Service {
    static let shared = Service()
    
    func fetchApps(searchTerm: String, completion: @escaping ([Item], Error?)->()){
        
        //let urlString = "https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvgpart=snippet,id&order=date"
       
        guard let url = URL(string: searchTerm) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                completion([], nil)
                return
            }
            
            //success
            //            print(data)
            //            print(String(data: data!, encoding: .utf8))
            guard let data = data else {return}
            do {
                let searchResult = try JSONDecoder().decode(PlayListService.self, from: data)
                //print(searchResult.results.forEach({ print($0.trackName, $0.primaryGenreName)}))
                print(searchResult.items.forEach({
                    print($0.id)
                    print($0.snippet.thumbnails.medium)
                }))
                print("resultsPerPage: \(searchResult.pageInfo?.resultsPerPage)")
                print("TotalResults: \(searchResult.pageInfo?.totalResults)")
                //self.appResult = searchResult.results
                //                DispatchQueue.main.async {
                //                    self.collectionView.reloadData()
                //                }
                //completion(searchResult.results, nil)
                completion(searchResult.items, nil)
                
            } catch let Jsonerror {
                completion([], Jsonerror)
                print(Jsonerror.localizedDescription)
                print(Jsonerror)
                print("Failed To Decode")
            }
            
        }.resume()
    }
}
