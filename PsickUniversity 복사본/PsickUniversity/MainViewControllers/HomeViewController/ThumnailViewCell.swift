//
//  ThumnailViewCell.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import UIKit


class VideoThumbNailCell: UICollectionViewCell {
    
    @IBOutlet var thumbnail: UIImageView!
    
}
