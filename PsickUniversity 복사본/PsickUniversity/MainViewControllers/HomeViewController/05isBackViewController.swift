//
//  05isBackViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import UIKit

class OldisBackViewController: UIViewController {

    @IBOutlet weak var oldIsBackCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        oldIsBackCollectionView.delegate = self
        oldIsBackCollectionView.dataSource = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.oldIsBackCollectionView.reloadData()
        }
    }
}

extension OldisBackViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Manager.shared.oldisBackItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OldIsBackThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}

        let url = URL(string: Manager.shared.oldisBackItems[indexPath.item].snippet.thumbnails.medium.url)
        
        cell.thumbnail.sd_setImage(with: url, completed: nil)
        return cell
    }
}

extension OldisBackViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did tapped")
    }
}
