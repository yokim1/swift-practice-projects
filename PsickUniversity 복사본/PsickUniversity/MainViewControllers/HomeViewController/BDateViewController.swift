//
//  BDateViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/23.
//

import UIKit
import SDWebImage

//https://www.youtube.com/watch?v=xXWyJEo6VgA&list=RDxXWyJEo6VgA

class BDateViewController: UIViewController {
    
    @IBOutlet weak var bDateCollectionView: UICollectionView!
    
    let urlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date&maxResults=45"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bDateCollectionView.delegate = self
        bDateCollectionView.dataSource = self
        
        //        DispatchQueue.main.async {
        //            Service.shared.fetchApps(searchTerm: "") { items, error in
        //                self.model = items
        //                print(self.model.count)
        //            }
        //        }
        
//        Service.shared.fetchApps(searchTerm: urlString) { items, error in
//            Manager.shared.bDate = items
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.bDateCollectionView.reloadData()
        }
    }
}

extension BDateViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(Manager.shared.bDateItems.count)
        return Manager.shared.bDateItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BDateThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}
        
        
        let url = URL(string: Manager.shared.bDateItems[indexPath.item].snippet.thumbnails.medium.url)
        
        cell.thumbnail.sd_setImage(with: url)

        return cell
    }
    
}

extension BDateViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("tapped cell\(indexPath.item)")
    }
}

