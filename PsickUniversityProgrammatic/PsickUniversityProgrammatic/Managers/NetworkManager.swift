//
//  NetworkManager.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/20.
//

import Foundation

class NetworkManager {
    static let shared = NetworkManager()
    
    private let baseUrl = "https://www.googleapis.com/youtube/v3/search?"
    private let channelId = "UCGX5sP4ehBkihHwt5bs5wvg"
    private let apiKey = "AIzaSyCtGLAQI9IIdq0YZHIO1Z9nn7sb3Cr6GJ8"
 
    func fetchVideos(nextPageToken:String = "", completionHandler: @escaping (Result<VideoInfo?, GFError>) -> Void) {
        let endPoint = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyCtGLAQI9IIdq0YZHIO1Z9nn7sb3Cr6GJ8&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=10&pageToken=\(nextPageToken)"
        //&pageToken=CAUQAA
        
        guard let url = URL(string: endPoint) else { return completionHandler(.failure(.invalidUsername))}
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let _ = error {
                completionHandler(.failure(.unableToComplete))
                return
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completionHandler(.failure(.invalidResponse))
                return
            }
            
            guard let data = data else {
                completionHandler(.failure(.invalidData))
                return
            }
            
            do {
                let decode = JSONDecoder()
                decode.keyDecodingStrategy = .convertFromSnakeCase
                let data = try decode.decode(VideoInfo.self, from: data)
                completionHandler(.success(data))
            } catch {
                completionHandler(.failure(.invalidData))
            }
            
        }.resume()
    }
    
    private init(){}
}
