//
//  VideoInfo.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/20.
//

import Foundation

//{
//  "kind": "youtube#searchListResponse",
//  "etag": "-J4l8qoBcYO6K_uyj8zK8uSUpS8",
//  "nextPageToken": "CAUQAA",
//  "regionCode": "KR",
//  "pageInfo": {
//    "totalResults": 440,
//    "resultsPerPage": 5
//  },
//
//  "items": [{
//      "kind": "youtube#searchResult",
//      "etag": "zmo9HV8Ll9lP-wudVD9Oe2Sqmog",
//      "id": {
//        "kind": "youtube#video",
//        "videoId": "zPsiPvNod08"
//      },
//      "snippet": {
//        "publishedAt": "2021-06-20T11:00:11Z",
//        "channelId": "UCGX5sP4ehBkihHwt5bs5wvg",
//        "title": "[한사랑산악회] 음식이 곧 약이고 약은 곧 음식이다,,,",
//        "description": "한사랑산악회#건강식품 #뉴틴 *본 영상은 뉴틴의 유료 광고를 포함하고 있습니다.",
//        "thumbnails": {
//          "default": {
//            "url": "https://i.ytimg.com/vi/zPsiPvNod08/default.jpg",
//            "width": 120,
//            "height": 90
//          },
//          "medium": {
//            "url": "https://i.ytimg.com/vi/zPsiPvNod08/mqdefault.jpg",
//            "width": 320,
//            "height": 180
//          },
//          "high": {
//            "url": "https://i.ytimg.com/vi/zPsiPvNod08/hqdefault.jpg",
//            "width": 480,
//            "height": 360
//          }
//        },
//        "channelTitle": "피식대학Psick Univ",
//        "liveBroadcastContent": "none",
//        "publishTime": "2021-06-20T11:00:11Z"
//      }
//    }]
//}
//

struct VideoInfo: Decodable {
    let nextPageToken: String?
    let prevPageToken: String?
    let items: [Item]
}

struct Item: Decodable {
    let id: Id?
    let snippet: Snippet
}

struct Id: Decodable{
    let videoId: String?
}

struct Snippet: Decodable {
    let title: String
    let thumbnails: Thumbnail
}

struct Thumbnail: Decodable {
    let high: ThumbnailInfoDetail?
    let maxres: ThumbnailInfoDetail?
}

struct ThumbnailInfoDetail: Decodable {
    let url: String
    let width: Int
    let height: Int
}
