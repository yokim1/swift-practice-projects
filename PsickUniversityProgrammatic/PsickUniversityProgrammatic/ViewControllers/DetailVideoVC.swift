//
//  DetailVideoVC.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/21.
//

import UIKit
import WebKit

class DetailVideoVC: UIViewController {

    var videoInfo: Item?
    
    var videoPlayer: WKWebView = {
       let webView = WKWebView()
        webView.backgroundColor = .systemRed
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        configureVideoPlayer()
        print(videoInfo?.id?.videoId ?? "")
        playVideo(from: videoInfo?.id?.videoId ?? "")
    }
    
    private func configureVideoPlayer() {
        view.addSubview(videoPlayer)
        NSLayoutConstraint.activate([
            videoPlayer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            videoPlayer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            videoPlayer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            videoPlayer.heightAnchor.constraint(equalToConstant: view.frame.width * 9 / 16)
        ])
    }
    
    private func playVideo(from videoID: String) {
        guard let videoUrl = URL(string: "https://www.youtube.com/embed/\(videoID)") else { return }
        print(videoUrl)
        videoPlayer.load(URLRequest(url: videoUrl))
    }
}
