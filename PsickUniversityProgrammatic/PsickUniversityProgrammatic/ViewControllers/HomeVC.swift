//
//  ViewController.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/20.
//

import UIKit

class HomeVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        getVideos()
    }
    
    var data: [Item] = []
    var hasMoreVideos = true
    var nextPageToken: String = ""
    var collectionView: UICollectionView?
    
    private func getVideos(nextPageToken: String = "") {
        showLoadingView()
        NetworkManager.shared.fetchVideos(nextPageToken: nextPageToken) {[weak self] result in
            guard let self = self else { return }
            self.dismissLoadingView()
            
            switch result {
            case .success(let data):
                if data?.items.count ?? 0 < 10 {
                    self.hasMoreVideos = false
                }
                
                self.data.append(contentsOf: data?.items ?? [])
                self.nextPageToken = data?.nextPageToken ?? ""
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    private func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: view.frame.width, height: 200)
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.register(VideoCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView?.frame = view.bounds
        collectionView?.backgroundColor = .systemBackground
        view.addSubview(collectionView!)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            let offsetY     = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height
            let height        = scrollView.frame.size.height
            
            if offsetY > contentHeight - height {
                if hasMoreVideos {
                    getVideos(nextPageToken: nextPageToken)
                }
            }
    }
}

extension HomeVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
        //10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! VideoCell
        cell.set(data: data[indexPath.item])
        return cell
    }
}

extension HomeVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailVideoVC()
        vc.videoInfo = data[indexPath.item]
        //vc.modalPresentationStyle = .
        navigationController?.pushViewController(vc, animated: true)
//        present(vc, animated: true, completion: nil)
    }
}

extension HomeVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = (view.frame.width - 16 - 16) * 9 / 16
        return CGSize(width: view.frame.width, height: height + 16 + 40)
    }
}
