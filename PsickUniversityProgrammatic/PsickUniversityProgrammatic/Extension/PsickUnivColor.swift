//
//  PsickUnivColor.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/20.
//

import UIKit

extension UIColor {
    static let psickUnivBlue = UIColor(red: 33/255, green: 52/255, blue: 105/255, alpha: 1)
}
