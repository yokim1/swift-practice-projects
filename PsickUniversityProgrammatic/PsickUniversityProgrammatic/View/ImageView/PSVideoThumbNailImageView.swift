//
//  PSImageView.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/20.
//

import UIKit

class PSVideoThumbNailImageView: UIImageView {

    static let cache         = NSCache<NSString, UIImage>()
    
    let placeholderImage     = UIImage(systemName: "folder")!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        image           = placeholderImage
        contentMode     = .scaleAspectFill
        
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    func downloadImage(from urlString: String) {
        let cacheKey = NSString(string: urlString)
        
        if let image = PSVideoThumbNailImageView.cache.object(forKey: cacheKey){
            self.image = image
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) {[weak self] data, response, error in
            if let _ = error { return }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { return }
            
            guard let data = data else { return }
            
            guard let image = UIImage(data: data) else { return }
            PSVideoThumbNailImageView.cache.setObject(image, forKey: cacheKey)
            DispatchQueue.main.async {
                self?.image = image
            }
        }.resume()
    }

}
