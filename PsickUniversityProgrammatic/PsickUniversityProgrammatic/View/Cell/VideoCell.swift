//
//  VideoCell.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/20.
//

import UIKit

class VideoCell: UICollectionViewCell {
    static let identifier = "VideoCell"
    
    let thumbnail = PSVideoThumbNailImageView(frame: .zero)
    let title = PSTitleLabel(textAlignment: .left, fontSize: 16)
    
    let separator: UIView = {
       let view = UIView()
        view.backgroundColor = .secondaryLabel
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(data: Item) {
        if let id = data.id?.videoId {
            thumbnail.downloadImage(from: "http://img.youtube.com/vi/\(id)/maxresdefault.jpg")
        }
        title.text = data.snippet.title
    }
    
    func configure() {
        addSubview(thumbnail)
        addSubview(title)
        addSubview(separator)
        
        let padding: CGFloat = 16
        
        NSLayoutConstraint.activate([
            thumbnail.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            thumbnail.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            thumbnail.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            thumbnail.heightAnchor.constraint(equalToConstant: 180),

            title.topAnchor.constraint(equalTo: thumbnail.bottomAnchor, constant: 12),
            title.leadingAnchor.constraint(equalTo: thumbnail.leadingAnchor),
            title.trailingAnchor.constraint(equalTo: thumbnail.trailingAnchor),
            title.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            
            separator.topAnchor.constraint(equalTo: title.bottomAnchor, constant: padding),
            separator.leadingAnchor.constraint(equalTo: leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor),
            separator.heightAnchor.constraint(equalToConstant: 1),
        ])
    }
}
