//
//  SceneDelegate.swift
//  PsickUniversityProgrammatic
//
//  Created by 김윤석 on 2021/06/20.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        
        window?.rootViewController = tabBarControllerCreator()
        window?.makeKeyAndVisible()
        
        configureNavigationBar()
    }
    
    func navigationControllerCreator(viewController: UIViewController, title: String, tabbarImageString: String?, selectedTabbarImageString: String?) -> UINavigationController {
        viewController.title = title
        
        guard let tabbarImageString = tabbarImageString,
              let selectedTabbarImageString = selectedTabbarImageString else {
            return UINavigationController()
        }
        
        viewController.tabBarItem = UITabBarItem(title: title, image: UIImage(systemName: tabbarImageString), selectedImage: UIImage(systemName: selectedTabbarImageString))
        return UINavigationController(rootViewController: viewController)
    }
    
    func tabBarControllerCreator() -> UITabBarController {
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [
            navigationControllerCreator(viewController: HomeVC(), title: "피식 대학", tabbarImageString: "house", selectedTabbarImageString: "house.fill"),
            navigationControllerCreator(viewController: CommentVC(), title: "학생들의 목소리", tabbarImageString: "bubble.right", selectedTabbarImageString: "bubble.right.fill"),
            navigationControllerCreator(viewController: SettingVC(), title: "설정", tabbarImageString: "gear", selectedTabbarImageString: "gear.fill")
        ]
        
        UITabBar.appearance().tintColor = .psickUnivBlue
        
        return tabBarController
    }
    
    func configureNavigationBar() {
        UINavigationBar.appearance().tintColor = .psickUnivBlue
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

