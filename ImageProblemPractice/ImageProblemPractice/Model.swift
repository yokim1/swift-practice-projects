//
//  Model.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import Foundation

struct ServiceResult: Decodable {
    let kind: String
    let nextPageToken: String
    let regionCode: String
    let pageInfo: PageInfo
    let items: [Item]
}

struct PageInfo: Decodable {
    var totalResults = 0
    var resultsPerPage = 0
}

struct Item: Decodable {
    var kind = ""
    var etag = ""
    var id: Id
    var snippet: Snippet
}

struct Id: Decodable {
    var kind = ""
    var videoId: String?
}

struct Snippet: Decodable {
    var channelId = ""
    var title = ""
    var description = ""
    var channelTitle = ""
    var thumbnails: Thumbnail
}

struct Thumbnail: Decodable {
    var defaultOne: ChannelURL
    var medium: ChannelURL
    var high: ChannelURL
    
    enum CodingKeys: String, CodingKey{
        case defaultOne = "default"
        case medium
        case high
    }
}

struct ChannelURL: Decodable {
    var url = ""
}


//
//{
//  "kind": "youtube#searchListResponse",
//  "etag": "qBGJvHi2ZCm6u4cIPhVnLnv9zoc",
//  "nextPageToken": "CBQQAA",
//  "regionCode": "KR",
//  "pageInfo": {
//    "totalResults": 372,
//    "resultsPerPage": 20
//  },
//  "items": [
//    {
//      "kind": "youtube#searchResult",
//      "etag": "hR2krmOoxg4ySoyevLiqkZieYLg",
//      "id": {
//        "kind": "youtube#video",
//        "videoId": "Jk3cYqCYpQE"
//      },
//      "snippet": {
//        "publishedAt": "2021-04-29T13:30:29Z",
//        "channelId": "UCGX5sP4ehBkihHwt5bs5wvg",
//        "title": "[김갑생TV] 김과 사랑에 빠진 사나이가 이호창 본부장? (형이 왜 거기서 나와? 😲) | Ep. 1",
//        "description": "역대급 게스트 두.둥.등.장! (단서는 노래, 어깨, 잘생김!) 김갑생TV에서 펼쳐진 매력만점 토크쇼! 지금 김갑생TV에서 만나보세요! #김갑생TV #리뉴얼출시 #커밍순.",
//        "thumbnails": {
//          "default": {
//            "url": "https://i.ytimg.com/vi/Jk3cYqCYpQE/default.jpg",
//            "width": 120,
//            "height": 90
//          },
//          "medium": {
//            "url": "https://i.ytimg.com/vi/Jk3cYqCYpQE/mqdefault.jpg",
//            "width": 320,
//            "height": 180
//          },
//          "high": {
//            "url": "https://i.ytimg.com/vi/Jk3cYqCYpQE/hqdefault.jpg",
//            "width": 480,
//            "height": 360
//          }
//        },
//        "channelTitle": "피식대학Psick Univ",
//        "liveBroadcastContent": "none",
//        "publishTime": "2021-04-29T13:30:29Z"
//      }
//    },
