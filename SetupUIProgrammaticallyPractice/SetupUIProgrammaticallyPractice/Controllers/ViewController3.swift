//
//  ViewController3.swift
//  SetupUIProgrammaticallyPractice
//
//  Created by 김윤석 on 2021/06/06.
//

import UIKit

class ListOfCell: UICollectionViewCell {
    let thumbnail: UIImageView = {
       let image = UIImageView()
        image.backgroundColor = .red
        image.heightAnchor.constraint(equalToConstant: 300).isActive = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let videoTitle: UILabel = {
        let label = UILabel()
        label.text = "title"
        label.numberOfLines = 2
        label.widthAnchor.constraint(equalToConstant: 50).isActive = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        backgroundColor = .brown
        
        let stackView = UIStackView(arrangedSubviews: [
            thumbnail, videoTitle
        ])
        stackView.axis = .vertical
        stackView.spacing = 12
        addSubview(stackView)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ViewController3: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    init(){
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 350)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        view.backgroundColor = .red
        collectionView.register(ListOfCell.self, forCellWithReuseIdentifier: "cellId")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
    }
    
    func setupUI(){
        view.backgroundColor = .green
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "VC 3"
        navigationController?.navigationBar.barTintColor = .magenta
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        10
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath)
        
        //cell.backgroundColor = .blue
        
        return cell
    }
}
