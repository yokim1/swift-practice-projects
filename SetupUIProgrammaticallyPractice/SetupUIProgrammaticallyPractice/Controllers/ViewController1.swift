//
//  ViewController.swift
//  SetupUIProgrammaticallyPractice
//
//  Created by 김윤석 on 2021/06/06.
//

import UIKit

class ViewController1: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.right"), style: .plain, target: self, action: #selector(buttonHandler))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    @objc func buttonHandler(){
        let vc = ViewController2()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupUI(){
        view.backgroundColor = .orange
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "VC 1"
        navigationController?.navigationBar.barTintColor = .gray
    }
}
