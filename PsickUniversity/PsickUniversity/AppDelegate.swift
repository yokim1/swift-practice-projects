//
//  AppDelegate.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/23.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    let bDateUrlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date&maxResults=5"
    
    
    
    let hanSarangUrlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsMkhTupzGC1QcHb4eJZQ-PY&part=snippet,id&orderby=rating&maxResults=5"
    
    let oldIsBackUrlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsOXHxUhVuXCKpeedQJV6Yid&part=snippet,id&orderby=rating&maxResults=5"
    
    let ronieAndSteveUrlString =
        "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsN22vkofUUoR_KIezgch7_K&part=snippet,id&orderby=rating&maxResults=5"

    
    let yourSongMySongUrlString =
        "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PLRmKGEHn3NPqIlSAEkS1h0PDOuoc5VoP8&part=snippet,id&orderby=rating&maxResults=5"
   
    //https://youtube.com/playlist?list=PLRmKGEHn3NPpSjXDHZuXC3YOED51ruU_P
    
    let allChannelItemsUrlString = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=5"
    
    
//임플란티드 키드    PLRmKGEHn3NPo99JmOpapWrPWJRTs-CV2i
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        Service.shared.fetchSpecificPlayList(searchTerm: bDateUrlString) {items, error in
//            Manager.shared.bDateItems = items
//        }
//
//        Service.shared.fetchSpecificPlayList(searchTerm: hanSarangUrlString) { items, error in
//            Manager.shared.hanSarangItems = items
//        }
//
//        Service.shared.fetchSpecificPlayList(searchTerm: oldIsBackUrlString) { items, error in
//            Manager.shared.oldisBackItems = items
//        }
//
//        Service.shared.fetchSpecificPlayList(searchTerm: ronieAndSteveUrlString) { items, error in
//            Manager.shared.ronieAndSteveItems = items
//        }
//
//        Service.shared.fetchSpecificPlayList(searchTerm: yourSongMySongUrlString) { items, error in
//            Manager.shared.yourSongMySongItems = items
//        }
//
//        Service.shared.fetchAllPlayList(searchTerm: allChannelItemsUrlString, completion: { items, error in
//            Manager.shared.allChannelItems = items
//        })
//
//        (searchTerm: allChannelItemsUrlString) { items, error in
//            Manager.shared.allChannelItems = items
//            print("Items: \(Manager.shared.allChannelItems.count)")
//        }
//
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "PsickUniversity")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

