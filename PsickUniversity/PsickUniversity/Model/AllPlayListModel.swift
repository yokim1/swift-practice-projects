//
//  AllPlayListModel.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/02.
//

import Foundation

struct AllPlayListService: Decodable{
    let kind: String
    let etag: String
    let nextPageToken: String
    let region: String?
    let pageInfo: PageInfo?
    let items: [Item2]
}

struct Item2: Decodable {
    let kind: String
    let etag: String
    let id: Id
    let snippet: Snippet
}

struct Id: Decodable{
    let kind: String
    let videoId: String
}

