//
//  Service.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//
import UIKit


// psick univ channel id: UCGX5sP4ehBkihHwt5bs5wvg
//key : AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28

//https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=20

//https://www.youtube.com/watch?v=xXWyJEo6VgA&list={playlist Id}

//https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date

//https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date

//https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date

// 비대면 id: PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp
//https://www.youtube.com/watch?v=xXWyJEo6VgA&list=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp
//list=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp
//https://youtube.com/playlist?list=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp


//https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date&maxResults=20

//https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date&maxResults=20

//김윤석의 재생 목록https://youtube.com/playlist?list=PLRmKGEHn3NPonxwDHafpif6mIHP4Adp1d



//MARK: PAGINATION example
//first Page: https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=50

//next Page: https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=50&pageToken=CDIQAA

//video with statistics
//https://www.googleapis.com/youtube/v3/videos?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&part=statistics&id=gYCd2UzR2pY&

//several videos with statistics
//https://www.googleapis.com/youtube/v3/videos?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&part=statistics&id=gYCd2UzR2pY%2CxbXPQhkTAXY%2CX--npnXa8GU%2CJk3cYqCYpQE

// , = %2C

//https://www.googleapis.com/youtube/v3/videos?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,part=statistics

//https://www.googleapis.com/youtube/v3/videos?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&myRating=like&part=snippet&maxResults=50



class Service {
    static let shared = Service()
    
    var isPaginating = false
    
    func fetchSpecificPlayList(paginating: Bool = false, searchTerm: String, completion: @escaping ([Item], String?, Error?)->()){
        
        //let urlString = "https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvgpart=snippet,id&order=date"
       
        if paginating {
            isPaginating = true
        }
        
        guard let url = URL(string: searchTerm) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                completion([], nil, nil)
                return
            }
            
            //success
            //            print(data)
            //            print(String(data: data!, encoding: .utf8))
            //print(url)
            guard let data = data else {return}
            do {
                let searchResult = try JSONDecoder().decode(SpecificPlayListService.self, from: data)
                //print(searchResult.results.forEach({ print($0.trackName, $0.primaryGenreName)}))
//                print(searchResult.items.forEach({
//                    print($0.id)
//                    print($0.snippet.thumbnails.medium)
//                }))
//                print("resultsPerPage: \(searchResult.pageInfo?.resultsPerPage)")
//                print("TotalResults: \(searchResult.pageInfo?.totalResults)")
                //self.appResult = searchResult.results
                //                DispatchQueue.main.async {
                //                    self.collectionView.reloadData()
                //                }
                //completion(searchResult.results, nil)
                
                completion(searchResult.items, searchResult.nextPageToken, nil)
                
                if paginating {
                    self.isPaginating = false
                }
                
            } catch let Jsonerror {
                completion([], nil, Jsonerror)
                print(Jsonerror.localizedDescription)
                print(Jsonerror)
                print("Failed To Decode")
            }
            
        }.resume()
    }
    
    func fetchAllPlayList(searchTerm: String, completion: @escaping ([Item2], Error?)->()){
        
        guard let url = URL(string: searchTerm) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                completion([], nil)
                return
            }
            
            //success
            //            print(data)
            //            print(String(data: data!, encoding: .utf8))
            guard let data = data else {return}
            do {
                let searchResult = try JSONDecoder().decode(AllPlayListService.self, from: data)

                completion(searchResult.items, nil)
                
            } catch let Jsonerror {
                completion([], Jsonerror)
                print(Jsonerror.localizedDescription)
                print(Jsonerror)
                print("Failed To Decode")
            }
            
        }.resume()
    }
}
