//
//  YTViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/27.
//

import UIKit
import WebKit

class YTViewController: UIViewController {

    @IBOutlet weak var webViewPlayer: WKWebView!
    
    var url: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadYoutube(videoID: url)
        
    }
    
    func loadYoutube(videoID:String) {
        guard let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoID)") else { return }
        
        webViewPlayer.load( URLRequest(url: youtubeURL))
        webViewPlayer.allowsLinkPreview = true
        
    }
    
    
    func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
}
