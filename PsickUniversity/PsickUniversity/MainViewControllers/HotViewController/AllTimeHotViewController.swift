//
//  AllTimeHotViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/02.
//

import UIKit

class AllTimeHotViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
    }
}

extension AllTimeHotViewController: UICollectionViewDelegate {
    
    
}

extension AllTimeHotViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        100
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllTimeHotThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}
        return cell
    }
    
    
}
