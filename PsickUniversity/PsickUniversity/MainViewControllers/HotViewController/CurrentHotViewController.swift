//
//  CurrentHotViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/02.
//

import UIKit

class CurrentHotViewController: UIViewController {
    
    let allChannelItemsUrlString = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=5"
    
    
    var model: [Item2] = []
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
//        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
//            for index in 0..<Manager.shared.allChannelItems.count {
//                if Manager.shared.allChannelItems[index].snippet.publishTime == "" {
//
//                }
//            }
//
//            self.collectionView.reloadData()
//        }
        
        Service.shared.fetchAllPlayList(searchTerm: allChannelItemsUrlString, completion: { [weak self] items, error in
            DispatchQueue.main.async {
                Manager.shared.allChannelItems = items
                self?.collectionView.reloadData()
            }
        })
    }
}

extension CurrentHotViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Manager.shared.allChannelItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CurrentHotThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}
        
        let items = Manager.shared.allChannelItems[indexPath.item]
        //let url = URL(string: items.snippet.thumbnails.high.url)
        let id = items.id.videoId
        let url = URL(string: "https://i.ytimg.com/vi/\(id)/maxresdefault.jpg")
        
        cell.thumbnail.sd_setImage(with: url)
        
        return cell
    }
}

extension CurrentHotViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(identifier: "YoutubePlayerViewController") as? YoutubePlayerViewController else {return }
//        vc.modalPresentationStyle = .popover
//        vc.modalPresentationStyle = .fullScreen
//        vc.youtubeVideoId = Manager.shared.allChannelItems[indexPath.item].id.videoId
//        present(vc, animated: true, completion: nil)
        
        let videoLauncher = VideoLauncher()
        videoLauncher.videoId = Manager.shared.allChannelItems[indexPath.item].id.videoId
        print(Manager.shared.allChannelItems[indexPath.item])
        videoLauncher.showVideoPlayer()
        
    }
}
