//
//  AboutViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/02.
//

import UIKit

class AboutViewController: UIViewController {

    //MARK: Find what the hell is wrong
    
    let text: UILabel = {
        let label = UILabel()
        label.text = "Youtube라는 거대한 복잡계를 통해 자신들의 진정한 가치를 대중에게 인정받아 매스미디어에서 마주한 위기를 전화위복으로 극복해낸 이들에게 보내는 선물이자 찬사"
        label.numberOfLines = 5
        label.textAlignment = .center
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(text)
        
        text.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        text.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
}
