//
//  BDateViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/23.
//

import UIKit
import SDWebImage

class BDateViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var bDateCollectionView: UICollectionView!
    
    var nextPageToken: String?
    
    let urlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date&maxResults=45"
    
    let bDateUrlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp&part=snippet,id&order=date"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bDateCollectionView.delegate = self
        bDateCollectionView.dataSource = self
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            self.bDateCollectionView.reloadData()
//        }
        
        Service.shared.fetchSpecificPlayList(searchTerm: bDateUrlString) {[weak self] items, nextPageToken, error  in
            DispatchQueue.main.async {
                Manager.shared.bDateItems = items
                self?.nextPageToken = nextPageToken
                self?.bDateCollectionView.reloadData()
            }
        }
    }
    
    var count = 0
    
}

extension BDateViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        print(Manager.shared.bDateItems.count)
//        print(Manager.shared.bDateItems.first?.id)
        return Manager.shared.bDateItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BDateThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}
        
        let item = Manager.shared.bDateItems[indexPath.item]
        let url = URL(string: item.snippet.thumbnails.maxres?.url ?? item.snippet.thumbnails.medium.url )
        
        cell.thumbnail.sd_setImage(with: url)
        cell.videoTitle.text = item.snippet.title
        return cell
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //print("Will End")
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("Did End")
        

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.x
        
        if position > bDateCollectionView.contentSize.width - scrollView.frame.size.width + 70 {
            guard let nextPageToken = nextPageToken,
                !Service.shared.isPaginating else { return }
            //print( bDateUrlString + "&pageToken=\(nextPageToken)")
            
           // guard !Service.shared.isPaginating else {return }
            
            Service.shared.fetchSpecificPlayList(paginating: true, searchTerm: bDateUrlString + "&pageToken=\(nextPageToken)") { items, nextPageToken, error in
                self.nextPageToken = nextPageToken
                print(items)
                let endIndex = Manager.shared.bDateItems.endIndex
                Manager.shared.bDateItems.append(contentsOf: items)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.bDateCollectionView.reloadData()
                    //self.bDateCollectionView.scrollToItem(at: .init(index: 0), at: .centeredHorizontally, animated: true)
                    print("count: \(self.count)")
                    self.count += 1
                }
            }
        }
    }
}

extension BDateViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(identifier: "YoutubePlayerViewController") as? YoutubePlayerViewController else {return }
        vc.modalPresentationStyle = .popover
        vc.modalPresentationStyle = .fullScreen
        vc.youtubeVideoId = Manager.shared.bDateItems[indexPath.item].snippet.resourceId?.videoId
        present(vc, animated: true, completion: nil)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(identifier: "YTViewController") as? YTViewController else {return }
//        vc.modalPresentationStyle = .popover
//        vc.modalPresentationStyle = .fullScreen
//        vc.url = Manager.shared.bDateItems[indexPath.item].snippet.resourceId?.videoId ?? ""
//        present(vc, animated: true, completion: nil)
//    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return .init(width: 320, height: 180)
    }
}

