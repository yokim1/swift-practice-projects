//
//  05isBackViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import UIKit

class OldisBackViewController: UIViewController {
    
    @IBOutlet weak var oldIsBackCollectionView: UICollectionView!
    
    let oldIsBackUrlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsOXHxUhVuXCKpeedQJV6Yid&part=snippet,id&orderby=rating&maxResults=5"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oldIsBackCollectionView.delegate = self
        oldIsBackCollectionView.dataSource = self
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //            self.oldIsBackCollectionView.reloadData()
        //        }
        
        Service.shared.fetchSpecificPlayList(searchTerm: oldIsBackUrlString) { [weak self] items, nextPageToken, error  in
            
            DispatchQueue.main.async {
                Manager.shared.oldisBackItems = items
                
                self?.oldIsBackCollectionView.reloadData()
            }
        }
    }
}

extension OldisBackViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Manager.shared.oldisBackItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OldIsBackThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}
        
        let item = Manager.shared.oldisBackItems[indexPath.item]
        let url = URL(string: item.snippet.thumbnails.maxres?.url ?? item.snippet.thumbnails.high.url )
        
        cell.thumbnail.sd_setImage(with: url, completed: nil)
        return cell
    }
}

extension OldisBackViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(identifier: "YoutubePlayerViewController") as? YoutubePlayerViewController else {return }
        vc.modalPresentationStyle = .popover
        vc.modalPresentationStyle = .fullScreen
        //vc.youtubeVideoId = Manager.shared.bDateItems[indexPath.item].id.videoId
        vc.youtubeVideoId = Manager.shared.oldisBackItems[indexPath.item].snippet.resourceId?.videoId
        present(vc, animated: true, completion: nil)
    }
}
