//
//  PostViewController.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/12.
//

import UIKit

protocol PostViewControllerDelegate {
    func completionButtonDidTapToAdd(post: PostModel)
    func completionButtonDidTapToEdit(toChanageFrom oldPost: PostModel, to newPost: PostModel)
    func cancelButtonDidTap()
}

class PostViewController: UIViewController {
    
    //MARK: - UI Objects
    private lazy var categorySelectionButton: UIButton = {
        let button = UIButton(type: .system)
        button.addTarget(self, action: #selector(categorySelectionButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    private let separatorView: UIView = {
        let uv = UIView()
        uv.backgroundColor = .gray
        uv.alpha = 0.2
        return uv
    }()
    
    private let bodyTextView: UITextView = {
        let tv = UITextView()
        tv.font = .systemFont(ofSize: 24, weight: .regular)
        tv.autocorrectionType = .no
        return tv
    }()
    
    //MARK: - Property
    private let actionType: ActionTypeForPost
    private var post: PostModel? {
        didSet {
            //categorySelectionButton.setTitle(post?.category, for: .normal)
            //categorySelectionButton.setTitleColor(.label, for: .normal)
            category = post?.category
            bodyTextView.text = post?.bodyContent
        }
    }
    
    private var category: String? {
        didSet {
            categorySelectionButton.setTitle(category, for: .normal)
            categorySelectionButton.setTitleColor(.label, for: .normal)
        }
    }
    
    var delegate: PostViewControllerDelegate?
    var userInfo: UserInfo?
    
    //MARK: - Init
    init(to actionType: ActionTypeForPost, for post: PostModel? = nil) {
        self.actionType = actionType
        self.post = post
        super.init(nibName: nil, bundle: nil)
        category = post?.category
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        navigationItem.title = "동네 생활"
        navigationController?.navigationBar.tintColor = .label
        
        configureContext()
        configureNavigationLeftBarItems()
        configureNavigationrightBarItems()
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Selector
extension PostViewController {
    
    @objc private func categorySelectionButtonDidTap() {
        let vc = CategorySelectionTableViewController()
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func completeButtonDidTap() {
        
        dismiss(animated: true) {
            guard let category = self.category else { return }

            switch self.actionType {
            case .edit: break
//                guard let oldPost = self.post else { return }
//                self.delegate?.completionButtonDidTapToEdit(toChanageFrom: oldPost,
//                                                    to: .init(
//                                                            category: category,
//                                                           bodyContent: self.bodyTextView.text,
//                                                            userInfo: oldPost.userInfo,
//                                                           date: Date(),
//                                                           isEdited: true,
//                                                           isLiked: oldPost.isLiked,
//                                                           numberOfLikes: oldPost.numberOfLikes,
//                                                           numberOfComments: oldPost.numberOfComments))
                
            case .add: break
//                self.delegate?.completionButtonDidTapToAdd(post: .init(
//                                                                category: category,
//                                                                bodyContent: self.bodyTextView.text,
//                                                                userInfo: self.userInfo ?? UserInfo(userId: "random", address: "none"),
//                                                                date: Date(),
//                                                                isEdited: false,
//                                                                isLiked: false,
//                                                                numberOfLikes: 0,
//                                                                numberOfComments: 0))
            case .remove:
                break
            }
        }
    }
    
    @objc private func closeButtonDidTap() {
        delegate?.cancelButtonDidTap()
    }
}

//MARK: - CategorySelectionTableViewController Delegate
extension PostViewController: CategorySelectionTableViewControllerDelegate {
    func categoryDidTap(categoryString: String) {
        category = categoryString
        navigationController?.popViewController(animated: true)
    }
}

//MARK: - Configure UI
extension PostViewController {
    private func configureContext() {
        categorySelectionButton.setTitle(category ?? "게시글의 주제를 선택해주세요.", for: .normal)
        let titleColor: UIColor = actionType == .add ? .lightGray : .label
        categorySelectionButton.setTitleColor(titleColor, for: .normal)
        bodyTextView.text = post?.bodyContent
    }
    
    private func configureNavigationLeftBarItems() {
        let closeButton = UIBarButtonItem(title: "취소", style: .done, target: self, action: #selector(closeButtonDidTap))
        navigationItem.leftBarButtonItems = [closeButton]
    }
    
    private func configureNavigationrightBarItems() {
        let completeButton = UIBarButtonItem(title: "완료", style: .done, target: self, action: #selector(completeButtonDidTap))
        navigationItem.rightBarButtonItems = [completeButton]
    }
    
    private func configureUI() {
        let items = [categorySelectionButton, separatorView, bodyTextView]
        items.forEach { item in
            view.addSubview(item)
            item.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            categorySelectionButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            categorySelectionButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            categorySelectionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            categorySelectionButton.heightAnchor.constraint(equalToConstant: 30),
            
            separatorView.topAnchor.constraint(equalTo: categorySelectionButton.bottomAnchor, constant: 10),
            separatorView.leadingAnchor.constraint(equalTo: categorySelectionButton.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: categorySelectionButton.trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            
            bodyTextView.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 10),
            bodyTextView.leadingAnchor.constraint(equalTo: categorySelectionButton.leadingAnchor),
            bodyTextView.trailingAnchor.constraint(equalTo: categorySelectionButton.trailingAnchor),
            bodyTextView.heightAnchor.constraint(equalToConstant: view.frame.height),
        ])
    }
}
