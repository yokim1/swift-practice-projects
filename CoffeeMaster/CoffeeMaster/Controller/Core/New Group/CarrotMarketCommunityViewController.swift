//
//  CarrotMarketCommunityViewController.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/12.
//

import MapKit
import FirebaseDatabase
import CoreLocation
import UIKit

class CarrotMarketCommunityViewController: UITableViewController {
    
    //MARK: - Model
    private var allPosts = [PostModel]()
    private var filteredPosts = [PostModel]()
    private var userInfo: UserInfo? /// This is initialize when device gets its location
    
    private var navTitleView = "Loading..." {
        didSet {
            setUpNavigationLabel(subLocality: navTitleView)
        }
    }
    
    //MARK: - Persistance Manager
    //private let persistanceManager = PersistanceManager()
    
    //MARK: - Location Manager
    private let manager = CLLocationManager()
    
    //MARK: - UI Object
    private let addNewPostButton = CarrotMarketImageCircularButton(image: UIImage(systemName: "pencil"), color: .systemOrange, width: 60)
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
//        fetchRememberMe()
        
        configureLogInVC()
        
        fetchPosts()
        setUpNavigationBarRightItems()
        configureCategorySelectionHeaderView()
        setUpTableView()
        setUpAddNewPostButton()
        configureLocationManager()
    }
    
    private func fetchRememberMe() {
//        persistanceManager.retrieveRememberMe { result in
//            switch result {
//            case .success(let rememberMe):
//                if !rememberMe.rememberMe {
//                    self.configureLogInVC()
//                }
//            case .failure(let error):
//                print(error)
//
//            case .none: break
//
//            }
//        }
    }
    
    private func configureLogInVC() {
        let vc = CarrotMarketLogInViewController()
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
}

//MARK: - CarrotMarketSignInViewController Delegate
extension CarrotMarketCommunityViewController: CarrotMarketSignInViewControllerDelegate {
    func loggedInSuccessfully(userId: String) {
        
        dismiss(animated: true) {
            self.userInfo = UserInfo(userId: userId, address: self.navTitleView)
        }
    }
}

//MARK: - CarrotMarketSignUpViewController Delegate
extension CarrotMarketCommunityViewController: CarrotMarketSignUpViewControllerDelegate {
    func createAccountSuccessfully() {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - TableView DataSource and Delegate
extension CarrotMarketCommunityViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filteredPosts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CarrotMarketCommunityTableViewCell.identifier, for: indexPath) as? CarrotMarketCommunityTableViewCell else {return UITableViewCell()}
        cell.configure(with: filteredPosts[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //action sheet title 지정
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let editAction = actionForSelectedPost(to: .edit, at: indexPath, style: .default)
        let removeAction = actionForSelectedPost(to: .remove, at: indexPath, style: .destructive)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        //action sheet에 옵션 추가.
        optionMenu.addAction(editAction)
        optionMenu.addAction(removeAction)
        optionMenu.addAction(cancelAction)
        
        //show
        self.present(optionMenu, animated: true, completion: nil)
    }
}

//MARK: - CLLocationManager Delegate
/// get user's current location and configure it with Navigation Button title
extension CarrotMarketCommunityViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        if let location = locations.first {
            let location = CLLocation(latitude: location.coordinate.latitude,
                                      longitude: location.coordinate.longitude)
            CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                placemarks?.forEach({ placemark in
                    
                    self.navTitleView = placemark.subLocality ?? ""
                    //self.userInfo = UserInfo(userId: "Kim", address: placemark.subLocality ?? "")
                    
                    if placemark.subLocality == nil {
                        self.navTitleView = placemark.locality ?? ""
                        //self.userInfo = UserInfo(userId: "Kim", address: placemark.locality ?? "")
                    }
                })
            }
        }
    }
}

//MARK: - Action Sheet
extension CarrotMarketCommunityViewController {
    
    func actionForSelectedPost(to action: ActionTypeForPost, at indexPath: IndexPath, style: UIAlertAction.Style) -> UIAlertAction {
        
        let action = UIAlertAction(title: action.rawValue, style: style) { _ in
            
            switch action {
            case .edit:
                self.presentPostViewController(to: .edit, for: self.filteredPosts[indexPath.row])
                
            case .remove:
//                self.persistanceManager.update(to: .remove, for: self.filteredPosts[indexPath.row]) { error in }
                self.updatePostOnTableView(to: .remove, for: self.filteredPosts[indexPath.row])
                
            case .add:
                break
            }
        }
        return action
    }
}

//MARK: - PostViewController
/// present PostViewController according to action (Edit / Add)
extension CarrotMarketCommunityViewController {
    private func presentPostViewController(to actionType: ActionTypeForPost, for post: PostModel? = nil) {
        let vc = PostViewController(to: actionType, for: post)
        vc.delegate = self
        vc.userInfo = userInfo
        
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
}

//MARK: - Selector
extension CarrotMarketCommunityViewController {
    @objc private func buttonDidTap() {
        print("navigation bar items are tapped")
    }
    
    @objc private func addNewPostButtonDidTap() {
        presentPostViewController(to: .add)
    }
    
    @objc private func townSelectionButtonDidTap() {
        print("Select your town")
    }
}

//MARK: - CategorySelectionHeaderView Delegate
extension CarrotMarketCommunityViewController: CategorySelectionHeaderViewDelegate {
    
    func categoryDidTap(category: String) {
        filteredPosts = allPosts.filter({ post in
            post.category == category
        })
        
        if category == "전체" {
            filteredPosts = allPosts
        }
        
        tableView.reloadData()
    }
}

//MARK: - PostVC Delegate
extension CarrotMarketCommunityViewController: PostViewControllerDelegate {
    
    func completionButtonDidTapToAdd(post: PostModel) {
        
        updatePostOnTableView(to: .add, for: post)
//        persistanceManager.update(to: .add, for: post) { error in
//            //print(error)
//        }
        CarrotMarketService.shared.uploadPost(post: post) { error, databaseReference in
            
        }
    }
    
    func completionButtonDidTapToEdit(toChanageFrom oldPost: PostModel, to newPost: PostModel) {
        updatePostOnTableView(to: .edit, for: oldPost, by: newPost)
//        persistanceManager.update(to: .edit, from: oldPost, for: newPost) { error in }
        
    }
    
    func cancelButtonDidTap() {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - Client Level Add / Edit / Remove
extension CarrotMarketCommunityViewController {
    
    private func updatePostOnTableView(to actionType: ActionTypeForPost, for post: PostModel, by newPost: PostModel? = nil) {
        switch actionType {
        case .add:
            addPostOnTableView(with: post)
            
        case .edit:
            guard let newPost = newPost else { return }
            editPostOnTableView(from: post, to: newPost)
            
        case .remove:
            removePostOnTableView(for: post)
        }
    }
    
    private func addPostOnTableView(with post: PostModel) {
        allPosts.reverse()
        allPosts.append(post)
        allPosts.reverse()
        
        filteredPosts.reverse()
        filteredPosts.append(post)
        filteredPosts.reverse()
        
        let newIndexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .middle)
    }
    
    private func editPostOnTableView(from oldPost: PostModel, to newPost: PostModel) {
        
        let row = allPosts.firstIndex(of: oldPost)
        let reloadIndexPath = IndexPath(row: row!, section: 0)
        allPosts[reloadIndexPath.row] = newPost
        
        let filteredRow = filteredPosts.firstIndex(of: oldPost)
        let filteredPostsIndexPath = IndexPath(row: filteredRow!, section: 0)
        filteredPosts[filteredPostsIndexPath.row] = newPost
        
        tableView.reloadRows(at: [filteredPostsIndexPath], with: .middle)
    }
    
    private func removePostOnTableView(for post: PostModel) {
        let row = allPosts.firstIndex(of: post)
        let allPostsIndexPath = IndexPath(row: row!, section: 0)
        allPosts.remove(at: allPostsIndexPath.row)
        
        let filteredRow = filteredPosts.firstIndex(of: post)
        let filteredPostsIndexPath = IndexPath(row: filteredRow!, section: 0)
        filteredPosts.remove(at: filteredPostsIndexPath.row)
        
        tableView.deleteRows(at: [filteredPostsIndexPath], with: .automatic)
    }
}

//MARK: - CarrotMarketCommunityTableViewCell Delegate
extension CarrotMarketCommunityViewController: CarrotMarketCommunityTableViewCellDelegate {
    func likeButtonDidTap() {
        print("Like button did tap")
        
    }
    
    func commentButtonDidTap() {
        print("Comment button did tap")
    }
}

//MARK: - Fetch Posts (UserDefaults)
extension CarrotMarketCommunityViewController {
    
    private func fetchPosts() {
        
//        persistanceManager.retrieveData { [weak self] result in
//            guard let self = self else { return }
//            switch result {
//            case .success(let posts):
//                self.allPosts = posts
//                self.allPosts.reverse()
//                self.filteredPosts = self.allPosts
//
//            case .failure(let error):
//                print(error)
//            }
//        }
//        Database.database().reference().child(DataKey.feed).observeSingleEvent(of: .value) { snapshot in
//            guard let dictionary = snapshot.value as? [String: AnyObject],
//                  let feeds = dictionary["feed"] as? [PostModel] else { return }
//            self.allPosts = feeds
//        }
        CarrotMarketService.shared.fetchPost { posts in
            
            DispatchQueue.main.async {
                self.allPosts = posts
                self.tableView.reloadData()
            }
        }
    }
}

//MARK: - Setup UI
extension CarrotMarketCommunityViewController {
   
    private func setUpNavigationLabel(subLocality: String? = nil) {
        
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        guard let subLocality = subLocality else { return }
        button.setTitle(subLocality, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 22, weight: .bold)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(townSelectionButtonDidTap), for: .touchUpInside)
        navigationItem.titleView = button
        
        if let navigationBar = navigationController?.navigationBar {
            button.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor, constant: 20).isActive = true
            button.widthAnchor.constraint(equalToConstant: navigationBar.frame.width/2).isActive = true
        }
        
        navigationController?.navigationBar.tintColor = .label
    }
    
    private func setUpNavigationBarRightItems() {
        
        navigationItem.rightBarButtonItems = [
            customNavigationBarButton(systemName: "bell", action: #selector(buttonDidTap)),
            customNavigationBarButton(systemName: "line.horizontal.3", action: #selector(buttonDidTap)),
            customNavigationBarButton(systemName: "magnifyingglass", action: #selector(buttonDidTap)),
        ]
    }
    
    private func customNavigationBarButton(systemName: String, action: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: systemName), for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        
        let searchButton = UIBarButtonItem(customView: button)
        
        let currWidth = searchButton.customView?.widthAnchor.constraint(equalToConstant: 35)
        currWidth?.isActive = true
        let currHeight = searchButton.customView?.heightAnchor.constraint(equalToConstant: 35)
        currHeight?.isActive = true
        
        return searchButton
    }
    
    private func setUpTableView() {
        tableView.frame = view.bounds
        tableView.separatorStyle = .none
        
        tableView.register(CarrotMarketCommunityTableViewCell.self, forCellReuseIdentifier: CarrotMarketCommunityTableViewCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = CarrotMarketCommunityTableViewCell.height
    }
    
    private func setUpAddNewPostButton() {
        view.addSubview(addNewPostButton)
        addNewPostButton.addTarget(self, action: #selector(addNewPostButtonDidTap), for: .touchUpInside)
        addNewPostButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            addNewPostButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            addNewPostButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            addNewPostButton.heightAnchor.constraint(equalToConstant: 60),
            addNewPostButton.widthAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func configureCategorySelectionHeaderView() {
        let view = CategorySelectionHeaderView()
        view.delegate = self
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 60)
        tableView.tableHeaderView = view
    }
    
    private func configureLocationManager() {
        
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
}
