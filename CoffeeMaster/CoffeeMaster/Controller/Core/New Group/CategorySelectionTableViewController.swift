//
//  CategorySelectionTableViewController.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/18.
//

import UIKit

protocol CategorySelectionTableViewControllerDelegate: AnyObject {
    func categoryDidTap(categoryString: String)
}

class CategorySelectionTableViewController: UITableViewController {

    //MARK: - Model
    
    private let basicCategories = CategoryConstants.basicCategories
    private let myInterestCategories = CategoryConstants.myInterestCategories
    private let categories = [CategoryConstants.basicCategories, CategoryConstants.myInterestCategories]
    
    //MARK: - Delegate
    
    weak var delegate: CategorySelectionTableViewControllerDelegate?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "주제 선택"
        configureTalbeView()
    }
    
    //MARK: - Configure TableView
    
    private func configureTalbeView() {
        tableView.separatorStyle = .none
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    // MARK: - TableView DataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return basicCategories.count
        }
        return myInterestCategories.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "기본 주제"
        }
        return "내 관심 주제"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if indexPath.section == 0 {
            cell.textLabel?.text = basicCategories[indexPath.row]
        } else {
            cell.textLabel?.text = myInterestCategories[indexPath.row]
        }
        
        return cell
    }
    
    //MARK: TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            delegate?.categoryDidTap(categoryString: basicCategories[indexPath.row])
            return
        } else {
            delegate?.categoryDidTap(categoryString: myInterestCategories[indexPath.row])
            return
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
