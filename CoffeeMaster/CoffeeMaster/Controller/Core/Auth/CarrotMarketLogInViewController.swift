//
//  SignIn.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/20.
//

import FirebaseAuth
import FirebaseDatabase
import Combine
import UIKit

protocol CarrotMarketSignInViewControllerDelegate: AnyObject {
    func loggedInSuccessfully(userId: String)
}

class CarrotMarketLogInViewController: UIViewController {
    
    weak var delegate: CarrotMarketSignInViewControllerDelegate?
    
    private let emailInputView = CarrotMarketCustomTextInputView(placeholder: "Email")
    private let passwordInputView = CarrotMarketCustomTextInputView(placeholder: "Password")
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Log In", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemOrange
        button.layer.cornerRadius = 8
        button.isEnabled = false
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.addTarget(self, action: #selector(loginButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    private lazy var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        let attributedTitle =
        NSMutableAttributedString(string: "새로운 계정 만들기 ",
                                  attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                                               NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        attributedTitle.append(
            NSMutableAttributedString(string: "Sign Up",
                                      attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                                                   NSAttributedString.Key.foregroundColor: UIColor.systemOrange]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        button.addTarget(self, action: #selector(signUpButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        title = "Log In"
        configureUI()
        
        loginButtonSubscriberForEnable = changeBacgroundColorToSubmit
            .receive(on: RunLoop.main)
            .assign(to: \.backgroundColor, on: loginButton)
        
        loginButtonSubscriberForBackgroundColor = enableToSubmit
            .receive(on: RunLoop.main)
            .assign(to: \.isEnabled, on: loginButton)
        
        self.navigationController?.view.tintColor = .systemOrange
    }
    
    @Published var id = ""
    @Published var password = ""
    
    private var enableToSubmit: AnyPublisher<(Bool), Never> {
        return eraseToAnyPublisher(successCase: true, failureCase: false)
    }
    
    private var changeBacgroundColorToSubmit: AnyPublisher<(UIColor?), Never> {
        return eraseToAnyPublisher(successCase: UIColor.systemOrange, failureCase: UIColor.systemGray)
    }
    
    private func eraseToAnyPublisher<T>(successCase: T, failureCase: T) -> AnyPublisher<T, Never>{
        return Publishers.CombineLatest($id, $password)
            .map { email, password in
                //
                if !email.isEmpty && !password.isEmpty {
                    return successCase
                }
                
                return failureCase
                
            }.eraseToAnyPublisher()
    }
    
    
    private var loginButtonSubscriberForEnable: AnyCancellable?
    private var loginButtonSubscriberForBackgroundColor: AnyCancellable?
}

//MARK: - Selector
extension CarrotMarketLogInViewController {
    
    @objc private func loginButtonDidTap() {
        guard let emailString = emailInputView.inputTextField.text else {return }
        guard let passwordString = passwordInputView.inputTextField.text else {return }
        
        Auth.auth().signIn(withEmail: emailString, password: passwordString) { result, error in
            if let error = error {
                print(error)
                return
            }
            
            if let uid = result?.user.uid {
                Database.database().reference().child(DataKey.userAuth).child(uid).observeSingleEvent(of: .value) { snapshot in
                    guard let dictionary = snapshot.value as? [String: AnyObject],
                          let id = dictionary["id"] as? String else {return }
                    self.delegate?.loggedInSuccessfully(userId: id)
                }
            }
        }
    }
    
    @objc private func signUpButtonDidTap() {
        let vc = CarrotMarketSignUpViewController()
        //        vc.delegate = self.delegate as? CarrotMarketSignUpViewControllerDelegate
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension CarrotMarketLogInViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let textFieldText = textField.text ?? ""
        let text = (textFieldText as NSString).replacingCharacters(in: range, with: string)
        
        if textField == emailInputView.inputTextField {
            id = text
        }
        
        if textField == passwordInputView.inputTextField {
            password = text
        }
        
        return true
    }
}

//MARK: - UI
extension CarrotMarketLogInViewController {
    private func configureUI() {
        emailInputView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        passwordInputView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        passwordInputView.inputTextField.isSecureTextEntry = true
        
        emailInputView.inputTextField.delegate = self
        passwordInputView.inputTextField.delegate = self
        
        let stackView = UIStackView(arrangedSubviews: [emailInputView, passwordInputView, loginButton])
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(stackView)
        view.addSubview(signUpButton)
        
        NSLayoutConstraint.activate([
            stackView.widthAnchor.constraint(equalToConstant: view.frame.width / 2),
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            
            signUpButton.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 20),
            signUpButton.centerXAnchor.constraint(equalTo: stackView.centerXAnchor),
        ])
    }
}
