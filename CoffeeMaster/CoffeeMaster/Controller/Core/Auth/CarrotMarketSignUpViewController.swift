//
//  CarrotMarketSignUpViewController.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/21.
//

import FirebaseAuth
import FirebaseDatabase
import Combine
import UIKit

protocol CarrotMarketSignUpViewControllerDelegate: AnyObject {
    func createAccountSuccessfully()
}

class CarrotMarketSignUpViewController: UIViewController {

    weak var delegate: CarrotMarketSignUpViewControllerDelegate?
    
    private lazy var emailInputView = CarrotMarketCustomTextInputView(placeholder: "Email")
    private lazy var userIdInputView = CarrotMarketCustomTextInputView(placeholder: "New ID")
    private lazy var passwordInputView = CarrotMarketCustomTextInputView(placeholder: "New Password")
    private lazy var confirmPasswordInputView = CarrotMarketCustomTextInputView(placeholder: "Confirm Password")
    
    private lazy var createAccountButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Create Account", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemOrange
        button.layer.cornerRadius = 8
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.addTarget(self, action: #selector(createAccountButtonDidTap), for: .touchUpInside)
        return button
    }()
    
    private var loginButtonSubscriberForEnable: AnyCancellable?
    private var loginButtonSubscriberForBackgroundColor: AnyCancellable?
    
    @Published var id = ""
    @Published var password = ""
    @Published var repeatPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Sign Up"
        view.backgroundColor = .systemBackground
        
        configureUI()
        
        loginButtonSubscriberForEnable = changeBacgroundColorToSubmit
            .receive(on: RunLoop.main)
            .assign(to: \.backgroundColor, on: createAccountButton)
        
        loginButtonSubscriberForBackgroundColor = enableToSubmit
            .receive(on: RunLoop.main)
            .assign(to: \.isEnabled, on: createAccountButton)
        
    }
    
}

//MARK: - Combine
extension CarrotMarketSignUpViewController {
    private var enableToSubmit: AnyPublisher<(Bool), Never> {
        return eraseToAnyPublisher(successCase: true, failureCase: false)
    }
    
    private var changeBacgroundColorToSubmit: AnyPublisher<(UIColor?), Never> {
        return eraseToAnyPublisher(successCase: UIColor.systemOrange, failureCase: UIColor.systemGray)
    }
    
    private func eraseToAnyPublisher<T>(successCase: T, failureCase: T) -> AnyPublisher<T, Never>{
        return Publishers.CombineLatest3($id, $password, $repeatPassword)
            .map { id, password, repeatPassword in
                if !id.isEmpty && !password.isEmpty && !repeatPassword.isEmpty && password == repeatPassword  {
                    return (successCase)
                }
                return (failureCase)
            }.eraseToAnyPublisher()
    }
}

//MARK: - TextField Delegate
extension CarrotMarketSignUpViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let textFieldText = textField.text ?? ""
        let text = (textFieldText as NSString).replacingCharacters(in: range, with: string)
            
        if textField == emailInputView.inputTextField {
            id = text
        }
        
        if textField == passwordInputView.inputTextField {
            password = text
        }
        
        if textField == confirmPasswordInputView.inputTextField {
            repeatPassword = text
        }
        
        return true
    }
}

//MARK: - Selector
extension CarrotMarketSignUpViewController {
    
    @objc private func createAccountButtonDidTap() {
        //save id, password
        guard let emailString = emailInputView.inputTextField.text else {return}
        guard let idString = userIdInputView.inputTextField.text else {return }
        guard let passwordString = passwordInputView.inputTextField.text else {return }
        
        Auth.auth().createUser(withEmail: emailString, password: passwordString) { result, error in
            if let error = error {
                print(error)
            }
            
            guard let uid = result?.user.uid else {return }
            let value = ["email": emailString,"id": idString, "password": passwordString] as [String: Any]
            
            Database.database().reference().child(DataKey.userAuth).child(uid).updateChildValues(value)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK: - UI
extension CarrotMarketSignUpViewController {
    
    private func configureUI() {
        emailInputView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        passwordInputView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        confirmPasswordInputView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        emailInputView.inputTextField.delegate = self
        passwordInputView.inputTextField.delegate = self
        confirmPasswordInputView.inputTextField.delegate = self
        
        let stackView = UIStackView(arrangedSubviews: [emailInputView, userIdInputView, passwordInputView, confirmPasswordInputView, createAccountButton])
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.widthAnchor.constraint(equalToConstant: view.frame.width / 2),
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
        ])
    }
}
