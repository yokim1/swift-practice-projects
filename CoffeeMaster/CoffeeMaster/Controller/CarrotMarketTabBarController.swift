//
//  File.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/14.
//

import UIKit

class CarrotMarketTabBarController: UITabBarController {
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = [
            navigationControllerCreator(viewController: CarrotMarketCommunityViewController(),
                                        title: "동네생활",
                                        tabbarImage: UIImage(systemName: "list.bullet.rectangle") ,
                                        selectedTabbarImage: UIImage(systemName: "list.bullet.rectangle.fill")),
            
            navigationControllerCreator(viewController: UIViewController(),
                                        title: "홈",
                                        tabbarImage: UIImage(systemName: "house") ,
                                        selectedTabbarImage: UIImage(systemName: "house.fill")),
            
            navigationControllerCreator(viewController: UIViewController(),
                                        title: "내근처",
                                        tabbarImage: UIImage(systemName: "mappin.circle") ,
                                        selectedTabbarImage: UIImage(systemName: "mappin.circle.fill")),
            
            navigationControllerCreator(viewController: UIViewController(),
                                        title: "채팅",
                                        tabbarImage: UIImage(systemName: "bubble.left.and.bubble.right") ,
                                        selectedTabbarImage: UIImage(systemName: "bubble.left.and.bubble.right.fill")),
            
            navigationControllerCreator(viewController: UIViewController(),
                                        title: "나의당근",
                                        tabbarImage: UIImage(systemName: "person") ,
                                        selectedTabbarImage: UIImage(systemName: "person.fill")),
        ]
        
        UITabBar.appearance().tintColor = .black
    }
}

extension CarrotMarketTabBarController {
    
    private func navigationControllerCreator(viewController: UIViewController, title: String, tabbarImage: UIImage?, selectedTabbarImage: UIImage?) -> UINavigationController {
        viewController.title = title
        viewController.tabBarItem = UITabBarItem(title: title, image: tabbarImage, selectedImage: selectedTabbarImage)
        return UINavigationController(rootViewController: viewController)
    }
    
    private func tabBarControllerCreator() -> UITabBarController {
        let tabBarController = UITabBarController()
        return tabBarController
    }
}
