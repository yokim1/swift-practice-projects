//
//  SceneDelegate.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/11.
//

import UIKit
import Firebase

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    private lazy var backgroundLogoView: UIView = {
       let uv = UIView()
        uv.translatesAutoresizingMaskIntoConstraints = false
        uv.backgroundColor = .white
        
        let logoView = UIImageView()
        logoView.image = UIImage(named: "CarrotMarketLogo")
        logoView.backgroundColor = .white
        
        uv.addSubview(logoView)
        logoView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            logoView.centerYAnchor.constraint(equalTo: uv.centerYAnchor),
            logoView.centerXAnchor.constraint(equalTo: uv.centerXAnchor),
            
            logoView.widthAnchor.constraint(equalToConstant: 100),
            logoView.heightAnchor.constraint(equalToConstant: 100),
        ])
        return uv
    }()
    
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        FirebaseApp.configure()
        window?.rootViewController = CarrotMarketTabBarController()
        window?.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
            backgroundLogoView.removeFromSuperview()
    }
    

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        
        if let window = window {
            window.addSubview(backgroundLogoView)
            
            NSLayoutConstraint.activate([
                backgroundLogoView.leadingAnchor.constraint(equalTo: window.leadingAnchor),
                backgroundLogoView.trailingAnchor.constraint(equalTo: window.trailingAnchor),
                backgroundLogoView.topAnchor.constraint(equalTo: window.topAnchor),
                backgroundLogoView.bottomAnchor.constraint(equalTo: window.bottomAnchor)
            ])
        }
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        print("will Enter Foreground")
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        print("DidEnter Foreground")
        
    }
}

