//
//  Categories.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/18.
//

import UIKit

struct CategoryConstants {
    static let basicCategories = ["우리동네질문", "동네사건사고"]
    static let myInterestCategories = ["동네맛집", "동네소식", "분실/실종센터", "일상", "해주세요", "취미생활", "고양이", "강아지", "건강", "살림", "인테리어", "교육","동네사진전", "무기", "삼국지", "출산", "요리", "기타"]
}

struct LayoutConstants {
    static let leadingPadding: CGFloat = 20
    static let trailingPadding: CGFloat = -20
}

struct DataKey {
    static let userAuth = "UserAuth"
    static let feed = "feed"
}
