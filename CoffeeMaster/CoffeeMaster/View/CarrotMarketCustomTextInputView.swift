//
//  File.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/20.
//

import UIKit

class CarrotMarketCustomTextInputView: UIView {
    
    let inputTextField: UITextField = {
       let tf = UITextField()
        tf.backgroundColor = .systemBackground
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        return tf
    }()
    
    private let underBarView: UIView = {
       let uv = UIView()
        uv.backgroundColor = .systemOrange
        return uv
    }()
    
    init(placeholder: String) {
        super.init(frame: .zero)
        
        inputTextField.placeholder = placeholder
        configureUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    private func configureUI() {
        addSubview(inputTextField)
        addSubview(underBarView)
        
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        underBarView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            inputTextField.leadingAnchor.constraint(equalTo: leadingAnchor),
            inputTextField.trailingAnchor.constraint(equalTo: trailingAnchor),
            inputTextField.bottomAnchor.constraint(equalTo: underBarView.topAnchor, constant: -6),

            underBarView.leadingAnchor.constraint(equalTo: inputTextField.leadingAnchor),
            underBarView.trailingAnchor.constraint(equalTo: inputTextField.trailingAnchor),
            underBarView.bottomAnchor.constraint(equalTo: bottomAnchor),
            underBarView.heightAnchor.constraint(equalToConstant: 2),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
