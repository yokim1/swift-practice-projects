//
//  CarrotCommunityCell.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/12.
//
import FirebaseDatabase
import UIKit

protocol CarrotMarketCommunityTableViewCellDelegate: AnyObject {
    func likeButtonDidTap()
    func commentButtonDidTap()
}

class CarrotMarketCommunityTableViewCell: UITableViewCell {
    static let identifier = "CarrotMarketCommunityTableViewCell"
    static let height: CGFloat = 250
    
    //MARK: - Delegate
    weak var delegate: CarrotMarketCommunityTableViewCellDelegate?
    
    //MARK: - Model
    var post: PostModel?
    
    //MARK: - UI Objects
    private let categoryLabel: PaddingLabel = {
       let label = PaddingLabel()
        label.alpha = 0.5
        label.backgroundColor = .systemGray4
        label.textColor = .label
        label.font = .systemFont(ofSize: 12, weight: .semibold)
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 5
        label.paddingLeft = 8
        label.paddingRight = 8
        label.paddingTop = 8
        label.paddingBottom = 8
        return label
    }()
    
    private let bodyLabel: UILabel = {
       let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private let userIdLabel = CarrotMarketBasicInfoLabel()
    private let addressLabel = CarrotMarketBasicInfoLabel()
    private let dateLabel = CarrotMarketBasicInfoLabel()
    private let contentSeparatorView = CarrotMarketSeparatorView(height: 1)
    private let likeButton = CarrotMarketPostTextButton()
    private let commentButton = CarrotMarketPostTextButton()
    private let cellSeparatorView = CarrotMarketSeparatorView(height: 10)
    
    //MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpUI()
        configureButton()
    }
    
    //MARK: - Configure
    func configure(with post: PostModel) {
        categoryLabel.text = post.category
        bodyLabel.text = post.bodyContent
        userIdLabel.text = post.userId
        addressLabel.text = post.address
        
        guard let timeDifference = DateComponentsFormatter().dateFormatter().string(from: post.date, to: Date()) else { return }
        dateLabel.text = post.isEdited ? "\(timeDifference) (Edited)" : "\(timeDifference)"
        
        self.post = post
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Selector
extension CarrotMarketCommunityTableViewCell {
    
    @objc private func likeButtonDidTap() {
        numberOfLikesHandler(for: post)
        delegate?.likeButtonDidTap()
//        Database.database().reference().child(DataKey.feed).child(<#T##pathString: String##String#>)
    }
    
    @objc private func commentButtonDidTap() {
        delegate?.commentButtonDidTap()
    }
}

//MARK: - Likes and Comments Count Handler
extension CarrotMarketCommunityTableViewCell {
    
    private func numberOfLikesHandler(for post: PostModel?) {
        likeButton.isSelected = !likeButton.isSelected
        
        guard var post = post else {return }
        
        if likeButton.isSelected {
            post.numberOfLikes += 1
        } else {
            post.numberOfLikes -= 1
        }
        
        if post.numberOfLikes >= 1 {
            likeButton.setTitle("공감하기 \(post.numberOfLikes)", for: .normal)
        } else {
            likeButton.setTitle("공감하기", for: .normal)
        }
    }
    
    private func numberOfCommentsHandler(for post: PostModel?) {
        guard let post = post else {return }
        
        if post.numberOfComments >= 1 {
            commentButton.setTitle("댓글 \(post.numberOfComments)", for: .normal)
        } else {
            commentButton.setTitle("댓글", for: .normal)
        }
    }
}

extension CarrotMarketCommunityTableViewCell {
    
    private func configureButton() {
        likeButton.setTitle("공감하기", for: .normal)
        commentButton.setTitle("댓글", for: .normal)
        likeButton.addTarget(self, action: #selector(likeButtonDidTap), for: .touchUpInside)
        commentButton.addTarget(self, action: #selector(commentButtonDidTap), for: .touchUpInside)
    }
}


//MARK: - configure UI
extension CarrotMarketCommunityTableViewCell {
    private func setUpUI() {
        let userInfoStackView = UIStackView(arrangedSubviews: [userIdLabel, addressLabel])
        userInfoStackView.axis = .horizontal
        userInfoStackView.spacing = 10
        userInfoStackView.alignment = .leading
        
        let buttonStackView = UIStackView(arrangedSubviews: [likeButton, commentButton])
        buttonStackView.axis = .horizontal
        buttonStackView.spacing = 20
        buttonStackView.alignment = .leading
        
        let items = [categoryLabel, bodyLabel, userInfoStackView, dateLabel, contentSeparatorView, buttonStackView, cellSeparatorView]
        
        items.forEach { item in
            contentView.addSubview(item)
            item.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            categoryLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutConstants.leadingPadding),
            categoryLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            
            bodyLabel.leadingAnchor.constraint(equalTo: categoryLabel.leadingAnchor),
            bodyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: LayoutConstants.trailingPadding),
            bodyLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: 15),
            
            userInfoStackView.leadingAnchor.constraint(equalTo: bodyLabel.leadingAnchor),
            userInfoStackView.topAnchor.constraint(equalTo: bodyLabel.bottomAnchor, constant: 30),
            
            dateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: LayoutConstants.trailingPadding),
            dateLabel.centerYAnchor.constraint(equalTo: userInfoStackView.centerYAnchor),
            
            contentSeparatorView.topAnchor.constraint(equalTo: userInfoStackView.bottomAnchor, constant: 20),
            contentSeparatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            contentSeparatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            buttonStackView.leadingAnchor.constraint(equalTo: categoryLabel.leadingAnchor),
            buttonStackView.topAnchor.constraint(equalTo: contentSeparatorView.bottomAnchor, constant: 10),
            
            cellSeparatorView.topAnchor.constraint(equalTo: buttonStackView.bottomAnchor, constant: 10),
            cellSeparatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            cellSeparatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            cellSeparatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
}

