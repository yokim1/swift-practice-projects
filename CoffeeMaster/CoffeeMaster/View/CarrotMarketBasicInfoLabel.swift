//
//  CarrotMarketBasicInfoLabel.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/19.
//

import UIKit

class CarrotMarketBasicInfoLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        font = .systemFont(ofSize: 14, weight: .light)
        textColor = .gray
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
