//
//  HeaderView.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/18.
//

import UIKit

protocol CategorySelectionHeaderViewDelegate: AnyObject {
    func categoryDidTap(category: String)
}

class CategorySelectionHeaderView: UIView {

    //MARK: - UI Objects
    private lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .systemGray5
        cv.isScrollEnabled = true
        cv.contentInset = .init(top: 0, left: LayoutConstants.leadingPadding, bottom: 0, right: -LayoutConstants.trailingPadding)
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    //MARK: - Model
    var categories:[String] = []
    
    //MARK: - Delegate
    weak var delegate: CategorySelectionHeaderViewDelegate?
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureCategories()
        configureCollectionView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - CollectionView DataSource
extension CategorySelectionHeaderView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategorySelectionHeaderViewCell
                                                        .identifier, for: indexPath) as? CategorySelectionHeaderViewCell else { return UICollectionViewCell() }
        
        cell.configure(with: categories[indexPath.row])
        return cell
    }
}

//MARK: - CollectionView Delegate
extension CategorySelectionHeaderView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        delegate?.categoryDidTap(category: categories[indexPath.row])
    }
}

//MARK: - UI Set up
extension CategorySelectionHeaderView {
    private func configureCategories(){
        categories.append("전체")
        categories.append(contentsOf: CategoryConstants.basicCategories)
        categories.append(contentsOf: CategoryConstants.myInterestCategories)
    }
    
    private func configureCollectionView() {
        collectionView.register(CategorySelectionHeaderViewCell.self, forCellWithReuseIdentifier: CategorySelectionHeaderViewCell.identifier)
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
            layout.itemSize = UICollectionViewFlowLayout.automaticSize
        }
        
        addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
}
