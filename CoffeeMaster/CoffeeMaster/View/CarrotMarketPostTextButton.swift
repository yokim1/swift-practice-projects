//
//  CarrotMarketPostTextButton.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/19.
//

import UIKit

class CarrotMarketPostTextButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel?.font = .systemFont(ofSize: 16, weight: .medium)
        setTitleColor(.darkGray, for: .normal)
        setTitleColor(.systemOrange, for: .selected)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

