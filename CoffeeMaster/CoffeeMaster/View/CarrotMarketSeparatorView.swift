//
//  CarrotMarketSeparatorView.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/19.
//

import UIKit

class CarrotMarketSeparatorView: UIView {
    
    init(height: CGFloat) {
        super.init(frame: .zero)
        backgroundColor = .lightGray
        alpha = 0.2
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
