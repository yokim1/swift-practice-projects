//
//  File.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/15.
//

import UIKit

class CarrotMarketImageCircularButton: UIButton {
    
    //MARK: - Init
    init(image: UIImage?, color: UIColor, width: CGFloat, isShadow: Bool = true) {
        super.init(frame: .zero)

        setImage(image, for: .normal)
        backgroundColor = color
        layer.cornerRadius = width / 2
        alpha = 1
        tintColor = .white
        
        if isShadow {
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOpacity = 0.5
            layer.shadowOffset = CGSize.zero
            layer.shadowRadius = 6
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
