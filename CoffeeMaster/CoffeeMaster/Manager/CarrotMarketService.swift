//
//  CarrotMarketService.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/26.
//
import FirebaseAuth
import FirebaseDatabase
import UIKit

class CarrotMarketService{
    static let shared = CarrotMarketService()
    
    func uploadPost(post: PostModel, completion: @escaping (Error?, DatabaseReference) -> Void) {
//        guard let uid = Auth.auth().currentUser?.uid else {return }
        
        let postDictionary = [
//            "category" : post.category,
//            "bodyContent" : post.bodyContent,
//            "isLiked" : post.isLiked,
//            "numberOfLikes" : post.numberOfLikes,
//            "numberOfComments" : post.numberOfComments,
//            "isEdited" : post.isEdited,
//            "date" : post.date,
//            "userId" : post.userInfo.userId,
//            "address" : post.userInfo.address,
            "category": post.category,
            "bodyContent": post.bodyContent,
            "isLiked": post.isLiked,
            "numberOfLikes": post.numberOfLikes,
            "numberOfComments": post.numberOfComments,
            "isEdited": post.isEdited,
            "date": Int(NSDate().timeIntervalSince1970),
//            "userInfo": post.userInfo
            "userId": post.userId,
            "address": post.address
        ] as [String : Any]
        
        Database.database().reference().child(DataKey.feed).childByAutoId().updateChildValues(postDictionary, withCompletionBlock: completion)
    }
    
    func fetchPost(completion: @escaping ([PostModel]) -> Void){
        Database.database().reference().child(DataKey.feed).observe(.childAdded) { snapshot in
            guard let dictionary = snapshot.value as? [String: Any ] else {return }
//            let post = PostModel(category: dictionary["category"],
//                                 bodyContent: dictionary["bodyContent"],
//                                 userId: dictionary["userId"],
//                                 address: dictionary["address"],
//                                 date: dictionary["date"] as! Int,
//                                 isEdited: dictionary["category"],
//                                 isLiked: dictionary["category"],
//                                 numberOfLikes: dictionary["category"],
//                                 numberOfComments: dictionary["category"])
            
        }
    }
}
