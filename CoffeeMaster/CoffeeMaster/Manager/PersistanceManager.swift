//
//  PersistanceManager.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/12.
//

import UIKit

class PersistanceManager {
    
    enum Keys {
        static let rememberMe = "rememberMe"
        static let post = "post"
    }
    
    func update(to actionType: ActionTypeForPost, from oldPost: PostModel? = nil,for post: PostModel, completed: @escaping ((Error?) -> Void)) {
        
       retrieveData { results in
           switch results {
           
           case .success(let posts):
               var retrievedPosts = posts
            
               switch  actionType {
               case .add:
                    guard !retrievedPosts.contains(post) else { completed(.none); return }
                    retrievedPosts.append(post)
                   
               case .remove:
                    retrievedPosts.removeAll { retrievedPost in
                        post == retrievedPost
                    }
                
               case .edit:
                    guard let oldPost = oldPost else { return }
                    if let index = retrievedPosts.firstIndex(of: oldPost) {
                        retrievedPosts[index] = post
                        
                    }
               }
            
            completed(self.save(post: retrievedPosts))
            
           case.failure(let error):
               completed(error)
           }
       }
   }
    
    func retrieveData(completed: @escaping (Result<[PostModel], Error>) -> Void) {
        guard let post =  UserDefaults.standard.object(forKey: Keys.post) as? Data else {
            completed(.success([]))
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let posts = try decoder.decode([PostModel].self, from: post)
            completed(.success(posts))
        } catch {
            completed(.failure(error))
        }
    }
    
   private func save(post: [PostModel]) -> Error? {
        do{
            let encoder = JSONEncoder()
            let encodedFavorites = try encoder.encode(post)
            UserDefaults.standard.setValue(encodedFavorites, forKey: Keys.post)
            return nil
        } catch {
            return .none
        }
    }
    
    func retrieveRememberMe(completed: @escaping ((Result<AuthRemeberMe, Error>)?) -> Void) {
        guard let rememberMe = UserDefaults.standard.object(forKey: Keys.rememberMe) as? Data else{
            completed(nil)
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let rememeber = try decoder.decode(AuthRemeberMe.self, from: rememberMe)
            completed(.success(rememeber))
        } catch {
            completed(.failure(error))
        }
    }
    
    func saveRememberMe(rememberMe: AuthRemeberMe) -> Error? {
        do {
           let encoder = JSONEncoder()
            let encodedFavorites = try encoder.encode(rememberMe)
            UserDefaults.standard.setValue(encodedFavorites, forKey: Keys.rememberMe)
            return nil
        } catch {
            return .none
        }
    }
}

struct AuthRemeberMe: Codable {
    let rememberMe: Bool
}
