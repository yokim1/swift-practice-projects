//
//  Enum.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/19.
//

import Foundation

enum ActionTypeForPost: String {
    case add, remove = "Delete", edit = "Edit"
}
