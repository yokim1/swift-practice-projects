//
//  PostModel.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/12.
//

import UIKit

struct PostModel: Codable, Equatable {
    var category: String
    let bodyContent: String
//    let userInfo: UserInfo
    let userId: String
    let address: String
    let date: Date
    var isEdited: Bool
    var isLiked: Bool
    var numberOfLikes: Int
    var numberOfComments: Int
    
    init(dictionary: [String: Any]) {
        
        self.category = dictionary["category"] as? String ?? ""
        self.bodyContent = dictionary["bodyContent"] as? String ?? ""
        self.userId = dictionary["userId"] as? String ?? ""
        self.address = dictionary["address"] as? String ?? ""
        
//        if let timestamp =  {
        self.date = Date(timeIntervalSince1970: dictionary["date"] as? Double ?? 0.0)
//        }
        
        self.isEdited = dictionary["isEdited"] as? Bool ?? false
        self.isLiked = dictionary["isLiked"] as? Bool ?? false
        self.numberOfLikes = dictionary["numberOfLikes"] as? Int ?? 0
        self.numberOfComments = dictionary["numberOfComments"] as? Int ?? 0
        
    }
}
