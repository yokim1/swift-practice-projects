//
//  UserInfo.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/20.
//

import Foundation

struct UserInfo: Codable, Equatable {
    let userId: String
    let address: String
}

