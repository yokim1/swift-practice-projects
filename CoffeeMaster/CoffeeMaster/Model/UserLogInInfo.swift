//
//  UserLogInInfo.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/21.
//

import Foundation

struct UserLogInInfo: Codable {
    let userInfo: UserInfo
    let password: String
    let isAutomaticSignUp: Bool
}
