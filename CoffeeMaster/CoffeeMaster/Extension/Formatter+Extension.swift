//
//  Extension.swift
//  CoffeeMaster
//
//  Created by 김윤석 on 2021/09/15.
//

import UIKit

extension Formatter {
    func dateFormatter() -> DateComponentsFormatter {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.second, .minute, .hour, .day, .weekOfMonth]
        formatter.maximumUnitCount = 1
        formatter.unitsStyle = .abbreviated
        return formatter
    }
}
