/*:
 [< Previous](@previous)           [Home](Introduction)

 ## sorted
 Use sorted to sort collection data using the given predicate as the comparison between elements.

*/
/*:
 #### Arrays
 */
/* Sort this array */
var numbers = [10, 5, 2, 8, 3, 9, 4, 1, 7, 6]

var sortedNumbers = numbers.sorted()
sortedNumbers

/* reverse sort */
var renumbers = numbers.sorted{$0 > $1}
renumbers

/* Use shortcut argument*/

/* Shorter yet */
var rrenumbers = numbers.sorted(by: >)
rrenumbers

/* Example 2: Arrays of Objects */
struct Person {
    var name: String
    var age: Int
}
var people = [
    Person(name: "Moe", age: 12),
    Person(name: "Larry", age: 22),
    Person(name: "Shemp", age: 25),
    Person(name: "Curly", age: 16)
]
/* Sort by name */
let sortedName = people.sorted{$0.name < $1.name}
sortedName

/* Sort by age using shortcut argument */
let sortedAge = people.sorted{$0.age < $1.age}
sortedAge

/* Example 3: Make the struct Comparable so you can sort by
   name without having to specify property in sorted */
struct Employee : Comparable {
    static func < (lhs: Employee, rhs: Employee) -> Bool {
        lhs.salary < rhs.salary
    }
    
    var name: String
    var salary: Double
}
var employees = [
    Employee(name: "Moe", salary: 100_000),
    Employee(name: "Larry", salary: 88_500),
    Employee(name: "Shemp", salary: 125_500),
    Employee(name: "Curly", salary: 96_900)
]

///* How about a shorthand reverse sort on salary */
let employ: () = employees.sort()
employ

let sortedSalaries = employees.sorted(by: >)
sortedSalaries
/*:
 #### Dictionaries
 */
let peopleDict = ["Curly": 16, "Larry": 22, "Moe": 12, "Shemp": 25]
/* sort by keys (names)*/
let sortName = peopleDict.sorted{$0.key < $1.key}
sortName

/* use shorthand arguments */

/*  sort by values (age) */
let sortValue = peopleDict.sorted { $0.value < $1.value}
sortValue
/*:

[< Previous](@previous)           [Home](Introduction)
*/

