/*:
 [< Previous](@previous)           [Home](Introduction)           [Next >](@next)
 
 ## filter
Use filter to loop over a collection and return a collection containing only those elements that match an include condition.
 */
/*:
 #### Simple Arrays
 */
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
/* Example 1: Filter this array to return another array of integers keeping only values that are less than or equal to 5
   Use for...in loop */

/* use Filter */
let lessfive = numbers.filter {$0 <= 5}
lessfive

/* use shorthand */

/* Example 2: Filter this array to return another array of Strings beginning with the letter 'a' or 'A' */
let names = ["Alice", "Bert", "Allen", "Samantha", "Ted", "albert"]
let anames = names.filter {$0.uppercased().prefix(1) == "A"}
anames

/* Example 3: Find the First person who matches the conditions above */
let anames1 = names.filter {$0.uppercased().prefix(1) == "A"}.first
anames1

/*:
#### Arrays of Objects
*/
struct Person {
    var name: String
    var age: Int
}
let people = [
    Person(name: "Curly", age: 16),
    Person(name: "Larry", age: 22),
    Person(name: "Moe", age: 12),
    Person(name: "Shemp", age: 25)
]
/* Example 1:  Get an array of people who are adults (age >= 19) */
let moreAges = people.filter { $0.age >= 19}
moreAges

/* Example 2: Get an array of people who are older than 20 with names beginning with 'M' */
let example2 = people.filter{$0.age <= 20 && $0.name.uppercased().prefix(1) == "M"}
example2

/* Example 3: Get an array of names of children (age < 19)
   Hint: Combine filter with map */
let ex3 = people.filter{$0.age < 19}.map { $0.name }
ex3
/*:
#### Dictionaries
*/
let peopleDict = ["Curly": 16, "Larry": 22, "Moe": 12, "Shemp": 25]
/* Filter to return a dictionary where the values are >= 19 */
let peopleDict1 = peopleDict.filter { $0.value >= 19}
peopleDict1

/* Shorthand version */

/*:
#### Sets
*/
let weights: Set = [98.5, 102.7, 100.2, 88.4]
/* Generate a set of weights where the values are < 100 */
let weights1 = Set(weights.filter { $0 < 100 })
weights1
/*:
 [< Previous](@previous)           [Home](Introduction)           [Next >](@next)
 */
