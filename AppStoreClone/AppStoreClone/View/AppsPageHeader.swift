//
//  AppsPageHeader.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/11.
//

import UIKit

class AppsPageHeader: UICollectionReusableView {
    
    static let identifier = "AppsPageHeader"
    
    let horizontalPageController = AppsHeaderHorizontalCollectionViewController()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .blue
        
        addSubview(horizontalPageController.view)
        horizontalPageController.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            horizontalPageController.view.topAnchor.constraint(equalTo: topAnchor),
            horizontalPageController.view.leadingAnchor.constraint(equalTo: leadingAnchor),
            horizontalPageController.view.trailingAnchor.constraint(equalTo: trailingAnchor),
            horizontalPageController.view.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
