//
//  File.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/11.
//

import UIKit

class AppCell: UICollectionViewCell {
    static let identifier = "AppCell"
    
    let imageView = UIImageView()
    
    let titleLabel = UILabel()
    let companyLabel = UILabel()
    
    let getButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        imageView.backgroundColor = .red
        imageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.layer.cornerRadius = 9
        
        titleLabel.text = "App Name"
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        companyLabel.text = "Company Name"
        companyLabel.font = UIFont.systemFont(ofSize: 12)
        
        getButton.setTitle("Get", for: .normal)
        getButton.backgroundColor = .gray
        getButton.layer.cornerRadius = 16
        getButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        getButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        let labelStackView = UIStackView(arrangedSubviews: [
            titleLabel, companyLabel
        ])
        labelStackView.axis = .vertical
        
        let viewStackView = UIStackView(arrangedSubviews: [
            imageView, labelStackView, getButton
        ])
        
        addSubview(viewStackView)
        viewStackView.spacing = 10
        viewStackView.alignment = .center
        viewStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            viewStackView.topAnchor.constraint(equalTo: topAnchor),
            viewStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            viewStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            viewStackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
}
