//
//  AppsCell.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/11.
//

import UIKit

class AppsGroupCell: UICollectionViewCell {
    static let identifier = "AppsGroupCell"
    
    static let prefferedHeight = 350
    
    let titleLabel = UILabel()
    
    let horizontalCollectionView = HorizontalCollectionView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
//        titleLabel.text = "App Section"
        titleLabel.font = .boldSystemFont(ofSize: 30)
        
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(horizontalCollectionView.view)
        horizontalCollectionView.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            horizontalCollectionView.view.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            horizontalCollectionView.view.leadingAnchor.constraint(equalTo: leadingAnchor),
            horizontalCollectionView.view.trailingAnchor.constraint(equalTo: trailingAnchor),
            horizontalCollectionView.view.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

