//
//  File.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/11.
//

import UIKit

class AppsHeaderCell: UICollectionViewCell {
    
    static let identifier = "AppHeaderCell"
    
    let companyLabel = UILabel()
    let descriptionLabel = UILabel()
    let imageView = UIImageView()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        companyLabel.text = "CompanyLabel"
        companyLabel.font = .systemFont(ofSize: 12)
        
        descriptionLabel.text = "This is Description Label for this app. Please Download this app and enjoy"
        descriptionLabel.numberOfLines = 2
        descriptionLabel.font = .systemFont(ofSize: 20)
            
        imageView.backgroundColor = .blue
        imageView.layer.cornerRadius = 6
        
        let stackView = UIStackView(arrangedSubviews: [
            companyLabel, descriptionLabel, imageView
        ])
        
        stackView.axis = .vertical
        stackView.spacing = 12
        
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
