//
//  SearchResultCell.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/09.
//

import UIKit

class SearchResultCell: UICollectionViewCell {
    
    static let identifier = "SearchResultCell"
    
    let imageView = UIImageView()
    let titleLabel = UILabel()
    let ratingLabel = UILabel()
    let categoryLabel = UILabel()
    let getButtonLabel = UIButton()
    
    let screenShot1 = UIImageView()
    let screenShot2 = UIImageView()
    let screenShot3 = UIImageView()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .yellow
        
        imageView.backgroundColor = .red
        imageView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        imageView.layer.cornerRadius = 6
        
        titleLabel.text = "Title Label"
        
        categoryLabel.text = "CategoryLabel"
        
        ratingLabel.text = "RatingLabel"
        
        getButtonLabel.setTitle("Get", for: .normal)
        getButtonLabel.backgroundColor = .gray
        getButtonLabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        screenShot1.backgroundColor = .red
        screenShot2.backgroundColor = .red
        screenShot3.backgroundColor = .red
        
        let labelStackView = UIStackView(arrangedSubviews: [
            titleLabel, categoryLabel, ratingLabel
        ])
        
        labelStackView.axis = .vertical
        
        let appInfoStackView = UIStackView(arrangedSubviews: [
            imageView, labelStackView, getButtonLabel
        ])
        appInfoStackView.spacing = 12
        appInfoStackView.alignment = .center
        
        let screenShotStackView = UIStackView(arrangedSubviews: [
            screenShot1, screenShot2, screenShot3
        ])
        screenShotStackView.spacing = 12
        screenShotStackView.distribution = .fillEqually
        
        let totalStackView = UIStackView(arrangedSubviews: [
            appInfoStackView, screenShotStackView
        ])
    
        totalStackView.axis = .vertical
        totalStackView.spacing = 12
        
        addSubview(totalStackView)
        
        totalStackView.translatesAutoresizingMaskIntoConstraints = false
        
        let padding: CGFloat = 12
        
        NSLayoutConstraint.activate([
            totalStackView.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            totalStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            totalStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            totalStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func configureUI(){
        
    }
    
}
