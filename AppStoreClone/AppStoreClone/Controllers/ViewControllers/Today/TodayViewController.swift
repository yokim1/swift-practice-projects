//
//  ViewController3.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/09.
//

import UIKit

class TodayViewController: BaseListCollectionCollectionViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.backgroundColor = .gray
        navigationController?.isNavigationBarHidden = true
        collectionView.register(TodayAppCell.self, forCellWithReuseIdentifier: TodayAppCell.identifier)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TodayAppCell.identifier, for: indexPath)
        
        return cell
    }
    
    var topConstraint: NSLayoutConstraint?
    var leadingConstraint: NSLayoutConstraint?
    var widthConstraint: NSLayoutConstraint?
    var heightConstraint: NSLayoutConstraint?
    
    var todayDetailVC: UITableViewController!
    
    var startingFrame: CGRect?
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let todayDetailViewController = TodayDetailViewController()
        self.todayDetailVC = todayDetailViewController
        addChild(todayDetailViewController)
        
        let todayDetailVCView = todayDetailViewController.view!
        
        todayDetailVCView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissCellView)))
        
        view.addSubview(todayDetailVCView)
        
        todayDetailVCView.layer.cornerRadius = 16
        
        guard let cell = collectionView.cellForItem(at: indexPath) else {return }
        guard let startingFrame = cell.superview?.convert(cell.frame, to: nil) else {return }
        
        //auto layout animation
        todayDetailVCView.translatesAutoresizingMaskIntoConstraints = false
        
        topConstraint = todayDetailVCView.topAnchor.constraint(equalTo: view.topAnchor, constant: startingFrame.origin.y)
        leadingConstraint = todayDetailVCView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: startingFrame.origin.x)
        widthConstraint = todayDetailVCView.widthAnchor.constraint(equalToConstant: startingFrame.width)
        heightConstraint = todayDetailVCView.heightAnchor.constraint(equalToConstant: startingFrame.height)
        
        [topConstraint, leadingConstraint, widthConstraint,heightConstraint].forEach {
            $0?.isActive = true
        }
        
        self.startingFrame = startingFrame
        
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut) {
            
            self.topConstraint?.constant = 0
            self.leadingConstraint?.constant = 0
            self.widthConstraint?.constant = self.view.frame.width
            self.heightConstraint?.constant = self.view.frame.height
            
            self.view.layoutIfNeeded()
            
            self.tabBarController?.tabBar.frame.origin.y += 100
        }
    }
    
    @objc
    private func dismissCellView(gesture: UIGestureRecognizer) {
        UIView.animate(withDuration: 0.9, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: .curveEaseIn) {
            
            guard let startingFrame = self.startingFrame else {return }
            
            self.todayDetailVC?.tableView.contentOffset = .zero
            
            self.topConstraint?.constant = startingFrame.origin.y
            self.leadingConstraint?.constant = startingFrame.origin.x
            self.widthConstraint?.constant = startingFrame.width
            self.heightConstraint?.constant = startingFrame.height
            
            self.view.layoutIfNeeded()
            self.tabBarController?.tabBar.frame.origin.y -= 100
            
        } completion: { _ in
            self.todayDetailVC?.view?.removeFromSuperview()
            self.todayDetailVC?.removeFromParent()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 48, height: 450)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        .init(top: 48, left: 0, bottom: 48, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        32
    }
}

