//
//  BaseListCollectionCollectionViewController.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/11.
//

import UIKit

class BaseListCollectionCollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    init(){
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
