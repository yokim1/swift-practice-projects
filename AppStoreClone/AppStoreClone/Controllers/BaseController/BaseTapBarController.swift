//
//  BaseTapBarController.swift
//  AppStoreClone
//
//  Created by 김윤석 on 2021/07/09.
//

import UIKit

class BaseTapBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Today, Apps, Search
        
        viewControllers = [
            configureTabBarItem(for: TodayViewController(),
                                title: "Today",
                                image: UIImage(systemName: "calendar"),
                                selectedImage: nil),
            
            configureTabBarItem(for: AppsViewController(),
                                title: "Apps",
                                image: UIImage(systemName: "apps.iphone"),
                                selectedImage: nil),
            
            configureTabBarItem(for: SearchViewController(),
                                title: "Search",
                                image: UIImage(systemName: "magnifyingglass"),
                                selectedImage: nil),
            ]
    }
    
    private func configureTabBarItem(for vc: UIViewController, title: String, image: UIImage?, selectedImage: UIImage?) -> UIViewController {
        
        let navController = UINavigationController(rootViewController: vc)
        
        navController.navigationBar.prefersLargeTitles = true
        
        vc.navigationItem.title                    = title
        vc.view.backgroundColor                    = .white
        
        navController.tabBarItem.title             = title
        navController.tabBarItem.image             = image
        navController.tabBarItem.selectedImage     = selectedImage
        return navController
    }
}

