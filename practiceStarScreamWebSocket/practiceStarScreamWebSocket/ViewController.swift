//
//  ViewController.swift
//  practiceStarScreamWebSocket
//
//  Created by 김윤석 on 2021/09/08.
//
import Starscream
import UIKit

class ViewController: UIViewController {

//    let webSocketTask = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue()).webSocketTask(with: URL(string: "wss://ws.finnhub.io?token=c3c6me2ad3iefuuilms0")!)
//
    lazy var urlSession: URLSession = {
        let us = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        return us
    }()
    
    var webSocketTask: URLSessionWebSocketTask?
    
    var isConnected: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        var request = URLRequest(url: URL(string: "wss://ws.finnhub.io?token=c3c6me2ad3iefuuilms0")!)
//        request.timeoutInterval = 2
//        let socket = WebSocket(request: request)
//        socket.delegate = self
//        socket.connect()
//
//        var request = URLRequest(url: URL(string: "wss://ws.finnhub.io?token=c3c6me2ad3iefuuilms0")!)
//        request.timeoutInterval = 5
//        let socket = WebSocket(request: request)
//        socket.delegate = self
//        socket.connect()
        let webSocketTask = urlSession.webSocketTask(with: URL(string: "wss://ws.finnhub.io?token=c3c6me2ad3iefuuilms0")!)
        webSocketTask.resume()
        listen()
        ping()
    }
    
    
    func listen() {
        webSocketTask?.receive { result in
            switch result {
            
            case .failure(let error):
                print("Failed to receive message: \(error)")
                
            case .success(let message):
                
                switch message {
                case .string(let text):
                    print("Received text message: \(text)")
                    print("successfully connected")
                    
                case .data(let data):
                    print("Received binary message: \(data)")
                    
                @unknown default:
                    fatalError()
                }
                
                self.listen()
            }
            
        }
    }
    
    func ping() {
        webSocketTask?.sendPing { (error) in
            if let error = error {
                print("Ping failed: \(error)")
            }
            print("sent ping")
            DispatchQueue.global().asyncAfter(deadline: .now() + 40) {
                self.ping()
            }
        }
    }
}

extension ViewController: URLSessionWebSocketDelegate {
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        print("Web Socket did connect")
        
        webSocketTask.receive { result in
            switch result{
            case .failure(let error):
                print(error)
                break
                
            case .success(let message):
                print(message)
                break
            }
        }
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        print("Web Socket did disconnect")
    }
}

//
//extension ViewController: WebSocketDelegate {
//    func didReceive(event: WebSocketEvent, client: WebSocket) {
//        switch event {
//        case .connected(let headers):
//            isConnected = true
//            print("websocket is connected: \(headers)")
//        case .disconnected(let reason, let code):
//            isConnected = false
//            print("websocket is disconnected: \(reason) with code: \(code)")
//        case .text(let string):
//            print("Received text: \(string)")
//        case .binary(let data):
//            print("Received data: \(data.count)")
//        case .ping(_):
//            break
//        case .pong(_):
//            break
//        case .viabilityChanged(_):
//            break
//        case .reconnectSuggested(_):
//            break
//        case .cancelled:
//            isConnected = false
//        case .error(let error):
//            isConnected = false
//            //handleError(error)
//        }
//    }
//}
