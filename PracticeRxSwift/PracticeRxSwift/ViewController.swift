//
//  ViewController.swift
//  PracticeRxSwift
//
//  Created by 김윤석 on 2021/08/21.
//
import Photos
import RxSwift
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let navC = segue.destination as? UINavigationController,
              let photoCVC = navC.viewControllers.first as? PhotoCollectionViewController else { return}
        
        photoCVC.selectedPhoto.subscribe(onNext: {[weak self] photo in
            
            self?.imageView.image = photo
        }).disposed(by: disposeBag)
    }
}

