import UIKit
import RxSwift
import RxCocoa

let strike = PublishSubject<String>()

let disposeBag = DisposeBag()

//strike.ignoreElements().subscribe { _ in
//    print("Subscription")
//}.disposed(by: disposeBag)
//
//strike.onNext("A")

strike.element(at: 2).subscribe { _ in
    print("You are out")
}.disposed(by: disposeBag)


strike.onNext("A")
strike.onNext("A")
strike.onNext("A")
