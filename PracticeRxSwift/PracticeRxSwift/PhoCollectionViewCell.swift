//
//  PhoCollectionViewCell.swift
//  PracticeRxSwift
//
//  Created by 김윤석 on 2021/09/01.
//

import UIKit

class PhoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
