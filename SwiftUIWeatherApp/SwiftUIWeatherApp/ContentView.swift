//
//  ContentView.swift
//  SwiftUIWeatherApp
//
//  Created by 김윤석 on 2021/12/26.
//

import SwiftUI

struct ContentView: View {
    
    @State private var isNight: Bool = false
    
    var body: some View {
        ZStack{
            
            BackgroundView(colors: isNight ?
                           [.black, .blue] : [.blue, .white])
            
            VStack{
                LocationTextView(title: "Shibuya, Tokyo")
                
                MainWeatherView(imageName: "cloud.sun.fill", temp: "76")
                
                HStack(spacing: 20) {
                    WeatherDayView(dayOfWeek: "Tue", imageName: "cloud.sun.fill", temperature: 50)
                    WeatherDayView(dayOfWeek: "Wed", imageName: "cloud.sun.fill", temperature: 60)
                    WeatherDayView(dayOfWeek: "Thur", imageName: "cloud.sun.fill", temperature: 70)
                    WeatherDayView(dayOfWeek: "Fri", imageName: "cloud.sun.fill", temperature: 80)
                    WeatherDayView(dayOfWeek: "Sat", imageName: "cloud.sun.fill", temperature: 85)
                    WeatherDayView(dayOfWeek: "Sun", imageName: "cloud.sun.fill", temperature: 90)
                }
               
                Spacer()
                
                ChangeDayTimeButton(title: "change day time", isNight: $isNight)
                
                Spacer()
            }
        }
    }
}

struct WeatherDayView: View {
    var dayOfWeek: String
    var imageName: String
    var temperature: Int
    
    var body: some View {
        VStack {
            Text(dayOfWeek)
                .foregroundColor(.white)
            
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
            
            Text("\(temperature)")
                .font(.system(size: 28, weight: .medium))
                .foregroundColor(.white)
        }
    }
}

struct BackgroundView: View {
    var colors: [Color]
    
    var body: some View {
        LinearGradient(colors: colors, startPoint: .topLeading, endPoint: .bottomTrailing)
            .ignoresSafeArea(.all)
    }
}

struct ChangeDayTimeButton: View {
    var title: String
    @Binding var isNight: Bool
    
    var body: some View {
        Button {
            isNight.toggle()
        } label: {
            Text(title)
                .frame(width: 280, height: 50)
                .background(.white)
                .font(.system(size: 20, weight: .bold, design: .default))
                .cornerRadius(10)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}

struct MainWeatherView: View {
    var imageName: String
    var temp: String
    
    var body: some View {
        VStack {
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 180, height: 180)
            
            Text(temp)
                .font(.system(size: 70, weight: .medium))
                .foregroundColor(.white)
        }
        .padding(.bottom, 40)
    }
}

struct LocationTextView: View {
    var title: String
    
    var body: some View {
        Text(title)
            .font(.system(size: 32, weight: .medium, design: .default))
            .foregroundColor(.white)
            .padding()
    }
}
