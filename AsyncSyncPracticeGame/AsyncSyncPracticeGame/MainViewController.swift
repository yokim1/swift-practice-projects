//
//  ViewController.swift
//  AsyncSyncPracticeGame
//
//  Created by 김윤석 on 2021/10/02.
//

import UIKit

class MainViewController: UIViewController {
    
    private lazy var resetButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Reset", for: .normal)
        button.setTitleColor( .white, for: .normal)
        button.backgroundColor = .tintColor
        button.addTarget(self, action: #selector(resetButtonDidTap), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    @objc private func resetButtonDidTap(){
        
    }
    
    private lazy var gameView: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var timerLabel: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.backgroundColor = .systemOrange
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 24, weight: .heavy)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    private var gameMode: Int = 4
    private var tileSize: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for row in 0..<gameMode{
            makeTiles(row: row)
        }
    }
    
    private func makeTiles(row: Int) {
        tileSize = 500 / CGFloat(gameMode)
        
        let horizontalStackView = UIStackView()
        for _ in 0..<gameMode {
            let tile = UILabel()
            tile.text = "tile"
            tile.textAlignment = .center
            tile.backgroundColor = .red
            horizontalStackView.addArrangedSubview(tile)
        }
        gameView.addSubview(horizontalStackView)
        horizontalStackView.spacing = 6
        horizontalStackView.distribution = .equalSpacing
        horizontalStackView.axis = .horizontal
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        
//        gameView.addSubview(verticalStackView)
        
        NSLayoutConstraint.activate([
            horizontalStackView.topAnchor.constraint(equalTo: gameView.topAnchor, constant: CGFloat(row * (Int(100)))),
            horizontalStackView.leadingAnchor.constraint(equalTo: gameView.leadingAnchor),
            horizontalStackView.trailingAnchor.constraint(equalTo: gameView.trailingAnchor)
        ])
    }
    
    
    
    private func setUpUI() {
        
        view.addSubview(resetButton)
        view.addSubview(gameView)
        view.addSubview(timerLabel)
        
        NSLayoutConstraint.activate([
            
            resetButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            resetButton.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            resetButton.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            resetButton.heightAnchor.constraint(equalToConstant: 100),
            
            gameView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            gameView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            gameView.heightAnchor.constraint(equalTo: gameView.widthAnchor),
            gameView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            timerLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            timerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            timerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            timerLabel.heightAnchor.constraint(equalToConstant: 100),
        ])
    }
}
