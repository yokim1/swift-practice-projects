//
//  Model.swift
//  practiceRxSwiftToDoList
//
//  Created by 김윤석 on 2021/09/02.
//

import Foundation

enum Priority: Int {
    case high
    case medium
    case low
}

struct Task{
    let title: String
    let priority: Priority
}
