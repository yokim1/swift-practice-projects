//
//  AddTaskViewController.swift
//  practiceRxSwiftToDoList
//
//  Created by 김윤석 on 2021/09/02.
//
import RxSwift
import UIKit

class AddTaskViewController: UIViewController {
    
    private let taskSubject = PublishSubject<Task>()
    
    var taskSubjectObservable: Observable<Task> {
        return taskSubject.asObservable()
    }
    
    @IBAction func saveButton() {
        guard let priority = Priority(rawValue: segmentControl.selectedSegmentIndex),
              let title = taskTextField.text else {return }
        let task = Task(title: title, priority: priority)
        taskSubject.onNext(task)
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var taskTextField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
