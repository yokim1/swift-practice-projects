//
//  ViewController.swift
//  practiceRxSwiftToDoList
//
//  Created by 김윤석 on 2021/09/02.
//
import RxSwift
import RxCocoa
import UIKit

class cell: UITableViewCell{
    static let id = "cell"
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    private var tasks = BehaviorRelay<[Task]>(value: [])
    
    let disposeBag = DisposeBag()
    
    @IBAction func addButton() {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let navC = segue.destination as? UINavigationController,
              let addVC = navC.viewControllers.first as? AddTaskViewController else { return }
        addVC.taskSubjectObservable
            .subscribe {[weak self] task in
                guard let self = self else {return}
                let priority = Priority(rawValue: self.segmentCOntrol.selectedSegmentIndex - 1)
                
                var existingTasks = self.tasks.value
                existingTasks.append(task.element!)
                
                self.tasks.accept(existingTasks)
                
                self.filterTask(by: priority)
            }.disposed(by: disposeBag)
        
    }
    
    private var filteredTask = [Task]()
    
    private func filterTask(by priority: Priority?){
        if priority == nil {
            self.filteredTask = self.tasks.value
        } else{
            self.tasks.map { tasks in
                return tasks.filter { $0.priority == priority}
            }.subscribe { [weak self] tasks in
                self?.filteredTask = tasks.element!
                print(tasks)
            }.disposed(by: disposeBag)
        }
    }
    
    @IBOutlet weak var segmentCOntrol: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
    
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cell.id, for: indexPath)
        return cell
    }

}

