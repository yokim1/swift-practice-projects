//
//  NetworkManager.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/17.
//

import UIKit

class NetworkManager {
    static let shared = NetworkManager()
    
    private let baseUrl       = "http://api.github.com/users/"
    //let cache         = NSCache<NSString, UIImage>()
    
    private init(){ }
    
    func getFollowers(for username: String, page: Int, completionHandler: @escaping (Result<[Follower]?, GFError>) -> Void) {
        let endPoint = baseUrl + "\(username)/followers?per_page=100&page=\(page)"
        
        guard let url = URL(string: endPoint) else { return completionHandler(.failure(.invalidUsername))}
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let _ = error{
                print(url)
                completionHandler(.failure(.unableToComplete))
                return
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completionHandler(.failure(.invalidResponse))
                return
            }
            
            guard let data = data else {
                completionHandler(.failure(.invalidData))
                return
            }
            
            do {
                let decode = JSONDecoder()
                decode.keyDecodingStrategy = .convertFromSnakeCase
                let followers = try decode.decode([Follower].self, from: data)
                completionHandler(.success(followers))
            } catch {
                completionHandler(.failure(.invalidData))
            }
            
        }.resume()
    }
    
    func getUser(for username: String, completionHandler: @escaping (Result<User?, GFError>) -> Void) {
        let endPoint = baseUrl + "\(username)"
        guard let url = URL(string: endPoint) else { return completionHandler(.failure(.invalidUsername))}
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let _ = error{
                completionHandler(.failure(.unableToComplete))
                return
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completionHandler(.failure(.invalidResponse))
                return
            }
            
            guard let data = data else {
                completionHandler(.failure(.invalidData))
                return
            }
            
            do {
                let decode = JSONDecoder()
                decode.keyDecodingStrategy = .convertFromSnakeCase
                decode.dateDecodingStrategy = .iso8601
                let user = try decode.decode(User.self, from: data)
                completionHandler(.success(user))
            } catch {
                completionHandler(.failure(.invalidData))
            }
            
        }.resume()
    }
    
    let cache = NSCache<NSString, UIImage>()
    
    func downloadImage(from urlString: String, completionHandler: @escaping (UIImage?) -> Void) {
        let cacheKey = NSString(string: urlString)
        
        if let image = self.cache.object(forKey: cacheKey) {
            print("cache")
            completionHandler(image)
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        print("fetching")
        URLSession.shared.dataTask(with: url) {[weak self] data, response, error in
            guard let self = self,
                  error == nil,
                  let response = response as? HTTPURLResponse, response.statusCode == 200,
                  let data = data,
                  let image = UIImage(data: data) else {
            
                    completionHandler(nil)
                    return
                }
            
            self.cache.setObject(image, forKey: cacheKey)
            
            completionHandler(image)
        }.resume()
    }
}
