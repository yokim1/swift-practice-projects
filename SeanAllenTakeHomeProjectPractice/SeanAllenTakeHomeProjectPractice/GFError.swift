//
//  ErrorMessage.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/17.
//

import Foundation

enum GFError: String, Error {
    case invalidUsername = "invalid username"
    case unableToComplete = "unable to complete"
    case invalidResponse = "invalid response"
    case invalidData = "invalid data"
    case alreadyExist = "The data already exist"
}
