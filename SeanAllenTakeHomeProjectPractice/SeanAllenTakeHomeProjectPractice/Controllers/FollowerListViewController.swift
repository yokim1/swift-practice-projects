//
//  FollowerListViewController.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/16.
//

import UIKit

protocol FollowerListViewControllerDelegate: class {
    func didRequestFollowers(for username: String)
}

class FollowerListViewController: UIViewController {

    enum Section{ case main }
    
    var username: String!
    var followers = [Follower]()
    var collectionView: UICollectionView!
    var diffableDataSource: UICollectionViewDiffableDataSource<Section, Follower>!
    var page: Int = 1
    var hasMoreFollowers = true
    var filteredFollowers: [Follower] = []
    var isSearching = false
    var isLoadingFollowers = false
    
    init(username: String){
        super.init(nibName: nil, bundle: nil)
        self.username = username
        self.title = username
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
        configureSearchController()
        configureCollectionView()
        getFollowers(username: username, page: page)
        configureDiffableDataSource()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "add", style: .done, target: self, action: #selector(addButtonDidTapped))
    }
    
    @objc func addButtonDidTapped() {
        self.showLoadingView()
        NetworkManager.shared.getUser(for: username) {[weak self] results in
            guard let self = self else {return }
            self.dismissLoadingView()
            
            switch results {
            case .success(let user):
                let favorite = Follower(login: user!.login, avatarUrl: user!.avatarUrl)
                PersistanceManager.update(favorite: favorite, actionType: .add) {[weak self] error in
                    guard let self = self else {return}
                    guard let error = error else {
                        self.presentGFAkertOnMainThread(title: "Success", message: "Added new favorite user", buttonTitle: "Ok")
                        return
                    }
                    self.presentGFAkertOnMainThread(title: "Error", message: error.rawValue, buttonTitle: "Ok")
                }
                
            case .failure(let error):
                self.presentGFAkertOnMainThread(title: "Error", message: error.rawValue, buttonTitle: "Ok")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func configureViewController() {
        view.backgroundColor = .white
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = .systemGreen
    }
    
    func configureCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createThreeColumnFlowLayout())
        view.addSubview(collectionView)
        collectionView.backgroundColor = .systemBackground
        collectionView.register(FollowerCell.self, forCellWithReuseIdentifier: FollowerCell.identifier)
        collectionView.delegate = self
    }
    
    func createThreeColumnFlowLayout() -> UICollectionViewFlowLayout {
        let width                       = view.bounds.width
        let padding: CGFloat            = 12
        let minimumItemSpacing: CGFloat = 10
        let availableWidth              = (width - (padding * 2) - (minimumItemSpacing * 2)) / 3
        
        let flowLayout                  = UICollectionViewFlowLayout()
        flowLayout.sectionInset         = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        flowLayout.itemSize             = CGSize(width: availableWidth, height: availableWidth + 40)
        
        return flowLayout
    }
    
    func configureSearchController() {
        let searchController                    = UISearchController()
        //searchController.searchBar.delegate     = self
        searchController.searchResultsUpdater   = self
        searchController.searchBar.placeholder  = "Search for a username"
        navigationItem.searchController         = searchController
        searchController.obscuresBackgroundDuringPresentation = false
    }
    
    func getFollowers(username: String, page: Int) {
        showLoadingView()
        isLoadingFollowers = true
        NetworkManager.shared.getFollowers(for: username, page: page) {[weak self] result in
            guard let self = self else {return }
            
            self.dismissLoadingView()
            
            switch result {
            case .success(let followers):
                if followers?.count ?? 0 < 100 {
                    self.hasMoreFollowers = false
                }
                self.followers.append(contentsOf: followers ?? [])
                
                if self.followers.isEmpty {
                    let message = "This user does not have any followers"
                    DispatchQueue.main.async {
                        self.showEmptyStateView(with: message, in: self.view)
                    }
                    return
                }
                
                self.updateData(on: self.followers)
                break
                
            case .failure(let error):
                self.presentGFAkertOnMainThread(title: "Error", message: error.rawValue , buttonTitle: "Ok")
                break
            }
            
            self.isLoadingFollowers = false
        }
    }
    
    func configureDiffableDataSource() {
        diffableDataSource = UICollectionViewDiffableDataSource<Section, Follower>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, follower) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FollowerCell.identifier, for: indexPath) as! FollowerCell
            cell.set(follower: follower)
            return cell
        })
    }
    
    func updateData(on followers: [Follower]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Follower>()
        snapshot.appendSections([.main])
        snapshot.appendItems(followers)
        DispatchQueue.main.async {
            self.diffableDataSource.apply(snapshot, animatingDifferences: true, completion: nil)
        }
    }
}

extension FollowerListViewController: UICollectionViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY     = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height        = scrollView.frame.size.height
        
        if offsetY > contentHeight - height {
            if hasMoreFollowers,
               isLoadingFollowers == false {
                page += 1
                getFollowers(username: username, page: page)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let follower = isSearching ? filteredFollowers[indexPath.item] : followers[indexPath.item]
        
        let vc                      = UserInfoViewController()
        //vc.follower                 = follower
        vc.username                 = follower.login
        vc.delegate                 = self
        let navigationController    = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true, completion: nil)
    }
}

extension FollowerListViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        guard let filtered = searchController.searchBar.text, !filtered.isEmpty else {
            isSearching = false
            updateData(on: self.followers)
            filteredFollowers.removeAll()
            return }
        
        isSearching = true
        filteredFollowers = followers.filter({ follower in
            follower.login.lowercased().contains(filtered.lowercased())
        })
        updateData(on: filteredFollowers)
    }
    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        isSearching = false
//        updateData(on: self.followers)
//    }
}

extension FollowerListViewController: FollowerListViewControllerDelegate {
    func didRequestFollowers(for username: String) {
        self.username = username
        title = username
        page = 1
        followers.removeAll()
        filteredFollowers.removeAll()
//        collectionView.setContentOffset(.zero, animated: true)
        collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        getFollowers(username: username, page: page)
    }
}
