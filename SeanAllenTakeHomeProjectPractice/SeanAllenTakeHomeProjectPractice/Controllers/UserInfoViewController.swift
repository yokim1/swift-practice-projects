//
//  UserInfoViewController.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/20.
//

import UIKit

protocol UserInfoViewControllerDelegate: class {
    func githubProfileButtonDidTapped(for user: User)
    func gitFollowersButtonDidTapped(for user: User)
}

class UserInfoViewController: UIViewController {

    let headerView = UIView()
    let itemViewOne = UIView()
    let itemViewTwo = UIView()
    let dateLabel = GFBodyLabel(textAlignment: .center)
    //var itemViews: [UIView] = []
    
    var follower: Follower?
    
    var username: String?
    weak var delegate: FollowerListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewController()
        getUserInfo()
        layoutUI()
        
    }
    
    func configureViewController() {
        view.backgroundColor = .systemBackground
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonHandler))
        navigationItem.rightBarButtonItem = doneButton
    }
    
    func getUserInfo() {
        NetworkManager.shared.getUser(for: username ?? "") {[weak self] result in
            guard let self = self else { return }
            
            switch result {
            
            case .success(let user):
                DispatchQueue.main.async { self.configureUIElements(with: user!) }
                break
                
            case .failure(let error):
                self.presentGFAkertOnMainThread(title: "Error", message: error.rawValue, buttonTitle: "Ok")
                break
            }
        }
    }
    
    func configureUIElements(with user: User) {
        
        let gfFollowerItemVC = GFRepoItemViewController(user: user)
        gfFollowerItemVC.delegate = self
        
        let gfRepoItemVC = GFFollowerItemViewController(user: user)
        gfRepoItemVC.delegate = self
        
        self.add(childVC: GFUserInfoHeaderViewController(user: user), to: self.headerView)
        self.add(childVC: gfFollowerItemVC, to: self.itemViewOne)
        self.add(childVC: gfRepoItemVC, to: self.itemViewTwo)
        self.dateLabel.text = "GitHub since \(user.createdAt.convertToMonthYearFormat() ?? "N/A")"
    }
    
    func layoutUI() {
        
        let views = [headerView, itemViewOne, itemViewTwo, dateLabel]
        
        let padding: CGFloat = 20
        let itemHeight: CGFloat = 140
        
        views.forEach { view in
            self.view.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: padding).isActive = true
            view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -padding).isActive = true
        }
        
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            headerView.heightAnchor.constraint(equalToConstant: 180),
            
            itemViewOne.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: padding),
            itemViewOne.heightAnchor.constraint(equalToConstant: itemHeight),
            
            itemViewTwo.topAnchor.constraint(equalTo: itemViewOne.bottomAnchor, constant: padding),
            itemViewTwo.heightAnchor.constraint(equalToConstant: itemHeight),
            
            dateLabel.topAnchor.constraint(equalTo: itemViewTwo.bottomAnchor, constant: padding),
            dateLabel.heightAnchor.constraint(equalToConstant: 18),
        ])
    }
    
    @objc func doneButtonHandler() {
        dismiss(animated: true, completion: nil)
    }
    
    func add(childVC: UIViewController, to containerView: UIView){
        addChild(childVC)
        containerView.addSubview(childVC.view)
        childVC.view.frame = containerView.bounds
        childVC.didMove(toParent: self)
    }
}

extension UserInfoViewController: UserInfoViewControllerDelegate {
    func githubProfileButtonDidTapped(for user: User) {
        guard let url = URL(string: user.htmlUrl) else {
            presentGFAkertOnMainThread(title: "Error", message: "Wrong URL", buttonTitle: "Ok")
            return
        }
        presentSafariVC(with: URL(string: user.htmlUrl)!)
    }
    
    func gitFollowersButtonDidTapped(for user: User) {
        delegate?.didRequestFollowers(for: user.login)
        dismiss(animated: true, completion: nil)
    }
    
    
}
