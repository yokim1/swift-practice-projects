//
//  GFTabBarController.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/27.
//

import UIKit

class GFTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = .systemGreen
        
        viewControllers = [
            navigationControllerCreator(viewController: SearchViewController(),
                                        title: "Search",
                                        tabbarImage: UIImage(systemName: "person"),
                                        selectedTabbarImage: UIImage(systemName: "person.fill")),
            
            navigationControllerCreator(viewController: FavoriteViewController(),
                                        title: "Favorite",
                                        tabbarImage: UIImage(systemName: "star"),
                                        selectedTabbarImage: UIImage(systemName: "star.fill"))
        ]
    }
    
    func navigationControllerCreator(viewController: UIViewController, title: String, tabbarImage: UIImage?, selectedTabbarImage: UIImage?) -> UINavigationController {
        viewController.title = title
        viewController.tabBarItem = UITabBarItem(title: title, image: tabbarImage, selectedImage: selectedTabbarImage)
        return UINavigationController(rootViewController: viewController)
    }
    
    func tabBarControllerCreator() -> UITabBarController {
        let tabBarController = UITabBarController()
        
        UITabBar.appearance().tintColor = .systemGreen
        
        return tabBarController
    }
}
