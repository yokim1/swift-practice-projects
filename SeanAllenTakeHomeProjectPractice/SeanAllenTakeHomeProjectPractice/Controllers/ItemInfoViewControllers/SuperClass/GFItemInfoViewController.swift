//
//  GFItemInfoViewController.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/21.
//

import UIKit

class GFItemInfoViewController: UIViewController {

    let stackView       = UIStackView()
    let itemInfoView    = GFItemInfoVIew()
    let itemInfoView2   = GFItemInfoVIew()
    let actionButton    = GFButton()
    
    var user: User!
    
    var delegate: UserInfoViewControllerDelegate?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        configureActionButton()
        configureBackgroundView()
        layoutUI()
        configureStackView()
    }
    
    init(user: User) {
        super.init(nibName: nil, bundle: nil)
        self.user = user
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureBackgroundView() {
        view.layer.cornerRadius = 18
        view.backgroundColor = .secondaryLabel
    }
    
    func configureStackView() {
        stackView.axis         = .horizontal
        stackView.distribution = .equalSpacing
        
        stackView.addArrangedSubview(itemInfoView)
        stackView.addArrangedSubview(itemInfoView2)
    }
    
    @objc func actionButtonTapped() { }
    
    private func configureActionButton() {
        actionButton.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
    }
    
    private func layoutUI() {
        view.addSubviews(stackView, actionButton)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        let padding: CGFloat = 20
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: padding),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -padding),
            stackView.heightAnchor.constraint(equalToConstant: 50),
            
            actionButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -padding),
            actionButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            actionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
            actionButton.heightAnchor.constraint(equalToConstant: 44 )
        ])
    }
    
    
}
