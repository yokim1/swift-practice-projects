//
//  File.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/23.
//

import UIKit

class GFFollowerItemViewController: GFItemInfoViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configureItems()
        
    }

    private func configureItems(){
        itemInfoView.set(itemInfoType: .followers, with: user.followers)
        itemInfoView2.set(itemInfoType: .following, with: user.following)
        actionButton.set(backgroundColor: .systemGreen, title: "Git Followers")
    }
    
    override func actionButtonTapped() {
        delegate?.gitFollowersButtonDidTapped(for: user)
        
    }
}
