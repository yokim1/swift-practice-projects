//
//  FavoriteViewController.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/14.
//

import UIKit

class FavoriteViewController: UIViewController {

    let tableView               = UITableView()
    var favorites:[Follower]    = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewController()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFollowers()
    }
    
    func configureViewController() {
        view.backgroundColor    = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        tableView.frame         = view.bounds
        tableView.rowHeight     = 80
        tableView.delegate      = self
        tableView.dataSource    = self
        tableView.register(FavoriteCellTableViewCell.self, forCellReuseIdentifier: FavoriteCellTableViewCell.identifier)
    }

    func getFollowers() {
        PersistanceManager.retrieveFavorites {[weak self] results in
            guard let self = self else { return }
            
            switch results {
            case .failure(let error):
                self.presentGFAkertOnMainThread(title: "Error", message: error.rawValue, buttonTitle: "Cancel")
                break
            case .success(let favorites):
                if favorites.isEmpty{
                    self.showEmptyStateView(with: "Empty", in: self.view)
                } else {
                    self.favorites = favorites
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.view.bringSubviewToFront(self.tableView)
                    }
                }
            }
        }
    }
}

extension FavoriteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let favorite        = favorites[indexPath.row]
        let destVC          = FollowerListViewController(username: favorite.login)
        navigationController?.pushViewController(destVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else {return}
        
        let favorite        = favorites[indexPath.row]
        
        PersistanceManager.update(favorite: favorite, actionType: .remove) {[weak self] error in
            guard let self  = self else { return }
            guard let error = error else {
                self.favorites.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .left)
                return
            }
           
            self.presentGFAkertOnMainThread(title: "Unable to remove", message: error.rawValue, buttonTitle: "Ok")
        }
    }
}

extension FavoriteViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        favorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FavoriteCellTableViewCell.identifier) as? FavoriteCellTableViewCell else{ return UITableViewCell() }
        
        cell.set(follower: favorites[indexPath.row])
        return cell
    }
    
    
}
