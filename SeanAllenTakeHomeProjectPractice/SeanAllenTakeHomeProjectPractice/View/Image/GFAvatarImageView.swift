//
//  GFAvatarImageView.swift
//  SeanAllenTakeHomeProjectPractice
//
//  Created by 김윤석 on 2021/06/18.
//

import UIKit

class GFAvatarImageView: UIImageView {

    let placeholderImage = UIImage(systemName: "person")!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        layer.cornerRadius = 10
        clipsToBounds = true
        image = placeholderImage
        translatesAutoresizingMaskIntoConstraints = false
    }
    
//    func downloadImage(from urlString: String) {
//        let cacheKey = NSString(string: urlString)
//        
//        if let image = GFAvatarImageView.cache.object(forKey: cacheKey){
//            self.image = image
//            return
//        }
//        
//        guard let url = URL(string: urlString) else { return }
//        URLSession.shared.dataTask(with: url) {[weak self] data, response, error in
//            if let _ = error{ return }
//            
//            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { return }
//            
//            guard let data = data else { return }
//            
//            guard let image = UIImage(data: data) else { return }
//            GFAvatarImageView.cache.setObject(image, forKey: cacheKey)
//            DispatchQueue.main.async {
//                self?.image = image
//            }
//        }.resume()
//    }
}
