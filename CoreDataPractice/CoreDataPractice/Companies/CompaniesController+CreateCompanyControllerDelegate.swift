//
//  CompaniesController+CreateCompanyControllerDelegate.swift
//  CoreDataPractice
//
//  Created by 김윤석 on 2021/06/05.
//

import UIKit

extension CompaniesController: CreateCompanyControllerDelegate {
    func didEditCompany(company: Company) {
        let row = companies.firstIndex(of: company)
        let reloadIndexPath = IndexPath(row: row!, section: 0)
        tableView.reloadRows(at: [reloadIndexPath], with: .middle)
    }
    
    func didAddCompany(company: Company) {
        //1.modify your array
        companies.append(company)
        
        //2.insert a new index path into tableView
        let newIndexPath = IndexPath(row: companies.count - 1, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
    }
}
