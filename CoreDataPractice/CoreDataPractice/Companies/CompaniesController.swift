//
//  ViewController.swift
//  CoreDataPractice
//
//  Created by 김윤석 on 2021/06/03.
//

import UIKit
import CoreData

class CompaniesController: UITableViewController {
    
    var companies = [Company]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companies = CoreDataManager.shared.fetchCompanies()
        
        view.backgroundColor = .white
        
        navigationItem.title = "Companies"
        
        tableView.backgroundColor = .darkBlue
        tableView.separatorColor = .white
        
        //tableView.separatorStyle = .none
        tableView.tableFooterView = UIView() // black UIView
        
        tableView.register(CompanyCell.self, forCellReuseIdentifier: "cellId")
        
        setupPlusButtonInNavBar(selector: #selector(handleAddCompany))
        
        //navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Remove All", style: .plain, target: self, action: #selector(handleRemoveCompanies))
        
        navigationItem.leftBarButtonItems = [UIBarButtonItem(title: "Remove All", style: .plain, target: self, action: #selector(handleRemoveCompanies)),
                                             UIBarButtonItem(title: "do Work", style: .plain, target: self, action: #selector(doWork))]
        
        setupNavigationStyle()
    }
    
    @objc private func doWork() {
        DispatchQueue.global(qos: .background).async {
            
        }
    }
    
    @objc private func handleAddCompany() {
        print("add company")
        
        let createCompanyController = CreateCompanyController()
        //createCompanyController.view.backgroundColor = .green
        
        let navController = CustomNavigationController(rootViewController: createCompanyController)
        navController.modalPresentationStyle = .fullScreen
        createCompanyController.delegate = self
        present(navController, animated: true, completion: nil)
    }
    
    @objc private func handleRemoveCompanies() {
        print("remove company")
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Company.fetchRequest())
        
        do {
            
            try context.execute(batchDeleteRequest)
            
            var indexPathToRemove = [IndexPath]()
            
            for (index, _) in companies.enumerated() {
                let indexPath = IndexPath(row: index, section: 0)
                indexPathToRemove.append(indexPath)
            }
            
            companies.removeAll()
            tableView.deleteRows(at: indexPathToRemove, with: .left)
            
        } catch {
            
        }
    }
    
}
