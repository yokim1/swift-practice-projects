//
//  CreateEmplyeeController.swift
//  CoreDataPractice
//
//  Created by 김윤석 on 2021/06/05.
//

import UIKit
import CoreData

protocol CreateEmployeeControllerDelegate {
    func didAddCompany(employee: Employee)
}

class CreateEmployeeController: UIViewController {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let nameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter Name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let birthdayLabel: UILabel = {
        let label = UILabel()
        label.text = "Birthday"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let birthdayTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "MM/DD/YYYY"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let employeesTypeSegmentController: UISegmentedControl = {
        let types = [EmployeeType.executive.rawValue, EmployeeType.manager.rawValue ,EmployeeType.staff.rawValue]
        let segmentControl = UISegmentedControl(items: types)
        segmentControl.selectedSegmentIndex = 0
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentControl
    }()
    
    var delegate: CreateEmployeeControllerDelegate?
    var company: Company?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Create Employees"
        
        setupCancelButtonInNavBar(selector: #selector(cancelButtonHandler))
        setupNavigationStyle()
        view.backgroundColor = .darkBlue
        
        
        setupUI()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonHandler))
    }
    
    @objc func saveButtonHandler(){
        let context = CoreDataManager.shared.persistentContainer.viewContext
        
        let employee = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context) as! Employee
        employee.setValue(nameTextField.text, forKey: "name")
        
        
        employee.company = company
        
        let employeeInformation = NSEntityDescription.insertNewObject(forEntityName: "EmployeeInformation", into: context) as! EmployeeInformation
        
        employeeInformation.taxId = "123"
        
        guard let birthdayText = birthdayTextField.text else {return }
        
        if birthdayText.isEmpty {
            showErrorMessage(title: "Need Birthday Information", message: "Birthday information is empty")
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        guard let birthdayDate = dateFormatter.date(from: birthdayText) else {
            showErrorMessage(title: "Wrong Birthday format", message: "Please keep MM/dd/yyyy format")
            return
        }
        
        employeeInformation.birthday = birthdayDate
        
        let employeeType = employeesTypeSegmentController.titleForSegment(at: employeesTypeSegmentController.selectedSegmentIndex)
        
        employee.type = employeeType
        
        employee.employeeInformation = employeeInformation
        
        do {
            try context.save()
        } catch {
            
        }
        
        dismiss(animated: true) {
            self.delegate?.didAddCompany(employee: employee)
        }
    }
    
    @objc func cancelButtonHandler() {
        dismiss(animated: true, completion: nil)
    }
    
    private func showErrorMessage(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    }
    
    func setupUI() {
        let lightBlueBackground = setUpLightBlueBackground(height: 250)
        
        view.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 15).isActive = true
        //nameLabel.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(nameTextField)
        nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        nameTextField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        nameTextField.bottomAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor).isActive = true
        
        view.addSubview(birthdayLabel)
        birthdayLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 10).isActive = true
        birthdayLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 15).isActive = true
        birthdayLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        birthdayLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(birthdayTextField)
        birthdayTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        birthdayTextField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        birthdayTextField.bottomAnchor.constraint(equalTo: birthdayLabel.bottomAnchor).isActive = true
        birthdayTextField.topAnchor.constraint(equalTo: birthdayLabel.topAnchor).isActive = true
        
        view.addSubview(employeesTypeSegmentController)
        employeesTypeSegmentController.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        employeesTypeSegmentController.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        employeesTypeSegmentController.topAnchor.constraint(equalTo: birthdayLabel.bottomAnchor, constant: 10).isActive = true
        employeesTypeSegmentController.heightAnchor.constraint(equalToConstant: 30).isActive = true
        employeesTypeSegmentController.bottomAnchor.constraint(equalTo: lightBlueBackground.bottomAnchor, constant: -10).isActive = true
       
        
    }
}
