//
//  EmployeesController.swift
//  CoreDataPractice
//
//  Created by 김윤석 on 2021/06/05.
//

import UIKit
import CoreData

class CustomIndentedLabel: UILabel{
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        let customRect = rect.inset(by: insets)
        super.drawText(in: customRect)
    }
}

class EmployeesController: UITableViewController, CreateEmployeeControllerDelegate {
    
    var company: Company?
    //var employees = [Employee]()
    
    var allEmployees = [[Employee]]()
    
    var executiveEmployees = [Employee]()
    var managerEmployees = [Employee]()
    var staffEmployees = [Employee]()
    
    override func viewDidLoad() {
        fetchEmployees()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellId")
        
        tableView.backgroundColor = .darkBlue
        tableView.separatorColor = .white
        
        setupPlusButtonInNavBar(selector: #selector(addButtonHandler))
    }
    
    @objc func addButtonHandler() {
        let createEmployeeController = CreateEmployeeController()
        let navController = UINavigationController(rootViewController: createEmployeeController)
        navController.modalPresentationStyle = .fullScreen
        createEmployeeController.delegate = self
        createEmployeeController.company = company
        present(navController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = company?.name
        //fetchEmployees()
    }
    
    let employeeTypes = [
        EmployeeType.executive.rawValue,
        EmployeeType.manager.rawValue,
        EmployeeType.staff.rawValue
    ]
    
    private func fetchEmployees() {
        guard let companyEmployees = company?.employees?.allObjects as? [Employee] else {return }
 
        //기존에 append한 중복된 Data를 없애기 위한것
        allEmployees = []
        
        employeeTypes.forEach { type in
            allEmployees.append(
                companyEmployees.filter({ employee in
                    employee.type == type
                })
            )
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        allEmployees.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = CustomIndentedLabel()

        label.text = employeeTypes[section]
        
        label.backgroundColor = .lightBlue
        label.textColor = .darkBlue
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        allEmployees[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellId") else {return UITableViewCell()}

        let employee = allEmployees[indexPath.section][indexPath.row]
        
        if let name = employee.name {
            cell.textLabel?.text = "Name: \(name) "
        }
        
        if let birthday = employee.employeeInformation?.birthday {
            cell.textLabel?.text?.append( " Birthday: \(birthday)")
        }

        cell.backgroundColor = .tealColor
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func didAddCompany(employee: Employee) {
        //fetchEmployees()
//        tableView.reloadData()
        guard let section = employeeTypes.index(of: employee.type ?? "") else {return}

        let row = allEmployees[section].count

        let indexPath = IndexPath(row: row, section: section)
        
        allEmployees[section].append(employee)
        
        tableView.insertRows(at: [indexPath], with: .middle)
    }
}


enum EmployeeType: String {
    case executive = "Executive"
    case manager = "Manager"
    case staff = "Staff"
}
