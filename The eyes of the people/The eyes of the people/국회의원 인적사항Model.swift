//
//  국회의원 인적사항Model.swift
//  The eyes of the people
//
//  Created by 김윤석 on 2021/05/15.
//

import Foundation

//key: 97aa2b669ea14ffd950f138321724a2a

//https://open.assembly.go.kr/portal/openapi/nwvrqwxyaytdsfvhu?KEY=97aa2b669ea14ffd950f138321724a2a&Type=json


//https://open.assembly.go.kr/portal/openapi/ncocpgfiaoituanbr?KEY=97aa2b669ea14ffd950f138321724a2a&Type=json&AGE=20


struct PersonalInformation: Codable {
    let nwvrqwxyaytdsfvhu: [Head]?
}

struct Head: Codable {
    let head: [ThisJsonInfo]?
    let row: [Row]?
}

struct ThisJsonInfo: Codable{
    let list_total_count: Int?
    let RESULT: Result?
}

struct Result: Codable {
    let CODE: String?
    let MESSAGE: String?
}

struct Row: Codable {
    let HG_NM: String?
    let HJ_NM: String?
    let ENG_NM: String?
    let BTH_GBN_NM: String?
    let BTH_DATE: String?
    let JOB_RES_NM: String?
    let POLY_NM: String?
    let ORIG_NM: String?
    let ELECT_GBN_NM: String?
    let CMIT_NM: String?
    let CMITS: String?
    let REELE_GBN_NM: String?
    let UNITS: String?
    let SEX_GBN_NM: String?
    let TEL_NO: String?
    let E_MAIL: String?
    let HOMEPAGE: String?
    let STAFF: String?
    let SECRETARY: String?
    let SECRETARY2: String?
    let MONA_CD: String?
    let MEM_TITLE: String?
    let ASSEM_ADDR: String?
}
////
//{
//  "nwvrqwxyaytdsfvhu": [
//    {
//      "head":
//      [
//        {
//          "list_total_count": 300
//        },
//        {
//          "RESULT": {
//            "CODE": "INFO-000",
//            "MESSAGE": "정상 처리되었습니다."
//          }
//        }
//      ]
//    },
//    {
//      "row": [
//        {
//          "HG_NM": "강기윤",
//          "HJ_NM": "姜起潤",
//          "ENG_NM": "KANG GIYUN",
//          "BTH_GBN_NM": "음",
//          "BTH_DATE": "1960-06-04",
//          "JOB_RES_NM": "간사",
//          "POLY_NM": "국민의힘",
//          "ORIG_NM": "경남 창원시성산구",
//          "ELECT_GBN_NM": "지역구",
//          "CMIT_NM": "보건복지위원회",
//          "CMITS": "보건복지위원회",
//          "REELE_GBN_NM": "재선",
//          "UNITS": "제19대, 제21대",
//          "SEX_GBN_NM": "남",
//          "TEL_NO": "02-784-1751",
//          "E_MAIL": "ggotop@naver.com",
//          "HOMEPAGE": "http://blog.naver.com/ggotop",
//          "STAFF": "김홍광, 한영애",
//          "SECRETARY": "박응서, 최광림",
//          "SECRETARY2": "김영록, 안효상, 이유진, 홍지형, 김지훈",
//          "MONA_CD": "14M56632",
//          "MEM_TITLE": "[학력]\r\n마산공고(26회)\r\n창원대학교 행정학과\r\n중앙대학교 행정대학원 지방의회과 석사\r\n창원대학교 대학원 행정학 박사\r\n\r\n[경력]\r\n현) 국회 보건복지위원회 국민의힘 간사\r\n현) 국민의힘 소상공인살리기 특별위원회 부위원장\r\n현) 국민의힘 코로나19 대책 특별위원회 위원\r\n\r\n미래통합당 경남도당 민생특위 위원장\r\n제19대 국회의원 (새누리당/경남 창원시 성산구)\r\n새누리당 원내부대표",
//          "ASSEM_ADDR": "의원회관 937호"
//        }
//    ]
