//
//  ViewController.swift
//  The eyes of the people
//
//  Created by 김윤석 on 2021/05/14.
//

import UIKit

//key: 97aa2b669ea14ffd950f138321724a2a

//https://open.assembly.go.kr/portal/openapi/nzmimeepazxkubdpn?KEY=97aa2b669ea14ffd950f138321724a2a&Type=json&pIndex=1&pSize=50&AGE=21

//https://open.assembly.go.kr/portal/openapi/nwbpacrgavhjryiph?KEY=97aa2b669ea14ffd950f138321724a2a&AGE=21

class ViewController: UIViewController {

    var datas: [Row] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        fetchApps(searchTerm: "https://open.assembly.go.kr/portal/openapi/nwvrqwxyaytdsfvhu?KEY=97aa2b669ea14ffd950f138321724a2a&Type=json&pSize=301") {
            
            self.datas = $0?.nwvrqwxyaytdsfvhu?.last?.row ?? []
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.tableView.reloadData()
        }
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? Cell else { return UITableViewCell() }
        cell.name.text = datas[indexPath.row].HG_NM
        cell.chineseName.text = datas[indexPath.row].HJ_NM
        cell.englishName.text = datas[indexPath.row].ENG_NM
        return cell
    }
    
    
}

func fetchApps(searchTerm: String, completion: @escaping (PersonalInformation?)->Void) {
    
    guard let url = URL(string: searchTerm) else { return }
    URLSession.shared.dataTask(with: url) { (data, response, error) in
        
        if let err = error {
            print("Failed to fetch apps:", err)
            //completion([], nil)
            return
        }
        
        guard let data = data else {return}
        
        do {
            let searchResult = try JSONDecoder().decode(PersonalInformation.self, from: data)
            
            completion(searchResult)
            
        } catch let Jsonerror {
            //completion([], Jsonerror)
            print(Jsonerror.localizedDescription)
            print(Jsonerror)
            print("Failed To Decode")
        }
        
    }.resume()
}

class Cell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var chineseName: UILabel!
    @IBOutlet weak var englishName: UILabel!
    @IBOutlet weak var sunOrMoon: UILabel!
    @IBOutlet weak var birthday: UILabel!
    @IBOutlet weak var job: UILabel!
    @IBOutlet weak var poly: UILabel!
    @IBOutlet weak var originalArea: UILabel!
    
}
