//
//  ViewController.swift
//  PracticeTimer
//
//  Created by 김윤석 on 2021/04/19.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    
    var timer = Timer()
    var counter = 0.0
    var isRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timeLabel.text = "\(counter)"
        
        startButton.isEnabled = true
        pauseButton.isEnabled = false
    }
    
    @IBAction func startButton(_ sender: Any) {
        if !isRunning {
            timer = Timer.scheduledTimer(timeInterval: 0.1,
                                         target: self,
                                         selector: #selector(ViewController.updateTimer),
                                         userInfo: nil,
                                         repeats: true)
            startButton.isEnabled = false
            pauseButton.isEnabled = true
            isRunning = true
        }
    }
    
    @IBAction func pauseButton(_ sender: Any) {
        startButton.isEnabled = true
        pauseButton.isEnabled = false
        isRunning = false
        timer.invalidate()
    }
    
    @IBAction func resetButton(_ sender: Any) {
        startButton.isEnabled = true
        pauseButton.isEnabled = false
        isRunning = false
        timer.invalidate()
        counter = 0.0
        timeLabel.text = "\(counter)"
    }
    
    @objc func updateTimer(){
        counter += 0.1
        timeLabel.text = String(format: "%.1f", counter)
    }
}
